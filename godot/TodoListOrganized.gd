# Jordan's TODO List:
# FIXME: Missing Death Events, no recorded event generated.
# FIXME: Connect the Death to which player you were killed by.
# FIXME: Connect the score counters to the events, track off the most current 
#        reliable data source, but once the server has delivered the truth data
#        we will always apply it as the authority.
# TODO: Implement Observer: 
#       1. Observer will not hit a game_state of 2, they stay at 1.
#       2. Observer Screen shows the history of events as a scrollable list.
#       3. Observer events screen can be maximized to take up all but the header.
#       4. The server will not consider an observer when determining how many 
#          players are ready to play.
# FIXME: Fix Dedicated Host:
#        1. Host does not count themselves as ready to play if they are a 
#           dedicated host.
#        2. Dedicated Host should not reach game_state of 2.
# FIXME: Any number of players in pre-game should work. Right now it only starts
#        the first 2 players.
# TODO: Vibrate phone on death and audio feedback.
# TODO: Ensure we can end the game and go back to main menu via the menu in header.
# TODO: Add Maps/Geodetic back into pre-game.
# FIXME: On connection to gun lost, alert the player. 
#        1. Attempt to auto reconnect and disable recoil, to reduce the chance 
#           of another disconnect.
#        2. If reconnect fails after 3 seconds display option to retry or to 
#           connect to a different weapon.
# FIXME: If the app is backgrounded then the gun may get disconnected, see if 
#        the above fixes have fixed the issue?
# TODO: Go Alpha
# FIXME: The button for the map needs to open up the entire map for viewing.
# TODO: Also need to be able to return to normal UI.
# TODO: Ensure the host can transfer the maps to clients and re-gets any bad maps.
# TODO: Connect the Radio to the Radio Comms overlay.
# TODO: Connect the scores to the Scores display.
# TODO: Connect Incoming Message to Radio History display.
# TODO: Medic Can play and heal via gun.
# TODO: Battle Royale
# FIXME: Can start the next game mode.
# TODO: Weapon damage levels needs to be implemented.
# FIXME: Reload sound needs to be stretched/compressed for different reload times.
# TODO: Go Beta
# TODO: Medic can heal via Audio.
# TODO: Standard Battle Mode
# TODO: Capture the Flag
# TODO: Integration with Digital Flag
# TODO: Go Release Canidate
# TODO: On Identify or right after, When a client rejoins, we should ask how large 
#       is their events history, if they size is different from the server, then 
#       resend them the events history to fix. Accounts for an app crash.
# TODO: Fix bugs until good enough.
# TODO: Make a stable release!

# FIXME: The app can crash if we try to connect to a gun that was selected by 
#        another player, we need to be able to trigger this in a test, to make 
#        it happen just before you connect to the gun have some one else do it.

# Circle Back For TODO List:
# Rename PregameLobby to GameSelection, because it is not really a lobby.
# TODO: Write WIFI status check for Android in Kotlin. Will need permissions.
# TODO: Write Android Battery get_percentage() in Kotlin. Will need permissions.


#Eric's Todo List:
#TODO: Validate that PlayerName, PlayerAvatar, and PlayerProfile all work with the last
#       set of script updates (unable to validate with current state of app).
#TODO: Create the UI for Inventory viewing InGame.
#       Replace crate icon with a better Inventory icon (?) in RPanel.tscn.
#       Create Inventory scene, add loading of the scene in Pregame.gd.
#       Add needed show/hide script to icon in RPanel.
#TODO: Add Hits and Kills code in GameCommon that's needed to support connecting
#       of LPanel and RPanel controls.
#TODO: Add Scoreboard scene and code (refer to Gen1 merge request)
#       InGame stats/variables added to Settings.Settings.md document.
#       SCOREBOARD CONCEPT
#           Because of space restrictions, StandardHeader is NOT SHOWN in Scoreboard scene
#           Divide data into Player and Team Stats
#           Use 4 data columns per display w/ Player Name as 1st column
#
#TODO: Periodically review all FIXME (or FIX_ME) areas in the code and resolve.
#
#
#FUTURE ADDITIONS:
#TODO: Loadouts:
#       Add loadouts to OfflineSettings, in Armory area.
#       Add loadouts to GameSettings (selections = "Random Weapons", "Loadouts").
#       Add loadouts to lobbies (user defined/selected or host defined/selected?).
#TODO: AssignWeapon Scene:
#       Add changing of weapon name after connection to weapon.
#       Add transfer of all weapon names from Host to other players.
#TODO: Standard Header
#       WIFI (Wifi Signal) - Short-term this will be repurposed to register connection to a game
#       MOBILE (Mobile Battery) - No native Mobile Battery support (get_power_percent_left)
#                                   for Android.  Wrote script for MacOS and Windows
#                                   support.  Need additional plugin?
#       GPS (Signal), BLUETOOTH (Connection), BATTERY (Weapon Battery) - COMPLETE
#TODO: FreecoiLInterface.gd, plugin, etc.
#       Add weapon damage as setting in gun.  Obtain damage from IR shot and apply to player's
#       health/shield.
#       Add multiple Laser ID capability to app (ask Jordan for details of what's needed).
#       Add game setting of requiring plugged-in IR receiver.  Add Clip-On RX Check Interval setting
#       in FreecoiLInterface.gd and plugin.  Apply damage to player when IR receiver is unplugged.
#
#
#FOR REFERENCE:
#   STANDARD VARIABLE NAMING: All lower case, words separated by underscore
#       (Example: variable_name)
#   See Template.gd for reference and explanation (comments) on State Class
#       implementation on scenes
#   For Debugging of MainScreen Scene/Screens
#       Splash.gd: Change State Class names in elif-statement of
#           background_load_state_ahead (Lines 258 & 375) 
#       Settings.gd: Add/Edit correct variable in experimental_toggles to "true" (Line 34)
#   For Show/Hide of the StandardHeader, toggle the
#       PM.get_tree().root.get_node("Container/UI").get_child(0).visible in the
#       on_enter and on_exit of the State Class.
#
