extends Resource
# Technically this extends Resource, but that is the default if empty.
# All components must have the below vars to be valid components.
var parent_entity: Node
var components_array: Array  # With great power comes great responsibility.
# Below determines if the component is unique. You pretty much always want true.
const unique_component: bool = true
# Be sure to adjust the booleans below to reflect what your subsystem needs.
const has_subsystem: bool = true
# For efficiency, subsystems should not be unique! Also make below a constant.
var unique_subsystem: bool = false
const subsystem_has_on_ready: bool = true
const subsystem_has_on_process: bool = true
const subsystem_has_on_physics_process: bool = true
const subsystem_has_on_input: bool = true
const subsystem_has_on_unhandled_input: bool = true
# Make the COMPONENT_SYSTEM_ID Unique but the same across the Component and the 
# System so they can be properly paired on ready/when the node enters the scene.
const COMPONENT_SYSTEM_ID = "Template"
const VERSION = 1
# Make your edits below this line.
# Declare constants here.
# Declare your component specific vars here.
var component_specific = true
var test_ready: int = 0
var test_process: int = 0
var test_physics_process: int = 0
var test_input: int = 0
var test_unhandled_input: int = 0
# Resources are preloaded in as objects, not resources if you preload.
# var make_entity_subsystem:Object = preload("res://systems/make_entity_subsystem.tres")
# Declare Component Export variables here.
export(int) var health = 2


# Remember components are just for data, any logic or functions should be 
# executed by a super system or a sub system.

# Declare functions that may be used by the system, typically this is useful
# for state machines that share functions via a component.

# Components are not Nodes, so they never technically enter the scene, 
# so _ready() is never triggered. Additionally most of these sort of functions 
# are never triggered.
#func _process():
#    pass # Never gets called by the scene tree on a resource..
