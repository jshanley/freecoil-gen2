extends Resource

#"state_has" variables are for the types of functions that would be used in this state
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "PreGameLobby" #Main state for this script.
var state_num: int #No longer used, replaced with Match on StateID
var transitionable_states: Dictionary = {} #Enables local mapping of State number to StateID
var required_for_state_is_complete = false #Initial value
var loading_next_state_already = false #Initial value
var started_background_loading = false #Initial value
var my_screen_offset = "0,0" #Initial value
var my_screen_name = "" #Initial value
var next_state: String #Used for name of next state (transition)
var cleanup_after_some_frames = 5
var thread_num: int

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    load_required_for_state()
    #ProgramManager_state_trigger is a generic variable for general use
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger") 
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    
func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value == "Join":
        # EXTRACT game details:
        var GamesContainer = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node(my_screen_name).get_node(
                "CenterContainer/VBoxContainer/ScrollContainer/VBoxContainer/GamesContainer")
        var selected_game = null
        var matched_server = null
        for game in GamesContainer.get_children():
            if game.modulate == Color("34e21b"):  # is selected.
                selected_game = game
                break
        if selected_game != null:
            for game_server in PM.Settings.Session.get_data("udp_server_dict"):
                var game_server_details = PM.Settings.Session.get_data("udp_server_details_dict")[game_server]
                if game_server_details[4] == selected_game.get_node("VBoxContainer/NameBtn").text:
                    matched_server = game_server
        if matched_server != null:
            #FIXME: REMOVE THIS AFTER OBSERVER ROLE IS IMPLEMENTED, ALL OTHER STATE VARIABLE 
            # ROUTING CODE IN THIS SCRIPT IS COMPLETE FOR FUTURE OBSERVER IMPLEMENTATION
            if PM.Settings.Session.get_data("is_observer") == true: 
                PM.Settings.Session.set_data("need_gps", false, false, false)
                PM.Settings.Session.set_data("need_bluetooth", false, false, false)
                next_state = "Observer"
            else:
                next_state = "AwaitingHostPregame"
            var game_server_details = PM.Settings.Session.get_data("udp_server_details_dict")[matched_server]
            PM.Settings.Session.set_data("server_ip", game_server_details[1])
            PM.Settings.Session.set_data("server_port", game_server_details[2])
            PM.Networking.Client_Module.setup_as_client()
            PM.call_deferred("evaluate_state_change", transitionable_states[next_state])
    if new_value == "GoBack":
        PM.call_deferred("evaluate_state_change", -1)

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID: #Search for state variables shown below and register their index
                "AwaitingHostPregame":
                    transitionable_states[state.STATE_ID] = counter
                "Observer":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
            PM.LoadingScreen.fade_in()
        PM.LoadingScreen.loading_progress(1.0)
        PM.LoadingScreen.loading_item_name("res://scenes/MainScreens/PreGameLobby.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
            "res://scenes/MainScreens/PreGameLobby.tscn", self, 
            "setup_required_for_state"
        )
    
func setup_required_for_state(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(15.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "setup_required_for_state").instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(17.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/GameModes/PreGame.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/GameModes/PreGame.tscn", self, 
                "stage_2_loading")

func stage_2_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(49.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_2_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(51.0)
        PM.LoadingScreen.loading_item_name("res://Logic/EverGreen/ECS/Systems/SuperSystems/RandomGenerator/RandomGenerator.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://Logic/EverGreen/ECS/Systems/SuperSystems/RandomGenerator/RandomGenerator.tscn", self, 
                "stage_3_loading")
                
func stage_3_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(66.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_3_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(68.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/GameModes/GameCommon.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/GameModes/GameCommon.tscn", self, 
                "stage_4_loading")

func stage_4_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(83.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_4_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(85.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/GameModes/BlinkHexTris.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/GameModes/BlinkHexTris.tscn", self, 
                "stage_5_loading")

func stage_5_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(98.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_5_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        call_deferred("finished_loading")

func finished_loading():
    PM.LoadingScreen.loading_progress(100.0)
    PM.LoadingScreen.fade_out()
    required_for_state_is_complete = true
    screen_change(null)

func background_load_state_ahead():
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

#Background loading for next state.  Last line calls ProgramManager and it handles the transition and cleanup
func setup_state_ahead(): 
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
