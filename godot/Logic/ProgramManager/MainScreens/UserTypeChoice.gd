extends Resource

#"state_has" variables are for the types of functions that would be used in this state
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "UserTypeChoice"
var state_num: int #No longer used, replaced with Match on StateID
var transitionable_states: Dictionary = {} #Enables local mapping of State number to StateID
var required_for_state_is_complete = false #Initial value
var loading_next_state_already = false #Initial value
var started_background_loading = false #Initial value
var my_screen_offset = "0,0" #Initial value
var my_screen_name = "" #Initial value
var next_state: String #Used for name of next state (transition)
var cleanup_after_some_frames = 5

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "info")
    load_required_for_state()
    #ProgramManager_state_trigger is a generic variable for general use
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger") 
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Networking.test_ip_address()

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value == "PlayerSelected":
        #DEBUG ONLY - Check variables
        PM.Settings.Log("UserTypeChoice RESULT:", "debug")
        PM.Settings.Log("is_host = " + str(PM.Settings.Session.get_data("is_host")), "debug")
        PM.Settings.Log("is_soldier = " + str(PM.Settings.Session.get_data("is_soldier")), "debug")
        PM.Settings.Log("is_medic = " + str(PM.Settings.Session.get_data("is_medic")), "debug")
        PM.Settings.Log("is_observer = " + str(PM.Settings.Session.get_data("is_observer")), "debug")
        if PM.Settings.Session.get_data("is_host") > 0:
            if PM.Settings.Session.get_data("server_ip_issue") == "all_good":
                next_state = "PreGameSettings"
            elif PM.Settings.Session.get_data("server_ip_issue") == "none":
                next_state = "NoIpAddresses"
            else:
                next_state = "MultipleIpAddresses"
        else:
            PM.Networking.UDP_Module.search_for_peers()
            next_state = "PreGameLobby"
        PM.call_deferred("evaluate_state_change", transitionable_states[next_state])
    elif new_value == "OfflineSettings":
        next_state = "OfflineSettings"
        PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID: #Search for state variables shown below and register their index
                "PreGameSettings":
                    transitionable_states[state.STATE_ID] = counter
                "PreGameLobby":
                    transitionable_states[state.STATE_ID] = counter
                "MultipleIpAddresses":
                    transitionable_states[state.STATE_ID] = counter
                "NoIpAddresses":
                    transitionable_states[state.STATE_ID] = counter
                "OfflineSettings":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        PM.background_load_resource("res://scenes/MainScreens/UserTypeChoice.tscn", self, "setup_required_for_state")
    
func setup_required_for_state(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_required_for_state")
        var loaded_scene = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        required_for_state_is_complete = true
        if PM.Settings.OperatingSystem != "Android":
            PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name
                    ).get_node("CenterContainer/VBoxContainer/VBoxContainer2/SoldierDescription").visible = false
            PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name
                    ).get_node("CenterContainer/VBoxContainer/VBoxContainer2/Soldier").visible = false
            PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name
                    ).get_node("CenterContainer/VBoxContainer/VBoxContainer2/Soldier").disabled = true
        screen_change(null)

func background_load_state_ahead():  # This is now optional.
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead(): # This is always called by the function above.
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    transition_to_next_state()

func transition_to_next_state():  # This is now the prefereed method of transitioning to the next state.
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
