extends Node

var recorded_events: Array = []
var recording = true
var time_last = OS.get_ticks_msec()

func _input(event):
    if recording:
        var new_event = null
        var time_now = OS.get_ticks_msec()
        if event is InputEventMouseButton:
            if not event.pressed:  # Then we know it was released.
                new_event = {"type": "InputEventMouseButton", 
                    "position": event.position, "button_index": event.button_index,
                    "time_before_exec": time_now - time_last,
                }
        elif event is InputEventKey:
            if not event.pressed:  # Then we know it was released.
                if event.scancode == 16777249:  # F6 == Stop recording.
                    var last_event = {"type": "End", 
                        "time_before_exec": time_now - time_last,
                    }
                    recorded_events.append(last_event)
                    recording = false
                    var itm_out_path = PM.ITM_test_path
                    itm_out_path = itm_out_path.replace("res://", "")
                    itm_out_path = ProjectSettings.globalize_path("res://") + itm_out_path
                    var file_name
                    if PM.ITM_act_as_server:
                        file_name = "server"
                    else:
                        file_name = "client"
                    itm_out_path = itm_out_path.plus_file(file_name) + ".json"
                    var file = File.new()
                    var err = file.open(itm_out_path, File.WRITE)
                    file.store_string(to_json(recorded_events))
                    file.close()
                    print("Recording has ended!")
                else:
                    new_event = {"type": "InputEventKey", 
                        "unicode": event.unicode, 
                        "time_before_exec": time_now - time_last,
                    }
        if new_event != null:
            recorded_events.append(new_event)
            time_last = time_now

