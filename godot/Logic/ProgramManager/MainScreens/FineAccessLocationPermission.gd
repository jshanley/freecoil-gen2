extends Resource
# Technically this extends Resource, but that is the default if empty.

var state_has_on_ready = false
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "FineAccessLocationPermission"
var state_num: int
var transitionable_states: Dictionary = {}
var scene_fader_is_complete = false
var required_for_state_is_complete = false
var loading_next_state_already = false
var connected_to_container_loading_state = false
var started_background_loading = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    var counter = 0
    for state in PM.States:
        match state.STATE_ID:
            "BluetoothStatus":
                transitionable_states[state.STATE_ID] = counter
        counter += 1
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_fine_access_location_permission"), self, 
        "check_if_permission_granted")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "get_location_permission")
    if PM.Settings.Session.get_data("Container_loading_state") != "idle":
        connected_to_container_loading_state = true
        PM.Settings.Session.connect(PM.Settings.Session.monitor_data("Container_loading_state"
                ), self, "check_scene_fader_is_complete")
    else:
        scene_fader_is_complete = true
        get_location_permission(null)
        check_if_permission_granted(PM.Settings.Session.get_data("fi_fine_access_location_permission"))
        while PM.get_tree().root.get_node("Container/Camera/TopLayer/SceneFader"
                ).AnimPlayer.is_playing():
            yield(PM.get_tree(), "idle_frame")
        PM.FreecoiLInterface.request_fine_access_permission()

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("fi_fine_access_location_permission"
            ), self, "check_if_permission_granted")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"
            ), self, "get_location_permission")
    if connected_to_container_loading_state:
        PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("Container_loading_state"
                ), self, "check_scene_fader_is_complete")
        call_deferred("cleanup")
    
func check_scene_fader_is_complete(loading_state):
    if loading_state == "idle":
        scene_fader_is_complete = true
        check_if_permission_granted(PM.Settings.Session.get_data("fi_fine_access_location_permission"))

func get_location_permission(_current_screen):
    if scene_fader_is_complete:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func check_if_permission_granted(granted):
    if granted:
        if not loading_next_state_already:
            loading_next_state_already = true
            background_load_state_ahead()
    else:
        get_location_permission(null)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        PM.background_load_resource("res://scenes/MainScreens/LocationPermission.tscn", 
                self, "setup_required_for_state")
    
func setup_required_for_state(background_results):
    if not required_for_state_is_complete:
        var results = background_results[PM.loading_thread_num]
        if results == "finished":
            PM.disconnect_background_load_resource(self, "setup_required_for_state")
            var loaded_scene = PM.Helpers.get_loaded_resource_from_background(
                    PM.loading_thread_num).instance()
            my_screen_offset = loaded_scene.editor_description
            my_screen_name = loaded_scene.name
            PM.get_tree().root.get_node("Container").current_scene.add_child(
                    loaded_scene)
            yield(PM.get_tree(), "idle_frame")
            required_for_state_is_complete = true

func background_load_state_ahead():
    PM.States[transitionable_states["BluetoothStatus"]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states["BluetoothStatus"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states["BluetoothStatus"])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
