extends Resource

var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "TestLineEdit"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var loading_next_state_already = false
var started_background_loading = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    var counter = 0
    for state in PM.States:
        match state.STATE_ID:
            "Template":
                transitionable_states[state.STATE_ID] = counter
        counter += 1
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    screen_change(null)

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value != null:
        pass

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        PM.background_load_resource("res://scenes/Tests/LineEditTest.tscn", self, "setup_required_for_state")
    
func setup_required_for_state(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_required_for_state")
        var loaded_scene = PM.Helpers.get_loaded_resource_from_background(
                PM.loading_thread_num).instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        required_for_state_is_complete = true

func background_load_state_ahead():
    PM.States[transitionable_states["Template"]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states["Template"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states["Template"])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
