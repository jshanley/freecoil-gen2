extends Resource
# alpha, beta, rc, dev
const VERSION = "0.3.2-dev4"
#Major.Minor.Micro
const VERSION_INT = 3  # Increment any time the micro changes. 
# 2 was 0.3.1
const DEBUG_LEVELS = ["not_set", "debug", "info", "warning", "error", "critical", "testing"]
const USER_DIR = "user://"
const GAME_NAME = "FreecoiL"
const SETTINGS_VERSION = 0
const UDP_BROADCAST_GREETING = "Want to play " + GAME_NAME + "?"
const UDP_BROADCAST_HOST = "I am hosting " + GAME_NAME + "!"
const NETWORK_BROADCAST_LAN_PORT = 8808
const NETWORK_LAN_PORT = 8818
const MAX_PLAYERS = 62
const MIN_PLAYERS = 2
const MAX_OBSERVERS = 1
const __MAX_SIGNALS = 255  #  + 1 is the real max because S0 is a possible signal.

# 0=Rebuild Boot Splash Screen, 1=Print all Log Entries, 3=Typical Release Level.
#DEBUG_LEVELS = [0 = "not_set", 1 = "debug", 2 = "info", 3 = "warning", 
#                4 = "error", 5 = "critical", 6 = "testing" # Straight to Debug output for testing only.]
var DEBUG_LEVEL = 2
var DEBUG_GUI = false
var max_threads: int = 1
var OperatingSystem: String
# Feature TOGGLEs BELOW: debug_toggles should be specific to debugging. 
#                        experimental_toggles are for new features not yet part of stable.
#                        feature_toggles are for features that just became stable and may 
#                               need to be turned off if they cause problems
const FLAG_ALLOW_SINGLE_PLAYER = true
const FLAG_ALLOW_SELF_KILL = true
var debug_toggles = {"TestLineEdit": false , "self_host_ok": true,
        #"ApplicationTest": "res://tests/integration/ServerClientPregame/ClientPregameTest.tscn",
        }
var experimental_toggles = {"gun_test_screen": false, "gun_names": false, 
        "GameSettingsReview": false}
var feature_toggles = {}
# warning-ignore:unused_class_variable
var __signals_used = -1
# warning-ignore:unused_class_variable
var Testing = Data.new("Testing", null, false, false, false)
# warning-ignore:unused_class_variable
var Preferences = Data.new("Preferences", USER_DIR + "Preferences.json", true, true, false)
# warning-ignore:unused_class_variable
var InGame = Data.new("InGame", USER_DIR + "InGame.json", false, true, true)
# warning-ignore:unused_class_variable
var Session = Data.new("Session", null, false, false, false)
# warning-ignore:unused_class_variable
var Network = Data.new("Network", null, false, false, true)
# warning-ignore:unused_class_variable

func Log(to_print, level="debug"):
    if DEBUG_LEVELS.find(level) >= DEBUG_LEVEL:
        print(to_print)
        if DEBUG_GUI:
            PM.get_tree().call_group("DebugOutput", "put", to_print)
        elif level == "testing":
            PM.get_tree().call_group("DebugOutput", "put", to_print)
            
class Data:
    var __settings = {}  # {"dummy": [1234, "S00"]}
    var auto_save = false
    var __additional_save = false
    var __save_thread = null
    var settings_path
    var network_sync
    var name
    var load_on_ready = false
    
    func _init(set_name, path=null, load_on_init=false, set_auto_save=false, set_network_sync=false):
        name = set_name
        settings_path = path
        auto_save = set_auto_save
        network_sync = set_network_sync
        if load_on_init:
            load_on_ready = load_on_init
            
    func loading_on_ready():
        if load_on_ready:
            var _result = load_settings()
            if _result != OK:
                register_data("SETTINGS_VERSION", SETTINGS_VERSION, false)
        else:
            register_data("SETTINGS_VERSION", SETTINGS_VERSION, false)
        
    func set_data(data_name, new_val, called_by_sync=false, emit_a_signal=true):
        #FIXME: Swap the order of called_by_sync=false, emit_a_signal=true.
        if not __settings.has(data_name):
            register_data(data_name, new_val)
            PM.Settings.Log("Settings: set_data(): " + self.name + ": " + str(data_name) + 
                        "(SignalName: " + str(__settings[data_name][1]) + ") = " + 
                        str(new_val), "debug")
        else:
            PM.Settings.Log("Settings: set_data(): " + self.name + ": " + str(data_name) + 
                        "(SignalName: " + str(__settings[data_name][1]) + ") = " + 
                        str(new_val), "debug")
            __settings[data_name][0] = new_val
        if emit_a_signal:
            emit_signal(__settings[data_name][1], __settings[data_name][0])
        if auto_save:
            save_settings()
        if network_sync:
            if not called_by_sync:
                if PM.get_tree().get_network_peer() != null:
                    PM.get_tree().call_group("Network", "rpc", "sync_var", name,
                        data_name, __settings[data_name][0])
        
    func get_data(data_name):
        if __settings.has(data_name):
            return __settings[data_name][0]
        else:
            return null
        
    func register_data(data_name, new_val, add_signal=true):
        if __settings.has(data_name):  # Duplicate registration
            # Set the data to the new_value but don't emit a signal.
            __settings[data_name][0] = new_val
            if auto_save:
                save_settings()
            return __settings[data_name][1]  # return the signal to listen for.
        else:
            if add_signal:
                PM.Settings.__signals_used += 1
                if PM.Settings.__signals_used > PM.Settings.__MAX_SIGNALS:
                    PM.Settings.Log("Error Out of Signals: PM.Settings Autoload: Max Data Signals exhausted! "
                            + "Please add more or reduce the amount of PM.Settings. " + 
                            "Max signals = " + str(PM.Settings.__MAX_SIGNALS + 1), "critical")
                    PM.Helpers.get_tree().quit()
                __settings[data_name] = [new_val, "Sig" + str(PM.Settings.__signals_used)]
            else:
                __settings[data_name] = [new_val, "Sig00"]
            if auto_save:
                save_settings()
            return __settings[data_name][1]  # return signal to listen too.
            
    func monitor_data(data_name):
        if __settings.has(data_name):
            return __settings[data_name][1]  # return signal to monitor.
        else:
            return register_data(data_name, null)

    func save_settings():
        if __save_thread == null:
            __save_thread = Thread.new()
            call_deferred("__deferred_save")
        else:
            __additional_save = true
                
    func __deferred_save():
        __save_thread.start(self, "__threaded_save")
        
    func __threaded_save(__):
        var file = File.new()
        file.open(settings_path, file.WRITE)
        file.store_string(to_json(__settings))
        file.close()
        call_deferred("__call_cleanup")
        
    func __call_cleanup():
        call_deferred("__cleanup_thread")
        
    func __cleanup_thread():
        __save_thread.wait_to_finish()
        __save_thread = null
        if __additional_save:
            __additional_save = false
            save_settings()

    func load_settings():
        var file = File.new()
        if file.file_exists(settings_path):
            file.open(settings_path, file.READ)
            var text = file.get_as_text()
            var temp = parse_json(text)
            file.close()
            if temp != null:
                if temp.has("SETTINGS_VERSION"):
                    if temp["SETTINGS_VERSION"][0] != SETTINGS_VERSION:
                        PM.Settings.Log("SETTINGS_VERSION mismatch. Current Program SETTINGS_VERSION = "
                                + str(SETTINGS_VERSION) +
                                "  | Saved SETTINGS_VERSION = " + str(__settings["SETTINGS_VERSION"]), "debug")
                        # We actually just ignore any lower version PM.Settings, so the 
                        # upgrade is by way of a over-write. Run Upgrades as needed.
                        # Downgrades should autofail, unless we find a need for that usecase.
                else:  
                    # We also need to check that we don't drop the existing PM.Settings, 
                    # just because they are not in the newly loaded data.
                    for data_name in temp:
                        # We need to re-map the signals.
                        if data_name != "SETTINGS_VERSION":  # else pass
                            PM.Settings.__signals_used += 1
                            if PM.Settings.__signals_used > PM.Settings.__MAX_SIGNALS:
                                PM.Settings.Log("Error Out of Signals: PM.Settings Autoload: " +
                                        "Max Data Signals exhausted! " +
                                        "Please add more or reduce the amount of PM.Settings. " + 
                                        "Max signals = " + str(PM.Settings.__MAX_SIGNALS + 1), "critical")
                                PM.Helpers.get_tree().quit()
                            PM.Settings.Log("Settings: load_settings(): " + self.name + ": " + str(data_name) + 
                                "(SignalName: " + "Sig" + str(PM.Settings.__signals_used) + ") = " + 
                                str(temp[data_name][0]), "debug")
                            __settings[data_name] = [temp[data_name][0], "Sig" + str(PM.Settings.__signals_used)]
        return OK
    
    func sync_peer(target_peer):
        if network_sync:
            if PM.get_tree().get_network_peer() != null:
                for setting in __settings:
                    PM.get_tree().call_group("Network", "rpc_id", target_peer, 
                            "sync_var", name, setting, __settings[setting][0])

    ######## BEGIN SIGNALS ########
    # warning-ignore:unused_signal
    signal Sig00
    # warning-ignore:unused_signal
    signal Sig0
    # warning-ignore:unused_signal
    signal Sig1
    # warning-ignore:unused_signal
    signal Sig2
    # warning-ignore:unused_signal
    signal Sig3
    # warning-ignore:unused_signal
    signal Sig4
    # warning-ignore:unused_signal
    signal Sig5
    # warning-ignore:unused_signal
    signal Sig6
    # warning-ignore:unused_signal
    signal Sig7
    # warning-ignore:unused_signal
    signal Sig8
    # warning-ignore:unused_signal
    signal Sig9
    # warning-ignore:unused_signal
    signal Sig10
    # warning-ignore:unused_signal
    signal Sig11
    # warning-ignore:unused_signal
    signal Sig12
    # warning-ignore:unused_signal
    signal Sig13
    # warning-ignore:unused_signal
    signal Sig14
    # warning-ignore:unused_signal
    signal Sig15
    # warning-ignore:unused_signal
    signal Sig16
    # warning-ignore:unused_signal
    signal Sig17
    # warning-ignore:unused_signal
    signal Sig18
    # warning-ignore:unused_signal
    signal Sig19
    # warning-ignore:unused_signal
    signal Sig20
    # warning-ignore:unused_signal
    signal Sig21
    # warning-ignore:unused_signal
    signal Sig22
    # warning-ignore:unused_signal
    signal Sig23
    # warning-ignore:unused_signal
    signal Sig24
    # warning-ignore:unused_signal
    signal Sig25
    # warning-ignore:unused_signal
    signal Sig26
    # warning-ignore:unused_signal
    signal Sig27
    # warning-ignore:unused_signal
    signal Sig28
    # warning-ignore:unused_signal
    signal Sig29
    # warning-ignore:unused_signal
    signal Sig30
    # warning-ignore:unused_signal
    signal Sig31
    # warning-ignore:unused_signal
    signal Sig32
    # warning-ignore:unused_signal
    signal Sig33
    # warning-ignore:unused_signal
    signal Sig34
    # warning-ignore:unused_signal
    signal Sig35
    # warning-ignore:unused_signal
    signal Sig36
    # warning-ignore:unused_signal
    signal Sig37
    # warning-ignore:unused_signal
    signal Sig38
    # warning-ignore:unused_signal
    signal Sig39
    # warning-ignore:unused_signal
    signal Sig40
    # warning-ignore:unused_signal
    signal Sig41
    # warning-ignore:unused_signal
    signal Sig42
    # warning-ignore:unused_signal
    signal Sig43
    # warning-ignore:unused_signal
    signal Sig44
    # warning-ignore:unused_signal
    signal Sig45
    # warning-ignore:unused_signal
    signal Sig46
    # warning-ignore:unused_signal
    signal Sig47
    # warning-ignore:unused_signal
    signal Sig48
    # warning-ignore:unused_signal
    signal Sig49
    # warning-ignore:unused_signal
    signal Sig50
    # warning-ignore:unused_signal
    signal Sig51
    # warning-ignore:unused_signal
    signal Sig52
    # warning-ignore:unused_signal
    signal Sig53
    # warning-ignore:unused_signal
    signal Sig54
    # warning-ignore:unused_signal
    signal Sig55
    # warning-ignore:unused_signal
    signal Sig56
    # warning-ignore:unused_signal
    signal Sig57
    # warning-ignore:unused_signal
    signal Sig58
    # warning-ignore:unused_signal
    signal Sig59
    # warning-ignore:unused_signal
    signal Sig60
    # warning-ignore:unused_signal
    signal Sig61
    # warning-ignore:unused_signal
    signal Sig62
    # warning-ignore:unused_signal
    signal Sig63
    # warning-ignore:unused_signal
    signal Sig64
    # warning-ignore:unused_signal
    signal Sig65
    # warning-ignore:unused_signal
    signal Sig66
    # warning-ignore:unused_signal
    signal Sig67
    # warning-ignore:unused_signal
    signal Sig68
    # warning-ignore:unused_signal
    signal Sig69
    # warning-ignore:unused_signal
    signal Sig70
    # warning-ignore:unused_signal
    signal Sig71
    # warning-ignore:unused_signal
    signal Sig72
    # warning-ignore:unused_signal
    signal Sig73
    # warning-ignore:unused_signal
    signal Sig74
    # warning-ignore:unused_signal
    signal Sig75
    # warning-ignore:unused_signal
    signal Sig76
    # warning-ignore:unused_signal
    signal Sig77
    # warning-ignore:unused_signal
    signal Sig78
    # warning-ignore:unused_signal
    signal Sig79
    # warning-ignore:unused_signal
    signal Sig80
    # warning-ignore:unused_signal
    signal Sig81
    # warning-ignore:unused_signal
    signal Sig82
    # warning-ignore:unused_signal
    signal Sig83
    # warning-ignore:unused_signal
    signal Sig84
    # warning-ignore:unused_signal
    signal Sig85
    # warning-ignore:unused_signal
    signal Sig86
    # warning-ignore:unused_signal
    signal Sig87
    # warning-ignore:unused_signal
    signal Sig88
    # warning-ignore:unused_signal
    signal Sig89
    # warning-ignore:unused_signal
    signal Sig90
    # warning-ignore:unused_signal
    signal Sig91
    # warning-ignore:unused_signal
    signal Sig92
    # warning-ignore:unused_signal
    signal Sig93
    # warning-ignore:unused_signal
    signal Sig94
    # warning-ignore:unused_signal
    signal Sig95
    # warning-ignore:unused_signal
    signal Sig96
    # warning-ignore:unused_signal
    signal Sig97
    # warning-ignore:unused_signal
    signal Sig98
    # warning-ignore:unused_signal
    signal Sig99
    # warning-ignore:unused_signal
    signal Sig100
    # warning-ignore:unused_signal
    signal Sig101
    # warning-ignore:unused_signal
    signal Sig102
    # warning-ignore:unused_signal
    signal Sig103
    # warning-ignore:unused_signal
    signal Sig104
    # warning-ignore:unused_signal
    signal Sig105
    # warning-ignore:unused_signal
    signal Sig106
    # warning-ignore:unused_signal
    signal Sig107
    # warning-ignore:unused_signal
    signal Sig108
    # warning-ignore:unused_signal
    signal Sig109
    # warning-ignore:unused_signal
    signal Sig110
    # warning-ignore:unused_signal
    signal Sig111
    # warning-ignore:unused_signal
    signal Sig112
    # warning-ignore:unused_signal
    signal Sig113
    # warning-ignore:unused_signal
    signal Sig114
    # warning-ignore:unused_signal
    signal Sig115
    # warning-ignore:unused_signal
    signal Sig116
    # warning-ignore:unused_signal
    signal Sig117
    # warning-ignore:unused_signal
    signal Sig118
    # warning-ignore:unused_signal
    signal Sig119
    # warning-ignore:unused_signal
    signal Sig120
    # warning-ignore:unused_signal
    signal Sig121
    # warning-ignore:unused_signal
    signal Sig122
    # warning-ignore:unused_signal
    signal Sig123
    # warning-ignore:unused_signal
    signal Sig124
    # warning-ignore:unused_signal
    signal Sig125
    # warning-ignore:unused_signal
    signal Sig126
    # warning-ignore:unused_signal
    signal Sig127
    # warning-ignore:unused_signal
    signal Sig128
    # warning-ignore:unused_signal
    signal Sig129
    # warning-ignore:unused_signal
    signal Sig130
    # warning-ignore:unused_signal
    signal Sig131
    # warning-ignore:unused_signal
    signal Sig132
    # warning-ignore:unused_signal
    signal Sig133
    # warning-ignore:unused_signal
    signal Sig134
    # warning-ignore:unused_signal
    signal Sig135
    # warning-ignore:unused_signal
    signal Sig136
    # warning-ignore:unused_signal
    signal Sig137
    # warning-ignore:unused_signal
    signal Sig138
    # warning-ignore:unused_signal
    signal Sig139
    # warning-ignore:unused_signal
    signal Sig140
    # warning-ignore:unused_signal
    signal Sig141
    # warning-ignore:unused_signal
    signal Sig142
    # warning-ignore:unused_signal
    signal Sig143
    # warning-ignore:unused_signal
    signal Sig144
    # warning-ignore:unused_signal
    signal Sig145
    # warning-ignore:unused_signal
    signal Sig146
    # warning-ignore:unused_signal
    signal Sig147
    # warning-ignore:unused_signal
    signal Sig148
    # warning-ignore:unused_signal
    signal Sig149
    # warning-ignore:unused_signal
    signal Sig150
    # warning-ignore:unused_signal
    signal Sig151
    # warning-ignore:unused_signal
    signal Sig152
    # warning-ignore:unused_signal
    signal Sig153
    # warning-ignore:unused_signal
    signal Sig154
    # warning-ignore:unused_signal
    signal Sig155
    # warning-ignore:unused_signal
    signal Sig156
    # warning-ignore:unused_signal
    signal Sig157
    # warning-ignore:unused_signal
    signal Sig158
    # warning-ignore:unused_signal
    signal Sig159
    # warning-ignore:unused_signal
    signal Sig160
    # warning-ignore:unused_signal
    signal Sig161
    # warning-ignore:unused_signal
    signal Sig162
    # warning-ignore:unused_signal
    signal Sig163
    # warning-ignore:unused_signal
    signal Sig164
    # warning-ignore:unused_signal
    signal Sig165
    # warning-ignore:unused_signal
    signal Sig166
    # warning-ignore:unused_signal
    signal Sig167
    # warning-ignore:unused_signal
    signal Sig168
    # warning-ignore:unused_signal
    signal Sig169
    # warning-ignore:unused_signal
    signal Sig170
    # warning-ignore:unused_signal
    signal Sig171
    # warning-ignore:unused_signal
    signal Sig172
    # warning-ignore:unused_signal
    signal Sig173
    # warning-ignore:unused_signal
    signal Sig174
    # warning-ignore:unused_signal
    signal Sig175
    # warning-ignore:unused_signal
    signal Sig176
    # warning-ignore:unused_signal
    signal Sig177
    # warning-ignore:unused_signal
    signal Sig178
    # warning-ignore:unused_signal
    signal Sig179
    # warning-ignore:unused_signal
    signal Sig180
    # warning-ignore:unused_signal
    signal Sig181
    # warning-ignore:unused_signal
    signal Sig182
    # warning-ignore:unused_signal
    signal Sig183
    # warning-ignore:unused_signal
    signal Sig184
    # warning-ignore:unused_signal
    signal Sig185
    # warning-ignore:unused_signal
    signal Sig186
    # warning-ignore:unused_signal
    signal Sig187
    # warning-ignore:unused_signal
    signal Sig188
    # warning-ignore:unused_signal
    signal Sig189
    # warning-ignore:unused_signal
    signal Sig190
    # warning-ignore:unused_signal
    signal Sig191
    # warning-ignore:unused_signal
    signal Sig192
    # warning-ignore:unused_signal
    signal Sig193
    # warning-ignore:unused_signal
    signal Sig194
    # warning-ignore:unused_signal
    signal Sig195
    # warning-ignore:unused_signal
    signal Sig196
    # warning-ignore:unused_signal
    signal Sig197
    # warning-ignore:unused_signal
    signal Sig198
    # warning-ignore:unused_signal
    signal Sig199
    # warning-ignore:unused_signal
    signal Sig200
    # warning-ignore:unused_signal
    signal Sig201
    # warning-ignore:unused_signal
    signal Sig202
    # warning-ignore:unused_signal
    signal Sig203
    # warning-ignore:unused_signal
    signal Sig204
    # warning-ignore:unused_signal
    signal Sig205
    # warning-ignore:unused_signal
    signal Sig206
    # warning-ignore:unused_signal
    signal Sig207
    # warning-ignore:unused_signal
    signal Sig208
    # warning-ignore:unused_signal
    signal Sig209
    # warning-ignore:unused_signal
    signal Sig210
    # warning-ignore:unused_signal
    signal Sig211
    # warning-ignore:unused_signal
    signal Sig212
    # warning-ignore:unused_signal
    signal Sig213
    # warning-ignore:unused_signal
    signal Sig214
    # warning-ignore:unused_signal
    signal Sig215
    # warning-ignore:unused_signal
    signal Sig216
    # warning-ignore:unused_signal
    signal Sig217
    # warning-ignore:unused_signal
    signal Sig218
    # warning-ignore:unused_signal
    signal Sig219
    # warning-ignore:unused_signal
    signal Sig220
    # warning-ignore:unused_signal
    signal Sig221
    # warning-ignore:unused_signal
    signal Sig222
    # warning-ignore:unused_signal
    signal Sig223
    # warning-ignore:unused_signal
    signal Sig224
    # warning-ignore:unused_signal
    signal Sig225
    # warning-ignore:unused_signal
    signal Sig226
    # warning-ignore:unused_signal
    signal Sig227
    # warning-ignore:unused_signal
    signal Sig228
    # warning-ignore:unused_signal
    signal Sig229
    # warning-ignore:unused_signal
    signal Sig230
    # warning-ignore:unused_signal
    signal Sig231
    # warning-ignore:unused_signal
    signal Sig232
    # warning-ignore:unused_signal
    signal Sig233
    # warning-ignore:unused_signal
    signal Sig234
    # warning-ignore:unused_signal
    signal Sig235
    # warning-ignore:unused_signal
    signal Sig236
    # warning-ignore:unused_signal
    signal Sig237
    # warning-ignore:unused_signal
    signal Sig238
    # warning-ignore:unused_signal
    signal Sig239
    # warning-ignore:unused_signal
    signal Sig240
    # warning-ignore:unused_signal
    signal Sig241
    # warning-ignore:unused_signal
    signal Sig242
    # warning-ignore:unused_signal
    signal Sig243
    # warning-ignore:unused_signal
    signal Sig244
    # warning-ignore:unused_signal
    signal Sig245
    # warning-ignore:unused_signal
    signal Sig246
    # warning-ignore:unused_signal
    signal Sig247
    # warning-ignore:unused_signal
    signal Sig248
    # warning-ignore:unused_signal
    signal Sig249
    # warning-ignore:unused_signal
    signal Sig250
    # warning-ignore:unused_signal
    signal Sig251
    # warning-ignore:unused_signal
    signal Sig252
    # warning-ignore:unused_signal
    signal Sig253
    # warning-ignore:unused_signal
    signal Sig254
    # warning-ignore:unused_signal
    signal Sig255
        ######## END SIGNALS ########
    
func initialize_settings():
    print(GAME_NAME + ": Version: " + VERSION + " = Integer Version Number: " 
        + str(VERSION_INT))
    Testing.loading_on_ready()
    Preferences.loading_on_ready()
    InGame.loading_on_ready()
    Session.loading_on_ready()
    Network.loading_on_ready()
    Session.set_data("experimental_toggles", experimental_toggles)
