extends Resource
# Technically this extends Resource, but that is the default if empty.

var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "PlayerAvatarSelector"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var loading_next_state_already = false
var started_background_loading = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String
var scene_loaded = false

var loaded_script
var thread_num:int
var thread_numbers:Array = []
var file_paths: Dictionary = {}

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "info")
    load_required_for_state()
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Preferences.connect(PM.Settings.Preferences.monitor_data(
            "player_avatar"), self, "avatar_selected")
    screen_change(null)

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Preferences.disconnect(PM.Settings.Preferences.monitor_data(
            "player_avatar"), self, "avatar_selected")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value == true:
        call_deferred("background_load_state_ahead")
    else:
        pass
        
func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID:
                "PlayerProfileEditor":
                    transitionable_states[state.STATE_ID] = counter
                "OfflineSettings":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        var player_avatar = PM.Settings.Preferences.get_data("player_avatar")
        if player_avatar != null and player_avatar != "" and \
                PM.Settings.Session.get_data("scene_routing_indicator") != "OfflineSettings":
            next_state = "PlayerProfileEditor"
            transition_to_next_state()
        else:
            PM.background_load_resource("res://scenes/MainScreens/PlayerAvatarSelector.tscn", 
                        self, "setup_required_for_state")
            scene_loaded = true
    
func setup_required_for_state(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_required_for_state")
        var loaded_scene = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        required_for_state_is_complete = true
        screen_change(null)
        call_deferred("load_image_selection_script")
        print("done loading images")

func background_load_state_ahead():
    if PM.Settings.Session.get_data("scene_routing_indicator") == "OfflineSettings":
        PM.Settings.Session.set_data("scene_routing_indicator", "")
        next_state = "OfflineSettings"
    else:
        next_state = "PlayerProfileEditor"
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func load_image_selection_script():
    thread_num = PM.Helpers.background_loader_n_callback("res://Logic/scenes/" +
            "MainScreens/ProfileAvatarEdit/ProfileAvatarImageSelectionButton.gd", 
            self, "load_all_avatars_in_background")

func load_all_avatars_in_background(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        loaded_script = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "load_all_avatars_in_background")
        var dir = Directory.new()
        dir.open("res://assets/images/avatars/")
        dir.list_dir_begin()
        while true:
            var file = dir.get_next()
            if file == "":
                break
            elif not file.begins_with("."):
                if "avatar_000" in file:
                    pass
                elif "avatar_000_w" in file:
                    pass
                elif "avatar_005_gray" in file:
                    pass
                else:
                    # https://github.com/godotengine/godot/issues/14562
                    if file.matchn("*.png"):
                        pass  # If they are real .png files instead of 
                        #.png.import then they are the real files that are only
                        # on the development side in Godot editor.
                    elif file.matchn("*.png.import"):
                        file = file.replace(".png.import", ".png")
                        var file_path = "res://assets/images/avatars/" + file
                        thread_numbers.append(PM.Helpers.background_loader_n_callback(
                                file_path, self, "setup_avatar_btns"))
                        file_paths[thread_numbers[-1]] = file_path
                    
func setup_avatar_btns(background_results):
    for thread in thread_numbers:
        if background_results.has(thread):
            var results = background_results[thread]
            if results == "finished":
                    var avatar_button = NinePatchRect.new()
                    var resource = PM.Helpers.get_loaded_resource_from_background(thread)
                    avatar_button.texture = resource
                    avatar_button.set_script(loaded_script)
                    avatar_button.rect_min_size = Vector2(120, 120)
                    avatar_button.rect_size = Vector2(120, 120)
                    # This would not work with out the MOUSE_FILTER being set correctly, see below.
# https://docs.godotengine.org/en/stable/classes/class_control.html#class-control-method-gui-input
                    avatar_button.mouse_filter = Button.MOUSE_FILTER_PASS
                    var grid = PM.get_tree().root.get_node("Container").current_scene.get_node(
                            "PlayerAvatarEdit/CenterContainer/VBoxContainer/ScrollContainer/GridContainer")
                    grid.add_child(avatar_button)
                    avatar_button.avatar_file_path = file_paths[thread]
                    thread_numbers.erase(thread)

func avatar_selected(_avatar_path):
    pass

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    if scene_loaded:
        PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""

# This is now the prefered method of transitioning to the next state.  Transitions the entire State Class.
func transition_to_next_state():  
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])
