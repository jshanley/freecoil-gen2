extends Resource

var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "AssignWeapon"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var loading_next_state_already = false
var started_background_loading = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String

var WpnFoundBtnScript
var GunSprite: Sprite
var MuzzleflashSprite: Sprite
var WpnGrid: GridContainer
var WpnNameEdit: LineEdit
var ConnectStatus: Label
var ConnectWpnBtn: Button
var FireWpnBtn: Button
var DisconnectWpnBtn: Button
var PlayWithoutBtn: Button
var ContinueBtn: Button
var WeaponNameContainer
var WeaponGridContainer

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_weapons_found"), self, "update_weapons_found_list")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_weapon_highlighted"), self, "weapon_highlighted")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_laser_is_connected"), self, "weapon_connected")
    #PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_laser_is_connected"), self, "weapon_connected")
    screen_change(null)
    ConnectStatus.text = "Weapon is Not Connected"
    call_deferred("trigger_weapons_found_pruner")

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("fi_weapons_found"), self, "update_weapons_found_list")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("fi_weapon_highlighted"), self, "weapon_highlighted")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("fi_laser_is_connected"), self, "weapon_connected")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value == "Continue":
        background_load_state_ahead()

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID:
                "PlayerNameEditor":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        PM.FreecoiLInterface.start_bt_scan()
        PM.background_load_resource("res://scenes/MainScreens/AssignWeapon.tscn", self, "setup_assign_wpn_screen")
        
func setup_assign_wpn_screen(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_assign_wpn_screen")
        var loaded_scene = PM.Helpers.get_loaded_resource_from_background(
                PM.loading_thread_num).instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        call_deferred("load_wpn_found_btn_script")
    
func load_wpn_found_btn_script():
    PM.background_load_resource("res://Logic/scenes/MainScreens/AssignWeapon/WpnFoundBtn.gd", 
            self, "setup_required_for_state")

func setup_required_for_state(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_required_for_state")
        WpnFoundBtnScript = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        GunSprite = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/GunImage")
        MuzzleflashSprite = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/MuzzleflashImage")
        WpnGrid = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node(
                "AssignWeapon/VBoxContainer/VBoxContainer/VBoxContainer2/" +
                "ScrollContainer/GridContainer")
        WpnNameEdit = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node(
                "AssignWeapon/VBoxContainer/VBoxContainer/VBoxContainer2/" + 
                "HBoxContainer/WeaponName")
        ConnectStatus = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node(
                "AssignWeapon/VBoxContainer/VBoxContainer2/ConnectionStatus")
        ConnectWpnBtn = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer2/HBoxContainer/Connect")
        FireWpnBtn = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer2/HBoxContainer/Fire")
        DisconnectWpnBtn = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer2/HBoxContainer/Disconnect")
        PlayWithoutBtn = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer2/HBoxContainer2/PlayWithout")
        ContinueBtn = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer2/HBoxContainer2/Continue")
        WeaponNameContainer = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer/VBoxContainer2/HBoxContainer")
        WeaponGridContainer = PM.get_tree().root.get_node("Container"
                ).current_scene.get_node("AssignWeapon/VBoxContainer/" +
                "VBoxContainer/VBoxContainer2/ScrollContainer/GridContainer")
        MuzzleflashSprite.modulate = ("ff0000") #Red
        weapon_connected(0)
        required_for_state_is_complete = true

func background_load_state_ahead():
    PM.States[transitionable_states["PlayerNameEditor"]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states["PlayerNameEditor"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states["PlayerNameEditor"])
    
func update_weapons_found_list(current_weapons_found_list):
    # TODO: Note how far down the scroll list we are.
    var highlighted_weapon = PM.Settings.Session.get_data("fi_weapon_highlighted")
    if current_weapons_found_list == null:
        for child in WpnGrid.get_children():
            child.queue_free()
    else:
        var kept_weapon_list: Array = []
        for child in WpnGrid.get_children():
            if current_weapons_found_list.has(child.get_meta("name")):
                kept_weapon_list.append(child.get_meta("name"))
            else:
                child.queue_free()
        for weapon in current_weapons_found_list:
            if kept_weapon_list == null or not kept_weapon_list.has(weapon):
                var new_btn = Button.new()
                new_btn.set_meta("name", weapon)
                new_btn.editor_description = weapon
                new_btn.text = weapon
                new_btn.rect_min_size.y = 80
                new_btn.anchor_left = 0.5
                new_btn.anchor_right = 0.5
                new_btn.rect_min_size.x = 480
                new_btn.mouse_filter = Button.MOUSE_FILTER_PASS
                new_btn.set_script(WpnFoundBtnScript)
                WpnGrid.add_child(new_btn)
        for child in WpnGrid.get_children():
            if child.get_meta("name") == highlighted_weapon:
                if child.selected == false:
                    child.selected = true
                    child.modulate = Color(1, 0.35, 0, 1)

func trigger_weapons_found_pruner():
    var timer = OS.get_unix_time()
    while true:
        yield(PM.get_tree(), "idle_frame")
        if OS.get_unix_time() - timer > 15:
            var weapons_found_dict = PM.Settings.Session.get_data("fi_weapons_found")
            weapons_found_dict = PM.FreecoiLInterface.prune_weapons_found(weapons_found_dict)
            PM.Settings.Session.set_data("fi_weapons_found", weapons_found_dict)
            timer = OS.get_unix_time()

func weapon_highlighted(weapon_real_name):
    WpnNameEdit.text = weapon_real_name
    WpnNameEdit.editable = true
    ConnectWpnBtn.disabled = false
    ConnectStatus.text = "Not Connected, Is Selected, Press Assign"

func weapon_connected(connection_state):
    if connection_state == 0: # Weapon is not (never) connected
        GunSprite.modulate = ("30ffffff") #Semitransparent/Disconnected
        WpnNameEdit.text = ""
        WpnNameEdit.editable = false
        ConnectStatus.text = "Weapon is Not Connected."
        ConnectWpnBtn.disabled = true
        ConnectWpnBtn.visible = true
        FireWpnBtn.visible = false
        DisconnectWpnBtn.visible = false
        var need_bluetooth = PM.Settings.Session.get_data("need_bluetooth")
        if need_bluetooth == false:
            PlayWithoutBtn.visible = true
        else:
            PlayWithoutBtn.visible = false
        ContinueBtn.visible = false
        WeaponNameContainer.visible = true
        WeaponGridContainer.visible = true
    elif connection_state == 2: # Weapon is connected
        WeaponNameContainer.visible = false
        WeaponGridContainer.visible = false
        GunSprite.modulate = Color("ffffff") #Opaque/Connected
        WpnNameEdit.text = ""
        WpnNameEdit.editable = false
        ConnectWpnBtn.visible = false
        FireWpnBtn.visible = true
        DisconnectWpnBtn.visible = true
        PlayWithoutBtn.visible = false
        ContinueBtn.visible = true

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
