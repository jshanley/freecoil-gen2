extends Resource
# Technically this extends Resource, but that is the default if empty.

var state_has_on_ready = true
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "Splash"
var state_num: int
var transitionable_states: Dictionary = {}

var start_pixel = Color(0,0,0,1)
var splash_screen_loaded = false
var wait_some_frames = 0
var LoadingOutput: Label
var LoadingSpinner
var continue_spinner_updates = true
var next_state: String
var next_text: String

var theme_setup_is_complete = false
var theme_loading_thread_num: int
var main_menu_empty_is_ready = false
var standard_header_is_ready = false

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    PM.Settings.Log("Program Manager: " + STATE_ID + " " + str(state_num), "info")

func populate_default_variables():
    var deathmatch_defaults = {"game_mode":"Deathmatch", "team_amount":0,
        "game_objective":"Kill Count", "game_objective_goal": 20, "game_length_time":30,
        "respawns_allowed":5, "respawn_type":"Timed", "respawn_delay":30,
        "initial_health":10, "initial_shield":5, "friendly_fire":false,
        "force_recoil_action":true, "indoor_game":false, "healing_radius":10,
        "heal_per_bandage":3, "healing_time":10, "initial_medpacks":8}
    var survival_defaults = {"game_mode":"Survival", "team_amount":0,
        "game_objective":"Death Count", "game_objective_goal": 0, "game_length_time":30,
        "respawns_allowed":5, "respawn_type":"Timed", "respawn_delay":10,
        "initial_health":10, "initial_shield":10, "friendly_fire":false,
        "force_recoil_action":true, "indoor_game":false, "healing_radius":10,
        "heal_per_bandage":3, "healing_time":10, "initial_medpacks":8}
    var freeforall_defaults = {"game_mode":"Free For All", "team_amount":0,
        "game_objective":"Kill Count", "game_objective_goal": 0, "game_length_time":30,
        "respawns_allowed":5, "respawn_type":"Timed", "respawn_delay":10,
        "initial_health":10, "initial_shield":10, "friendly_fire":false,
        "force_recoil_action":true, "indoor_game":false, "healing_radius":10,
        "heal_per_bandage":3, "healing_time":10, "initial_medpacks":8}
    var battleroyale_defaults = {"game_mode":"Battle Royale", "team_amount":0,
        "game_objective":"Kill Count", "game_objective_goal": 0, "game_length_time":30,
        "respawns_allowed":5, "respawn_type":"Timed", "respawn_delay":10,
        "initial_health":10, "initial_shield":10, "friendly_fire":false,
        "force_recoil_action":true, "indoor_game":false, "healing_radius":10,
        "heal_per_bandage":3, "healing_time":10, "initial_medpacks":8}
    var capturetheflag_defaults = {"game_mode":"Capture The Flag", "team_amount":2,
        "game_objective":"Flag Count", "game_objective_goal": 2, "game_length_time":45,
        "respawns_allowed":5, "respawn_type":"Timed", "respawn_delay":10,
        "initial_health":10, "initial_shield":10, "friendly_fire":false,
        "force_recoil_action":true, "indoor_game":false, "healing_radius":10,
        "heal_per_bandage":3, "healing_time":10, "initial_medpacks":8}
    var pregame_defaults = {"game_mode":"Free For All", "team_amount":0,
        "game_objective":"Kill Count", "game_objective_goal": 0, "game_length_time":PM.GODOT_MAX_INT,
        "respawns_allowed":PM.GODOT_MAX_INT, "respawn_type":"Timed", "respawn_delay":10,
        "initial_health":10, "initial_shield":5, "friendly_fire":true,
        "force_recoil_action":false, "indoor_game":true, "healing_radius":10,
        "heal_per_bandage":3, "healing_time":10, "initial_medpacks":8}
    PM.Settings.Session.set_data("deathmatch_settings", deathmatch_defaults)
    PM.Settings.Session.set_data("survival_settings", survival_defaults)
    PM.Settings.Session.set_data("freeforall_settings", freeforall_defaults)
    PM.Settings.Session.set_data("battleroyale_settings", battleroyale_defaults)
    PM.Settings.Session.set_data("capturetheflag_settings", capturetheflag_defaults)
    PM.Settings.Session.set_data("pregame_settings", pregame_defaults)

    #Preset Armory
    var preset_armory = {\
    "W1A":{"name":"Pistol L1", "magazine_size":7, "damage":1, "shot_modes": ["single"], "reload_speed":3, "rate_of_fire":0.3, "short_power":150, "long_power":15, "indoor_long_power":15},\
    "W1B":{"name":"Pistol L2", "magazine_size":15, "damage":1, "shot_modes": ["single"], "reload_speed":3, "rate_of_fire":0.3, "short_power":150, "long_power":15, "indoor_long_power":15},\
    "W1C":{"name":"Pistol L3", "magazine_size":15, "damage":1, "shot_modes": ["single"], "reload_speed":3, "rate_of_fire":0.3, "short_power":150, "long_power":15, "indoor_long_power":15},\
    "W1D":{"name":"Pistol L4", "magazine_size":30, "damage":1, "shot_modes": ["single", "burst"], "reload_speed":3, "rate_of_fire":0.3, "short_power":150, "long_power":15, "indoor_long_power":15},\
    "W2A":{"name":"Assault Rfl L1", "magazine_size":10, "damage":1, "shot_modes": ["single", "burst", "auto"], "reload_speed":4, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W2B":{"name":"Assault Rfl L2", "magazine_size":20, "damage":1, "shot_modes": ["single", "burst", "auto"], "reload_speed":4, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W2C":{"name":"Assault Rfl L3", "magazine_size":20, "damage":1, "shot_modes": ["single", "burst", "auto"], "reload_speed":4, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W2D":{"name":"Assault Rfl L4", "magazine_size":30, "damage":1, "shot_modes": ["single", "burst", "auto"], "reload_speed":4, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W3A":{"name":"Machine Gun L1", "magazine_size":50, "damage":1, "shot_modes": ["burst", "auto"], "reload_speed":8, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W3B":{"name":"Machine Gun L2", "magazine_size":80, "damage":1, "shot_modes": ["burst", "auto"], "reload_speed":8, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W3C":{"name":"Machine Gun L3", "magazine_size":150, "damage":1, "shot_modes": ["burst", "auto"], "reload_speed":6, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W3D":{"name":"Machine Gun L4", "magazine_size":150, "damage":1, "shot_modes": ["burst", "auto"], "reload_speed":5, "rate_of_fire":0.3, "short_power":60, "long_power":135, "indoor_long_power":45},\
    "W4A":{"name":"Sniper Rfl L1", "magazine_size":8, "damage":1, "shot_modes": ["single"], "reload_speed":6, "rate_of_fire":0.5, "short_power":0, "long_power":255, "indoor_long_power":45},\
    "W4B":{"name":"Sniper Rfl L2", "magazine_size":12, "damage":1, "shot_modes": ["single"], "reload_speed":5, "rate_of_fire":0.5, "short_power":0, "long_power":255, "indoor_long_power":45},\
    "W4C":{"name":"Sniper Rfl L3", "magazine_size":15, "damage":1, "shot_modes": ["single"], "reload_speed":5, "rate_of_fire":0.5, "short_power":0, "long_power":255, "indoor_long_power":45},\
    "W4D":{"name":"Sniper Rfl L4", "magazine_size":15, "damage":2, "shot_modes": ["single"], "reload_speed":5, "rate_of_fire":0.5, "short_power":0, "long_power":255, "indoor_long_power":45},\
    "W5A":{"name":"Shotgun L1", "magazine_size":2, "damage":2, "shot_modes": ["single"], "reload_speed":4, "rate_of_fire":0.5, "short_power":255, "long_power":0, "indoor_long_power":15},\
    "W5B":{"name":"Shotgun L2", "magazine_size":7, "damage":2, "shot_modes": ["single"], "reload_speed":7, "rate_of_fire":0.5, "short_power":255, "long_power":0, "indoor_long_power":15},\
    "W5C":{"name":"Shotgun L3", "magazine_size":10, "damage":2, "shot_modes": ["single"], "reload_speed":10, "rate_of_fire":0.5, "short_power":255, "long_power":0, "indoor_long_power":15},\
    "W5D":{"name":"Shotgun L4", "magazine_size":12, "damage":2, "shot_modes": ["single"], "reload_speed":12, "rate_of_fire":0.5, "short_power":255, "long_power":0, "indoor_long_power":15}}
    PM.Settings.Session.set_data("preset_armory", preset_armory)
    #Populate saved_armories for first use only
    var saved_armories
    if PM.Settings.Preferences.get_data("saved_armories") == null:
        saved_armories = {"Armory 1":preset_armory, "Armory 2":preset_armory, "Armory 3":preset_armory, "Armory 4":preset_armory}
        PM.Settings.Preferences.set_data("saved_armories", saved_armories)
    else:
        saved_armories = PM.Settings.Preferences.get_data("saved_armories")
    #Set the default_armory_name to the first Armory if not already set
    var default_armory_name
    if PM.Settings.Preferences.get_data("default_armory_name") == null or not\
            saved_armories.has(PM.Settings.Preferences.get_data("default_armory_name")):
        var keys = saved_armories.keys()
        default_armory_name = keys[0]
        PM.Settings.Preferences.set_data("default_armory_name", default_armory_name)
    #Populate player profile settings with defaults, if preference has not been defined
    PM.Settings.Session.set_data("recoil_action_default", true) #See Preferences doc for details (recoil_action)
    PM.Settings.Session.set_data("narrator_default", true) #See Preferences doc for details (narrator)
    PM.Settings.Session.set_data("hud_footer_default", 1)
    PM.Settings.Session.set_data("player_hit_indicator_duration_default", 3)
    if PM.Settings.Preferences.get_data("recoil_action") == null:
        PM.Settings.Preferences.set_data("recoil_action", PM.Settings.Session.get_data("recoil_action_default"))
    if PM.Settings.Preferences.get_data("narrator") == null:
        PM.Settings.Preferences.set_data("narrator", PM.Settings.Session.get_data("narrator_default"))
    if PM.Settings.Preferences.get_data("hud_footer") == null:
        PM.Settings.Preferences.set_data("hud_footer", PM.Settings.Session.get_data("hud_footer_default"))
    if PM.Settings.Preferences.get_data("player_hit_indicator_duration") == null:
        PM.Settings.Preferences.set_data("player_hit_indicator_duration", PM.Settings.Session.get_data("player_hit_indicator_duration_default"))

func on_exit() -> void:
    pass

func on_ready_event() -> int:
    call_deferred("detect_os_and_max_threads")
    return state_num

func on_process_event(_delta) -> int:
    return state_num

func detect_os_and_max_threads():
    print("OS = " + OS.get_name())
    print("Virtual Cores in CPU = " + str(OS.get_processor_count()))
    if OS.get_processor_count() - 1 < 1:
        PM.Settings.max_threads = 1
    else:
        PM.Settings.max_threads = OS.get_processor_count() - 1
    PM.Settings.OperatingSystem = OS.get_name()
    call_deferred("defered_loading")

func defered_loading():
    LoadingSpinner = PM.get_tree().root.get_node(
            "Container/Scene0/Splash/CenterContainer/VBoxContainer/LoadingSpinner")
    LoadingOutput = PM.get_tree().root.get_node(
            "Container/Scene0/Splash/CenterContainer/VBoxContainer/LoadingLabel")
    LoadingOutput.text = "Loading: Animated Loading Spinner..."
    PM.background_load_resource("res://assets/images/gifs/Spinner/spinner_spriteframes.tres", 
            self, "setup_loading_spinner")
   
func setup_loading_spinner(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: Animated Loading Spinner..."
        PM.disconnect_background_load_resource(self, "setup_loading_spinner")
        LoadingSpinner.frames = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        yield(PM.get_tree(), "idle_frame")
        LoadingSpinner.frame = 0
        LoadingSpinner.play("default")
        call_deferred("manually_advance_spinner")
        LoadingOutput.text = "Initializing: PM.Settings..."
        yield(PM.get_tree(), "idle_frame")
        PM.Settings.initialize_settings()
        populate_default_variables()
        PM.Settings.InGame.register_data("except_pause_disable_input", false)
        PM.Settings.Session.set_data("current_menu", "0,0")
        PM.Settings.Session.set_data("previous_menu", "0,0")
        PM.get_tree().call_group("Camera", "delayed_setup")
        call_deferred("background_load_logo")
        background_load_theme()

func manually_advance_spinner():
    var last_manual_frame = 0
    yield(PM.get_tree().create_timer(0.1), "timeout")
    while continue_spinner_updates:
        if LoadingSpinner.frame != last_manual_frame:
            continue_spinner_updates = false
            break
        if LoadingSpinner.frame == 3:
            LoadingSpinner.frame = 0
        else:
            LoadingSpinner.frame += 1
        last_manual_frame = LoadingSpinner.frame
        yield(PM.get_tree().create_timer(0.20), "timeout")

func background_load_theme():
    LoadingOutput.text = "Loading: Theme..."
    theme_loading_thread_num = PM.Helpers.background_loader_n_callback("res://assets/themes/tempRecoil.tres", 
            self, "setup_theme")

func setup_theme(background_results):
    var results = background_results[theme_loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: Theme..."
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(theme_loading_thread_num,
                self, "setup_theme")
        while not main_menu_empty_is_ready:
            yield(PM.get_tree(), "idle_frame")
        PM.get_tree().root.get_node("Container").current_scene.theme = temp
        while not standard_header_is_ready:
            yield(PM.get_tree(), "idle_frame")
        PM.get_tree().root.get_node("Container/UI/StandardHeader").theme = temp
        yield(PM.get_tree(), "idle_frame")
        theme_setup_is_complete = true

func background_load_logo():
        LoadingOutput.text = "Loading: FeralBytes Logo..."
        PM.background_load_resource("res://assets/images/logos/FeralBytes_logo.jpg", 
                self, "setup_feralbytes_logo")

func setup_feralbytes_logo(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: FeralBytes Logo..."
        PM.disconnect_background_load_resource(self, "setup_feralbytes_logo")
        var feralbytes_logo_ref = PM.get_tree().root.get_node(
                "Container/Scene0/Splash/CenterContainer/VBoxContainer/NinePatchRect2")
        if PM.Settings.DEBUG_LEVEL == 0:
            pass  # Not transparent and no fade in.
        else:
            feralbytes_logo_ref.modulate = Color(1,1,1,0)  # Transparent
            call_deferred("fade_in_logo", feralbytes_logo_ref)
        feralbytes_logo_ref.texture = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        
        call_deferred("load_freecoil_logo")

func fade_in_logo(logo_ref):
    var visibility = logo_ref.modulate
    while visibility.a < 1.0:
        visibility.a = visibility.a + 0.01
        if is_instance_valid(logo_ref):
            logo_ref.modulate = visibility
            yield(PM.get_tree(), "idle_frame")
        else:
            break
    
func load_freecoil_logo():
    LoadingOutput.text = "Loading: FreecoiL Logo..."
    PM.background_load_resource("res://assets/images/logos/Freecoil_Logo_black_big.jpg", 
            self, "setup_freecoil_logo")
    
func setup_freecoil_logo(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: FreecoiL Logo..."
        PM.disconnect_background_load_resource(self, "setup_freecoil_logo")
        var freecoil_logo_ref = PM.get_tree().root.get_node(
                "Container/Scene0/Splash/CenterContainer/VBoxContainer/NinePatchRect")
        freecoil_logo_ref.texture = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        if PM.Settings.DEBUG_LEVEL == 0:  # Do the screenshot for boot_splash.png
            # Not transparent and no fade in.
            var color_rect_ref = PM.get_tree().root.get_node(
                    "Container/Scene0/Splash/CenterContainer/VBoxContainer/ColorRect")
            color_rect_ref.visible = false
            var spinner_ref = PM.get_tree().root.get_node(
                    "Container/Scene0/Splash/CenterContainer/VBoxContainer/LoadingSpinner")
            spinner_ref.visible = false
            LoadingOutput.text = ""
            yield(PM.get_tree(), "idle_frame")
            var clear_mode = PM.get_viewport().get_clear_mode()
            PM.get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
            # Wait until the frame has finished before getting the texture.
            yield(VisualServer, "frame_post_draw")
            # Retrieve the captured image.
            var img = PM.get_viewport().get_texture().get_data()
            PM.get_viewport().set_clear_mode(clear_mode)
            # Flip it on the y-axis (because it's flipped).
            img.flip_y()
            img.save_png("res://assets/images/boot_splash.png")
            color_rect_ref.visible = true
            spinner_ref.visible = true
        else:
            freecoil_logo_ref.modulate = Color(1,1,1,0)  # Transparent
            call_deferred("fade_in_logo", freecoil_logo_ref)
        call_deferred("load_menu_music")

func load_menu_music():
    LoadingOutput.text = "Loading: Menu Music..."
    PM.background_load_resource("res://assets/sounds/background music/BM_menu_loop.ogg", 
            self, "setup_menu_music")
    
func setup_menu_music(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: Menu Music..."
        PM.disconnect_background_load_resource(self, "setup_menu_music")
        var temp = PM.get_tree().root.get_node("Container/Audio")
        for count in range(0, 15):
            var temp2 = AudioStreamPlayer.new()
            temp2.name = "Stream" + str(count)
            temp.add_child(temp2)
        # Audio Stream 0 is Always Background Music
        temp = PM.get_tree().root.get_node("Container/Audio/Stream0")
        temp.stream = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        temp.volume_db = -20.0
        temp.play()
        call_deferred("load_scene_fader")  

func load_scene_fader():
    LoadingOutput.text = "Loading: Scene Fader..."
    PM.background_load_resource("res://scenes/Global/SceneFader/SceneFader.tscn", 
            self, "setup_scene_fader")

func setup_scene_fader(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: Scene Fader..."
        PM.disconnect_background_load_resource(self, "setup_scene_fader")
        PM.get_tree().call_group("Container", "instance_scene_fader", 
                PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num))
        call_deferred("load_main_menu_states")

func load_main_menu_states():
    LoadingOutput.text = "Loading: Program Manager States..."
    PM.Settings.Log("Program Manager: Loading Main Menu states...")
    var dir = Directory.new()
    dir.open("res://Logic/ProgramManager/MainScreens/")
    dir.list_dir_begin()
    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with("."):
            if "Splash" in file:
                pass
            else:
                # https://github.com/godotengine/godot/issues/42507
                if file.matchn("*.gd.remap"):
                    file = file.replace(".gd.remap", ".gd")
                elif file.matchn("*.gdc"):
                    file = file.replace(".gdc", ".gd")
                LoadingOutput.text = "Loading: Program Manager State - " + file + "..."
                PM.Settings.Log("Program Manager: Loading: " + file)
                PM.background_load_resource("res://Logic/ProgramManager/MainScreens/" + file, 
                        self, "null", false)
                var state_loaded = false
                while not state_loaded:
                    yield(PM.Settings.Session, PM.Settings.Session.monitor_data("Helpers_background_load_result"))
                    var loading_result = PM.Settings.Session.get_data(
                            "Helpers_background_load_result")[PM.loading_thread_num]
                    if loading_result == "finished":
                        state_loaded = true
                        break
                # FIXME: When Godot fixes this: https://github.com/godotengine/godot/issues/24311
                LoadingOutput.text = "Setting Up: Program Manager State - " + file + "..."
                PM.States.append(PM.Helpers.get_loaded_resource_from_background(
                        PM.loading_thread_num).new())
                PM.Settings.Log("Program Manager: Loaded: " + PM.States[-1].STATE_ID)
    dir.list_dir_end()
    PM.Settings.Log("Program Manager: State Loading Completed.")
    var counter = 0
    for state in PM.States:
        match state.STATE_ID:
            "UserTypeChoice":
                transitionable_states[state.STATE_ID] = counter
            "FineAccessLocationPermission":
                transitionable_states[state.STATE_ID] = counter
            "NoPermissionsNeeded":
                transitionable_states[state.STATE_ID] = counter
            "TestLineEdit":
                transitionable_states[state.STATE_ID] = counter
            "GameSettingsReview":
                transitionable_states[state.STATE_ID] = counter
        counter += 1
    call_deferred("background_load_freecoil_interface")

func background_load_freecoil_interface():
    LoadingOutput.text = "Loading: FreecoiL Interface..."
    PM.background_load_resource("res://Logic/FreecoiLInterface/FreecoiLInterface.gd", 
            self, "setup_freecoil_interface")

func setup_freecoil_interface(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: FreecoiL Interface..."
        PM.disconnect_background_load_resource(self, "setup_freecoil_interface")
        var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).new()
        PM.FreecoiLInterface = temp
        call_deferred("initialize_freecoil_interface")

func initialize_freecoil_interface():
    LoadingOutput.text = "Initializing: FreecoiL Interface..."
    PM.FreecoiLInterface.initialize_freecoil_singleton()
    call_deferred("background_load_networking")
    
func background_load_networking():
    LoadingOutput.text = "Loading: Networking..."
    PM.background_load_resource("res://Logic/Networking/Networking.gd", self, "setup_networking")
    
func setup_networking(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: Networking..."
        PM.disconnect_background_load_resource(self, "setup_networking")
        var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        PM.add_child(temp.new())
        PM.Networking = PM.get_node("Networking")
        call_deferred("background_load_main_menu_empty")
    
func background_load_main_menu_empty():
    LoadingOutput.text = "Loading: Empty Main Scene..."
    PM.background_load_resource("res://scenes/MainScreens/MainScreensEmpty.tscn", 
            self, "setup_main_menu_empty")

func setup_main_menu_empty(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: Empty Main Scene..."
        PM.disconnect_background_load_resource(self, "setup_main_menu_empty")
        var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
        PM.get_tree().call_group("Container", "set_new_scene", 
                temp)
        main_menu_empty_is_ready = true
        call_deferred("background_load_title_label")

func background_load_title_label():
    LoadingOutput.text = "Loading: The Title Label..."
    PM.background_load_resource("res://scenes/Global/TitleLabel.tscn", 
            self, "setup_loading_title_label")

func setup_loading_title_label(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: The Title Label..."
        PM.disconnect_background_load_resource(self, "setup_loading_title_label")
        var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
        PM.get_tree().root.get_node("Container").current_scene.get_node("Cache").add_child(temp)
        temp.visible = false
        call_deferred("background_load_standard_header")

func background_load_standard_header():
    LoadingOutput.text = "Loading: FreecoiL Header UI..."
    PM.background_load_resource("res://scenes/MainScreens/StandardHeader.tscn", 
            self, "setup_standard_header")

func setup_standard_header(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: FreecoiL Header UI..."
        PM.disconnect_background_load_resource(self, "setup_standard_header")
        PM.get_tree().root.get_node("Container/UI").add_child(
                PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance())
        PM.get_tree().root.get_node("Container/UI").get_child(0).visible = false
        standard_header_is_ready = true
        PM.Settings.Session.connect(PM.Settings.Session.monitor_data("Container_loading_state"), self, "unhide_ui")
        call_deferred("check_n_wait_till_theme_is_loaded")

func check_n_wait_till_theme_is_loaded():
    LoadingOutput.text = "Loading: Theme..."
    while not theme_setup_is_complete:
        yield(PM.get_tree(), "idle_frame")
    call_deferred("background_load_loading_scene")

func background_load_loading_scene():
    LoadingOutput.text = "Loading: The Loading Screen..."
    PM.background_load_resource("res://scenes/Global/Loading.tscn", 
            self, "setup_loading_scene")

func setup_loading_scene(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: The Loading Screen..."
        PM.disconnect_background_load_resource(self, "setup_loading_scene")
        var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
        PM.get_tree().root.get_node("Container/Loading").add_child(temp)
        PM.LoadingScreen = PM.get_tree().root.get_node("Container/Loading/Loading")
        temp.visible = false
        temp.modulate.a = 0
        call_deferred("background_load_app_tests")

func background_load_app_tests():
    var arguments = {}
    for argument in OS.get_cmdline_args():
        if argument.find("=") > -1:
            var key_value = argument.split("=")
            arguments[key_value[0].lstrip("--")] = key_value[1]
    if arguments.has("run_test"):
        if arguments["run_test"] == "true":
            LoadingOutput.text = "Loading: An Application Test..."
            PM.background_load_resource("res://tests/integration/ApplicationTestLoader.tscn", 
                    self, "setup_application_test")
        else:
            call_deferred("background_load_state_ahead")
    elif PM.Settings.debug_toggles.has("ApplicationTest"):
        LoadingOutput.text = "Loading: An Application Test..."
        PM.background_load_resource("res://tests/integration/ApplicationTestLoader.tscn", 
                self, "setup_application_test")
    else:
        call_deferred("background_load_state_ahead")

func setup_application_test(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        LoadingOutput.text = "Setting Up: An Application Tests..."
        PM.disconnect_background_load_resource(self, "setup_application_test")
        var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
        PM.get_tree().root.get_node("Container").add_child(temp)
        call_deferred("background_load_state_ahead")

func background_load_state_ahead():
    if PM.Settings.debug_toggles["TestLineEdit"]:
        LoadingOutput.text = "Loading: TestLineEdit..."
        PM.States[transitionable_states["TestLineEdit"]].load_required_for_state()
        next_state = "TestLineEdit"
        next_text = "Setting Up: TestLineEdit..."
    elif PM.Settings.experimental_toggles["GameSettingsReview"]:
        LoadingOutput.text = "Loading: GameSettingsReview..."
        PM.States[transitionable_states["GameSettingsReview"]].load_required_for_state()
        next_state = "GameSettingsReview"
        next_text = "Setting Up: GameSettingsReview..."
    else:
        LoadingOutput.text = "Loading: User Type Choice Screen..."
        PM.States[transitionable_states["UserTypeChoice"]].load_required_for_state()
        next_state = "UserTypeChoice"
        next_text = "Setting Up: User Type Choice Screen..."
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    LoadingOutput.text = next_text
    PM.get_tree().call_group("SceneFader", "fade_in")
    continue_spinner_updates = false
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])
        
 
func unhide_ui(state):
    if state == "idle":
        PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("Container_loading_state"), self, "unhide_ui")
        PM.get_tree().root.get_node("Container/UI").get_child(0).visible = true
