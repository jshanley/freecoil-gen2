extends Resource
class_name Chance

export(String) var named_value = ""
export(int) var pick_chance: int = 1
export(bool) var can_be_chosen: bool = true
# Below are optional values
export(int) var quantity: int = 1
export(float) var worth: float = 0.0
export(String) var description: String = ""
export(String) var rarity: String = ""

