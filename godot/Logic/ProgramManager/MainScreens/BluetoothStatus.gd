extends Resource
# Technically this extends Resource, but that is the default if empty.

var state_has_on_ready = false
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "BluetoothStatus"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var loading_next_state_already = false
var loaded_broken_bluetooth = false
var loaded_enable_bluetooth = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    var counter = 0
    for state in PM.States:
        match state.STATE_ID:
            "GPSStatus":
                transitionable_states[state.STATE_ID] = counter
        counter += 1
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_bluetooth_status"), self, 
            "check_if_enabled")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "get_bluetooth_enabled")
    get_bluetooth_enabled(null)
    check_if_enabled(PM.Settings.Session.get_data("fi_bluetooth_status"))

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("fi_bluetooth_status"), self, 
            "check_if_enabled")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "get_bluetooth_enabled")

func get_bluetooth_enabled(_current_screen):
    if (PM.Settings.Session.get_data("current_screen") != "6,1" and 
            PM.Settings.Session.get_data("current_screen") != "4,1"):  # and not or
        PM.get_tree().call_group("Container", "next_screen", "4,1")

func check_if_enabled(enabled):
    if enabled == 1:
        if not loading_next_state_already:
            loading_next_state_already = true
            background_load_state_ahead()
    elif enabled == 2:
        if PM.Settings.Session.get_data("current_screen") != "6,1":
            PM.get_tree().call_group("Container", "next_screen", "6,1")
    else:
        PM.get_tree().call_group("Container", "next_screen", "4,1")

func load_required_for_state():
    PM.background_load_resource("res://scenes/MainScreens/TurnOnBluetooth.tscn", self, "setup_enable_bluetooth")

func setup_enable_bluetooth(background_results):
    if not required_for_state_is_complete:
        var results = background_results[PM.loading_thread_num]
        if results == "finished":
            PM.disconnect_background_load_resource(self, "setup_enable_bluetooth")
            var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
            PM.get_tree().root.get_node("Container").current_scene.add_child(
                    temp)
            loaded_enable_bluetooth = true
            PM.background_load_resource("res://scenes/MainScreens/BluetoothBroken.tscn", self, "setup_broken_bluetooth")
    
func setup_broken_bluetooth(background_results):
    if not required_for_state_is_complete:
        var results = background_results[PM.loading_thread_num]
        if results == "finished":
            PM.disconnect_background_load_resource(self, "setup_broken_bluetooth")
            var temp = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance()
            PM.get_tree().root.get_node("Container").current_scene.add_child(
                    temp)
            loaded_broken_bluetooth = true
            call_deferred("setup_required_for_state")

func setup_required_for_state():
    if not required_for_state_is_complete:
        if loaded_broken_bluetooth and loaded_enable_bluetooth:
            required_for_state_is_complete = true

func background_load_state_ahead():
    PM.States[transitionable_states["GPSStatus"]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states["GPSStatus"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states["GPSStatus"])
