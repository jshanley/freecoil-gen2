extends Resource

var Server = null

func setup_server_part1():
    PM.Networking.reset_networking()
    PM.Settings.Session.set_data("mup_id", "1")  # mup_id != peer id.
    PM.Networking.test_ip_address()
    if PM.Settings.Session.get_data("server_ip_issue") == "all_good":
        PM.Settings.Log("Network: Server: Valid IP Address found of " + str(
                PM.Settings.Session.get_data("server_ip"), "info") + ".")
        call_deferred("setup_server_part2")
        
func setup_server_part2():
    PM.Settings.Log("Network: Server: Setting up the Server now.", "info")
    PM.Settings.Session.set_data("server_port", PM.Settings.NETWORK_LAN_PORT)
    Server = NetworkedMultiplayerENet.new()
    Server.set_bind_ip(PM.Settings.Session.get_data("server_ip"))
    Server.create_server(PM.Settings.Session.get_data("server_port"), 
            PM.Settings.MAX_PLAYERS + PM.Settings.MAX_OBSERVERS)
    PM.Networking.get_tree().set_network_peer(Server)
    if PM.run_ITM and PM.ITM_act_as_server:
        PM.Settings.Network.set_data("mups_status", {PM.ITM_fake_server_unique_id: "connected"})
    else:
        PM.Settings.Network.set_data("mups_status", {OS.get_unique_id(): "connected"})
    PM.Settings.Session.set_data("connection_status", "connecting")
    PM.Networking.UDP_Module.host_udp_broadcast = true
    PM.Networking.UDP_Module.broadcast_to_peers()
    PM.Settings.InGame.set_data("player_name_by_id", {"1": PM.Settings.Preferences.get_data("player_name")})
    PM.Settings.InGame.set_data("player_team_by_id", {"1": 0})
    PM.Settings.InGame.set_data("game_teams_by_team_num_by_id", [["1"]])
    PM.Networking.set_process(true)
    PM.Settings.Log("Network: Server: Server created and sending out UDP invites" +
            " with a UID of " + str(PM.Networking.UDP_Module.host_udp_broadcast_uid) + ".", "info")
            
func _client_connected(godot_peer_id):  # Client Equals Another Player
    if PM.Networking.get_tree().is_network_server():
        PM.Settings.Log("Network: Server: Peer Connected with peer id: " + str(godot_peer_id), "info")
        if godot_peer_id in PM.Settings.Network.get_data("mups_to_peers").values():
            var peers_to_mups = PM.Settings.Network.get_data("peers_to_mups")
            var mups_status = PM.Settings.Network.get_data("mups_status")
            mups_status[peers_to_mups[godot_peer_id]] = "identifying"
            PM.Settings.Network.set_data("mups_status", mups_status)
        if PM.Settings.Session.get_data("connection_status") != "connected":
            PM.Settings.Session.set_data("connection_status", "connected")
    
func _client_disconnected(godot_peer_id):  # Client Equals Another Player
    if PM.Networking.get_tree().is_network_server():
        PM.Settings.Log("Network: Server: Peer Disconnected with peer id: " + str(godot_peer_id))
        if godot_peer_id in PM.Settings.Network.get_data("mups_to_peers").values():
            var peers_to_mups = PM.Settings.Network.get_data("peers_to_mups")
            var mups_status = PM.Settings.Network.get_data("mups_status")
            mups_status[peers_to_mups[godot_peer_id]] = "disconnected"
            PM.Settings.Network.set_data("mups_status", mups_status)
            var all_mups_disconnected = true
            for mup in mups_status:
                if mup != "1":
                    if mups_status[mup] == "connected":
                        all_mups_disconnected = false
                    elif mups_status[mup] == "reconnected":
                        all_mups_disconnected = false
            if all_mups_disconnected:
                PM.Settings.Session.set_data("connection_status", "disconnected")
