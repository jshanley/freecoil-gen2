extends Resource
# Technically this extends Resource, but that is the default if empty.

var state_has_on_ready = false
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "NoPermissionsNeeded"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var loading_next_state_already = false
var started_background_loading = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    background_load_state_ahead()

func on_exit() -> void:
    pass

func on_ready_event() -> int:
    return state_num

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func load_required_for_state():
    var counter = 0
    for state in PM.States:
        match state.STATE_ID:
            "PlayerName":
                transitionable_states[state.STATE_ID] = counter
        counter += 1
    PM.States[transitionable_states["PlayerName"]].load_required_for_state()
    while not PM.States[transitionable_states["PlayerName"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    required_for_state_is_complete = true
    
func setup_required_for_state(_background_results):
#    var results = background_results[PM.loading_thread_num]
#    if results == "finished":
#        PM.disconnect_background_load_resource(self, "setup_screen_for_state")
#        PM.get_tree().root.get_node("Container").current_scene.add_child(
#                PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num).instance())
#        yield(PM.get_tree(), "idle_frame")
#        required_for_state_is_complete = true
    pass

func background_load_state_ahead():
    PM.States[transitionable_states["PlayerName"]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states["PlayerName"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states["PlayerName"])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
