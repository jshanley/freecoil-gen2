extends "res://addons/gut/test.gd"

var Helpers

func before_each():
    Helpers = load("res://Logic/Helpers.gd").new()

func test_godot_dict_hash_still_broken():
    var time_used = OS.get_ticks_usec()
    var dict1 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":time_used, 
        "type":"Test"
    }
    var dict2 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":time_used
    }
    # The dicts are not the same hash() value because the order is different.
    assert_ne(dict1.hash(), dict2.hash())
    assert_eq_deep(dict1, dict2)

func test_dicts_same_keys_n_vals():
    var dict1 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":12345, 
        "type":"Test"
    }
    var dict2 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "time":12345, 
        "type":"Test"
    }
    assert_true(Helpers.dicts_same_keys_n_vals(dict1, dict2), 
        "Identical dictionaries, but different places in memory."
    )
    var dict3 = {"additional":{"test":"works"}, "event_num":0, 
        "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    assert_true(Helpers.dicts_same_keys_n_vals(dict1, dict3), 
        "The order of the dicts is different."
    )
    dict2["time"] = 0
    dict2["event_num"] = 12345
    assert_false(Helpers.dicts_same_keys_n_vals(dict1, dict2), 
        "Values of the keys are not the same."
    )
    dict2["time"] = 12345
    dict2["event_num"] = 0
    dict2["extra"] = "slot"
    assert_false(Helpers.dicts_same_keys_n_vals(dict1, dict2),
        "The size and contents of the dictionaries are different."
    )
    var dict4 = {"additional":{"test":{"deep":{"deeper":{"even_deeper":"Nesting"}}}}, 
        "event_num":0, "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    var dict5 = {"additional":{"test":{"deep":{"deeper":{"even_deeper":"Nesting"}}}},
        "event_num":0, "rec_by":"939972095cf1459c8b22cc608eff85da", "type":"Test",
        "time":12345
    }
    assert_true(Helpers.dicts_same_keys_n_vals(dict4, dict5), 
        "Test 5 level deep nested dictionaries."
    )
