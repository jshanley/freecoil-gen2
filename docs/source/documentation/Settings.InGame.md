# PM.Settings.InGame

InGame Settings are saved to disk and they are passed over the network.

## "current_armory_name"

The name (key) of the armory that is currently in use.  This is a key of the "saved_armories" variable in the Preferences area of the host.

## "current_armory"

The active armory that is in use.  See "preset_armory" variable in Session area for dictionary format.

## "gps_required"

Used to determine if the game requires GPS.

### "gps_required" Values

* true == GPS is required for the game.
* false == GPS is not required for the game.

## "medic_type"

Used to determine if the game allows medics, the type of medic that is set for the game, and to determine the function that is used in the game for the medic to heal other players.

### "medic_type" Values

* 0 == No Medics
* 1 == Medics that heal by GPS proximity and that take NO damage.
* 2 == Medics that heal by GPS proximity and that take damage from Area Effects only.
* 3 == Medics that heal by GPS proximity and that take damage from Guns only.
* 4 == Medics that heal by GPS proximity and that take ALL damage types.
* 5 == Medics that heal by audio-sync and that take NO damage.
* 6 == Medics that heal by audio-sync and that take damage from Area Effects only.
* 7 == Medics that heal by audio-sync and that take damage from Guns only.
* 8 == Medics that heal by audio-sync and that take ALL damage types.

## "player_name"

Player name (unique).

## "player_name_by_id"

Player name by MUP ID (dictionary)

### "player_name_by_id" Values

```
{mups: player_name}
```

## "player_team_by_id"

Player team by MUP ID (dictionary)

### "player_team_by_id" Values

```
{mups: player_team}
```

## "game_teams_by_team_num_by_id" TBD

## "game_number_of_teams" TBD

## "current_game_settings"

Contains all of the game settings that have been set by the host for the current game (copied from identically formatted "next_game_settings" in Session variables).  This same dictionary format is used in the Settings.Preferences area for default settings for each game_mode.

### "current_game_settings"

An example (default Deathmatch shown) of the game settings is shown below.

```
{"game_mode": "Deathmatch", 
"team_amount": 0, 
"game_objective": "Kill Count",
"game_length_time": 30,
"respawns_allowed": 5,
"respawn_type": "Location",
"respawn_delay": 30,
"initial_health": 10,
"initial_shield": 5,
"friendly_fire": false,
"force_recall_action":false,
"indoor_game": false,
"healing_radius: 10,
"heal_per_bandage": 3,
"healing_time": 10,
"initial_medpacks": 8}
```

## "game_mode" (set in GameSettings)

The selected game mode for the current game.  The default game mode for PreGames is Survival(1).

### "game_mode" Values

* "Deathmatch"
* "Survival"
* "Free For All"
* "Battle Royale"
* "Capture the Flag"

## "team_amount" (set in GameSettings)

The maximum amount of teams allowed in the current game.

### "team_amount" Values

* 0 == No teams (individual game mode).
* 2-8 == Maximum number of teams allowed.

## "game_objective" (set in GameSettings)

The score metric for point-based (non-survival) games.

### "game_objective" Values

* "Kill Count"
* "Death Count"
* "Flag Count" (Capture the Flag only)

## "game_objective_goal" (set in GameSettings)

The end-game goal for point-based (non-survival) games.

### "game_objective_goal" Values

* 0 == No end game goal.
* 1 to 25 == End game goal for the game.

## "game_length_time" (set in GameSettings)

The length of the time assigned to the game (for timed games, minutes).

### "game_length_time" Values

* 0 == No time limit
* 5 to 60 == Game length (minutes)

## "respawns_allowed" (set in GameSettings)

Sets whether respawns are enabled in the game, and if so, the number of respawns allowed

### "respawns_allowed" Values

* 0 == Respawns are disabled
* 1 to 20 == Number of respawns that are allowed
* "Unlimited" == Unlimited respawns are allowed

## "respawn_type" (set in GameSettings)

Sets the type of respawn that is used in the game.

### "respawn_type" Values

* "Location" == Respawn at home location
* "Timed" == Respawn automatically after time delay

## "respawn_delay" (set in GameSettings)

Amount of time delay required after death before respawning.

### "respawn_delay" Values

* 0 == No Delay
* 10 to 120 (seconds, 10 sec increments)

## "initial_health" (set in GameSettings)

The initial user health.
Note:  This may be moved to the Settings.Session as a static value.

### "initial_health" Values

* 2 to 20 == Health Setting

## "initial_shield" (set in GameSettings)

The initial user shield level.

### "initial_shield" Values

* 0 == No shields allowed
* 1 to 10 == Shield Setting

## "friendly_fire" (set in GameSettings)

Sets whether friendly fire is enabled in the game.

### "friendly_fire" Values

* true == Friendly fire is enabled
* false == Friendly fire is disabled

## "host_lock_settings" (set in GameSettings)

Host locks the games settings from all players.

### "host_lock_settings" Values

* true == Settings are locked
* false == Settings are unlocked

### "force_recoil_action" (set in GameSettings)

* true == Recoil action on all player's guns are locked on.
* false == Recoil action on all player's guns are NOT locked on.

## "indoor_game" (set in GameSettings)

Setting to disable the short range (wide beam) IR on all player's guns.

### "indoor_game" Values

* true == Short range (wide beam) IR is disabled on all player's guns.
* false == Short range (wide beam) IR is enabled on all player's guns.

## "healing_radius" (set in GameSettings)

The radius (meters) that the healed player must be to the medic in order for GPS proximity healing to occur.

### "healing_radius" Values

* 10 == 10 meter radius
* 20 == 20 meter radius
* 30 == 30 meter radius

## "heal_per_bandage" (set in GameSettings)

The amount of health that is healed per bandage.

### "heal_per_bandage" Values

* 1-10 == Health points healed per bandage

## "healing_time" (set in GameSettings)

The amount of time (seconds) that it takes for a medic to bandage another player.

### "healing_time" Values

* 5 == 5 seconds to heal
* 10 == 10 seconds to heal
* 15 == 15 seconds to heal
* 20 == 20 seconds to heal
* 30 == 30 seconds to heal

## "initial_medpacks" (set in GameSettings)

The initial amount of medpacks that a medic has in the game.

### "initial_medpacks" Values

* 2-10 == 2-10 initial medpacks
* 15 == 15 initial medpacks
* 20 == 20 initial medpacks
* "Unlimited" == Unlimited initial medpacks

## "team_number"

Team number (1-8).

## "respawns_taken"

The number of times a user has respawned.

## "shots"

Number of shots made by the user.

## "hits"

Number of successful hits made by the user.

## "kills"

Number of kills made by the user.

## "time_in_game"

Amount of time that user lasted in the game.

## "heals"

Number of successful heals made by user.

## "times_hit"

Number of times that the user was hit.

## "deaths"

Number of times that the user was killed.

## "times_healed"

Number of times that the user was healed.

## "field_center_latitude" (set in MapSelect) 

The latitude coordinate for the field center.

## "field_center_longitude" (set in MapSelect)

The latitude coordinate for the field center.

## "field_height" (set in MapSelect)

The height of the effective game area.

## "field_width" (set in MapSelect)

The height of the effective game area.

## "field_centerline_angle" (set in MapSelect)

The angle of the field centerline.  The centerline is the center axis of the field.  As a reference, the centerline is oriented along the North and South direction when the field angle is at 0 degrees.

## "flag_location_by_team"

The coordinates of each team flag, as assigned by the team leader.

## "flag_capture_goal"

The target number of flags that are to be captured by each team.

## "flag_capture_count"

The number of flags that have been successfully captured by the team.

## "storm_center"

The coordinates of the storm center, if different from field_center.

## "storm_radius"

The initial radius of the storm (meters), if different from field_radius.

## "storm_stall_time"

The time that the storm stalls between each period of storm contraction.

## "storm_stall_amount"

The number of times during the game that the storm stalls.

## "storm_shrink_rate"

The rate (meters/sec) at which the storm radius shrinks when it is active.

## "game_load_complete_by_mup"

A dictionary where the key is the MUP ID and the value is what stage of loading and starting of the game that the player is at.  
* 0 is not loaded and and not ready
* 1 is loaded_and_ready, but not synced to the server.
* 2 is synced to the server.
* 3 is started playing.
