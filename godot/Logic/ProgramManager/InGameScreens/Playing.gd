extends Resource

#"state_has" variables are for the types of functions that would be used in this state
var state_has_on_process = false #Should be "false" if not used
var state_has_on_physics_process = false #Should be "false" if not used
var state_has_on_input = false #Should be "false" if not used, May be used for mouse or button tracking if PC use is expanded in the future
var state_has_on_unhandled_input = false #Should be "false" if not used, Used for more global inputs (i.e. screen swipe that aren't directly associated with a specific control/button)
const STATE_ID:String = "Playing" #Name of the scene that corresponds to this State Class
var state_num: int #No longer used, replaced with Match on StateID (sometimes used for internal tracking)
var transitionable_states: Dictionary = {} #Dictionary that locally maps the State numbers to StateID's for external related State Classes
var required_for_state_is_complete = false #Initial value, Guard variable
var all_scenes_cued = false #Initial value, Secondary guard variable needed because multiple scenes are loaded
var loading_next_state_already = false #Initial value, Guard variable
var started_background_loading = false #Initial value, Guard variable
var my_screen_offset: String = "0,0" #Initial value
var my_screen_name: String = ""
var next_state: String

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned #state_num is used for on_process_event()->on_unhandeled_input_event(), etc returned values to PM
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "debug") #Debug only
    load_required_for_state()
    #Connect all the triggers that are needed for this State Class
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger") #General use (generic) trigger
    #Control to prevent the user from navigating to other screens without the State Class being aware
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")

func on_exit() -> void: #Perform general cleanup of this State Class
    #Remove connections that were used for this State Class
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    
    #Perform general cleanup
    call_deferred("cleanup")

func state_trigger(new_value): #Function for general-use trigger
    if new_value != null:
        pass

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        
func load_required_for_state():
    #Func Description:
        #Func Desc 1 - Compile list ("match state.STATE_ID") of other State Classes that are involved in this State Class (i.e. those that are related with the next potential scenes)?
        #Func Desc 2 - Background load the scene/screen that corresponds with this State Class
    var subscene_list = ["Playing.tscn", "Map (Old).tscn", "Footer.tscn", "LPanel.tscn", "RPanel.tscn"] #Used to load and layer an InGame/Playing scenes.
    var dir = Directory.new()
    dir.open("res://Logic/ProgramManager/InGame/")
    dir.list_dir_begin()
    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with("."):
            if ("Playing" in file) or ("Map (Old)" in file) or ("Footer" in file) or ("LPanel" in file) or ("RPanel" in file):
                pass
            else:
                # https://github.com/godotengine/godot/issues/42507
                if file.matchn("*.gd.remap"):
                    file = file.replace(".gd.remap", ".gd")
                elif file.matchn("*.gdc"):
                    file = file.replace(".gdc", ".gd")
                PM.background_load_resource("res://Logic/ProgramManager/InGame/" + file, 
                        self, "null", false)
                yield(PM.Settings.Session, PM.Settings.Session.monitor_data("Helpers_background_load_result"))
                # FIXME: When Godot fixes this: https://github.com/godotengine/godot/issues/24311
                PM.States.append(PM.Helpers.get_loaded_resource_from_background(
                        PM.loading_thread_num).new())
    dir.list_dir_end()
    var counter = 0
    for state in PM.States:
        match state.STATE_ID:
            "RadioHistory":
                transitionable_states[state.STATE_ID] = counter
            "Respawn":
                transitionable_states[state.STATE_ID] = counter
            "Eliminated":
                transitionable_states[state.STATE_ID] = counter
            "End of Game":
                transitionable_states[state.STATE_ID] = counter
        counter += 1
        
        for n in range(0, subscene_list.size):
            PM.background_load_resource("res://scenes/InGame/" + subscene_list[n], self, "setup_required_for_state") #Loads the scenes that corresponds to this State Class
            if n == subscene_list.size:
                all_scenes_cued = true
    
func setup_required_for_state(background_results): #When the resource is ready, set it up and draw it to screen
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_required_for_state")
        var loaded_scene = PM.Helpers.get_loaded_resource_from_background(
                PM.loading_thread_num).instance() #Initialization of the scene
        my_screen_offset = loaded_scene.editor_description #Obtaining scene/screen location from Edittor Description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene) #Adding scene to the view and running the screen/scene
        yield(PM.get_tree(), "idle_frame") #Wait 1 frame for on_ready and drawing
        if all_scenes_cued == true:
            required_for_state_is_complete = true
            screen_change(null)

func background_load_state_ahead():  # This is now optional for preloading the next scene/screen.
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():  # This is always called by the function above.
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame") #Yields to a frame until fully loaded
    transition_to_next_state()
        
func transition_to_next_state():  # This is now the prefered method of transitioning to the next state.  Transitions the entire State Class.
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free() #Deletes the screen/scene
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
