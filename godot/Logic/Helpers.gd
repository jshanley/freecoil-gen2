extends Resource

var background_threads: Dictionary = {}
var background_loaded_resources: Dictionary = {}
var thread_counter = 0
var session_data_initialized = false
var queued_threads: Dictionary = {}


static func general_type_of(obj):
    var typ = typeof(obj)
    var builtin_type_names = ["Area2D", "nil", "bool", "int", "real", "string", "vector2", "rect2", "vector3", 
        "maxtrix32", "plane", "quat", "aabb",  "matrix3", "transform", "color", "image", "nodepath",
        "rid", null, "inputevent", "dictionary", "array", "rawarray", "intarray", "realarray",
        "stringarray", "vector2array", "vector3array", "colorarray", "unknown"]
    if(typ == TYPE_OBJECT):
       return obj.type_of()
    else:
        return builtin_type_names[typ]

static func bearing(origin, destination):
    return degrees_360_from_to(origin, destination)
    
static func inverse_bearing(bearing):
    if bearing > 180:
        return bearing - 180
    elif bearing < 180:
        return bearing + 180
    else:  # bearing == 180
        return 360

static func degrees_360_from_to(from, to):
    var delta_x = from.x - to.x 
    var delta_y = from.y - to.y
    var inverse_tangent_2 = atan2(delta_x, delta_y)
    var angle_degrees = rad2deg(inverse_tangent_2)
    var angle_degrees_360
    if angle_degrees == 0:
        angle_degrees_360 = 0
    elif angle_degrees < 0:
        angle_degrees_360 = angle_degrees * -1
    elif angle_degrees > 0:
        angle_degrees_360 = (180 - angle_degrees) + 180
    return angle_degrees_360
    
static func join_array_to_str(arr, delimiter=","):
    var str_arr = ""
    for item in arr:
        if str_arr == "":
            str_arr = str(item)
        else:
            str_arr += delimiter + str(item)
    return str_arr
    
func error_lookup(err_num):
    return get_error_description(err_num)

static func get_error_description(err_num):
    var error_array = ["OK: No error.", "FAILED: Generic error, error uknown.", 
            "ERR_UNAVAILABLE: Unavailable error, resource unavailable.", 
            "ERR_UNCONFIGURED: Resource unconfigured.",
            "ERR_UNAUTHORIZED: Resource returned unauthorized error.", 
            "ERR_PARAMETER_RANGE_ERROR", "ERR_PARAMETER_RANGE_ERROR", "ERR_FILE_NOT_FOUND",
            "ERR_FILE_BAD_DRIVE", "ERR_FILE_BAD_PATH", "ERR_FILE_NO_PERMISSION", 
            "ERR_FILE_ALREADY_IN_USE", "ERR_FILE_CANT_OPEN", "ERR_FILE_CANT_WRITE",
            "ERR_FILE_CANT_READ", "ERR_FILE_UNRECOGNIZED", "ERR_FILE_CORRUPT", 
            "ERR_FILE_MISSING_DEPENDENCIES", "ERR_FILE_EOF", 
            "ERR_CANT_OPEN: Can't open the resource. Typically a file but from a higher level library like ConfigFile or JSON."]
    return error_array[err_num]

    
func print_display_metrics():
    var display_script = load("res://Logic/display.gd").new()
    display_script.print_display_metrics()
    
func get_display_metrics():
    var display_script = load("res://Logic/display.gd").new()
    return display_script.get_display_metrics()
    
func background_loader_n_callback(resource_path, object, method):
    if not PM.Settings.Session.is_connected(PM.Settings.Session.monitor_data("Helpers_background_load_result"), object, method):
        PM.Settings.Session.connect(PM.Settings.Session.monitor_data("Helpers_background_load_result"), object, method)
    return threaded_background_loader(resource_path)

func threaded_background_loader(resource_path):
    if not session_data_initialized:
        PM.Settings.Session.set_data("Helpers_background_load_progress", {})
        PM.Settings.Session.set_data("Helpers_background_load_result", {}, false, false)
        session_data_initialized = true
    thread_counter += 1
    var thread_num = thread_counter
    if background_threads.size() >= PM.Settings.max_threads:
        var background_load_progress =  PM.Settings.Session.get_data("Helpers_background_load_progress")
        background_load_progress[thread_num] = 0.0
        PM.Settings.Session.set_data("Helpers_background_load_progress", background_load_progress)
        var background_load_results = PM.Settings.Session.get_data("Helpers_background_load_result")
        background_load_results[thread_num] = null
        PM.Settings.Session.set_data("Helpers_background_load_result", background_load_results, false, false)
        queued_threads[thread_num] = resource_path
    else:
        var background_load_thread = Thread.new()
        background_threads[thread_num] = background_load_thread
        var background_load_progress =  PM.Settings.Session.get_data("Helpers_background_load_progress")
        background_load_progress[thread_num] = 0.0
        PM.Settings.Session.set_data("Helpers_background_load_progress", background_load_progress)
        var background_load_results = PM.Settings.Session.get_data("Helpers_background_load_result")
        background_load_results[thread_num] = null
        PM.Settings.Session.set_data("Helpers_background_load_result", background_load_results, false, false)
        background_load_thread.start(self, '_threaded_loader', [resource_path, thread_num])
    return thread_num
    
func background_loading_progress(progress, thread_num):
    var background_load_progress =  PM.Settings.Session.get_data("Helpers_background_load_progress")
    background_load_progress[thread_num] = progress
    PM.Settings.Session.set_data("Helpers_background_load_progress", background_load_progress)
    
func finished_background_loading(thread_num):
    var background_load_results = PM.Settings.Session.get_data("Helpers_background_load_result")
    background_load_results[thread_num] = "finished"
    PM.Settings.Session.set_data("Helpers_background_load_result", background_load_results)
    
func failed_background_loading(err_msg, thread_num):
    PM.Settings.Log(err_msg)
    var background_load_results = PM.Settings.Session.get_data("Helpers_background_load_result")
    background_load_results[thread_num] = "failed"
    PM.Settings.Session.set_data("Helpers_background_load_result", background_load_results)

func _threaded_loader(resource_data):
    var path = null
    var thread_num = null
    for i in range(0, len(resource_data)):
        match i:
            0:
                path = resource_data[i]
            1: 
                thread_num = resource_data[i]
    var progress = 0.01
    var loader = ResourceLoader.load_interactive(path)
    var err = OK
    call_deferred('background_loading_progress', progress, thread_num)
    while err == OK:  # ERR_FILE_EOF = loading finished
        err = loader.poll()
        if err == OK:
            progress = float(loader.get_stage()) / loader.get_stage_count()
        else:
            progress = 0.99
        call_deferred('background_loading_progress', progress, thread_num)
    if err == ERR_FILE_EOF:
        var resource = loader.get_resource()
        call_deferred("move_resource_from_background_to_main", thread_num)
        return resource
    else: # error during loading
            call_deferred('failed_background_loading', "Error during background loading! Error Number = " 
                + str(err), thread_num)

func get_resource_from_background_n_disconnect(thread_num, object, method):
    if PM.Settings.Session.is_connected(PM.Settings.Session.monitor_data("Helpers_background_load_result"), object, method):
        PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("Helpers_background_load_result"), object, method)
    return get_loaded_resource_from_background(thread_num)

func get_loaded_resource_from_background(thread_num):
    var loaded_resource = null
    loaded_resource = background_loaded_resources[thread_num]
    var __ = background_loaded_resources.erase(thread_num)
    return loaded_resource

func move_resource_from_background_to_main(thread_num):
    # This function eliminates a bunch of errors that were occuring randomly,
    # from the thread not actually finishing in a single frame, which may tie up 
    # other functions that wanted to instance the result but too early, because 
    # it was not yet loaded completely.
    yield(PM.get_tree(), "idle_frame") # Wait a frame
    var tear_down = false
    background_loaded_resources[thread_num] = background_threads[thread_num].wait_to_finish()
    # Pull the data into the main thread then below we can start to tear down
    # and let the other methods know that the resource is ready to be instanced.
    tear_down = true
    #print("background_threads = " + str(background_threads.size()))
    #print("queued_threads = " + str(queued_threads.size()))
    yield(PM.get_tree(), "idle_frame")
    if tear_down:
        call_deferred("finished_background_loading", thread_num)
        call_deferred("delayed_tear_down", thread_num)

func delayed_tear_down(thread_num):
    var __ = background_threads.erase(thread_num)
    if background_threads.size() <= PM.Settings.max_threads:
        if queued_threads.size() > 0:
            PM.call_next_frame(PM.Helpers, "start_a_queued_thread")
    
func start_a_queued_thread():
    if background_threads.size() <= PM.Settings.max_threads:
        if queued_threads.size() > 0:
            var queued_thread_num = get_random_key_of_dict(queued_threads)
            var resource_path = queued_threads[queued_thread_num]
            var background_load_thread = Thread.new()
            background_threads[queued_thread_num] = background_load_thread
            background_load_thread.start(self, '_threaded_loader', [resource_path, queued_thread_num])
            var __ = queued_threads.erase(queued_thread_num)

func list_files_in_directory(path):
    var files = []
    var dir = Directory.new()
    if not dir.dir_exists(path):
        return []
    dir.open(path)
    dir.list_dir_begin(true)
    var file = dir.get_next()
    while file != "":
        if dir.current_is_dir():
            pass
        else:
            files.append(file)
        file = dir.get_next()
    return files

func get_random_key_of_dict(dict):
  var a = dict.keys()
  return a[randi() % a.size()]

# This method was created because of how bad dict.has() is.
# https://github.com/godotengine/godot/issues/27615
func dicts_same_keys_n_vals(dict1, dict2):
    var are_the_same = true  # Not checking for order.
    if dict1.size() != dict2.size():
        are_the_same = false
    if are_the_same:
        var dict1_keys = dict1.keys()
        var dict1_vals = dict1.values()
        var dict2_keys = dict2.keys()
        var dict2_vals = dict2.values()
        for i in range(0, dict1_keys.size()):
            var needs_sorting = false
            if typeof(dict1_keys[i]) != typeof(dict2_keys[i]):
                needs_sorting = true
            if dict1_keys[i] != dict2_keys[i]:
                needs_sorting = true
            if needs_sorting:
                var position = dict2_keys.find(dict1_keys[i]) 
                var key = dict2_keys[position]
                var value = dict2_vals[position]
                dict2_keys.erase(key)
                dict2_vals.erase(value)
                dict2_keys.insert(i, key)
                dict2_vals.insert(i, value)
        for i in range(0, dict1_keys.size()):
            if dict1_keys[i] != dict2_keys[i]:
                are_the_same = false
            if dict1_vals[i] != dict2_vals[i]:
                if (typeof(dict1_vals[i]) == TYPE_DICTIONARY and 
                    typeof(dict2_vals[i]) == TYPE_DICTIONARY
                ):
                    are_the_same = dicts_same_keys_n_vals(dict1_vals[i], dict2_vals[i])
                else:
                    are_the_same = false
    return are_the_same

func get_dir_contents(rootPath: String) -> Array:
    var files = []
    var directories = []
    var dir = Directory.new()
    if dir.open(rootPath) == OK:
        dir.list_dir_begin(true, false)
        _add_dir_contents(dir, files, directories)
    else:
        push_error("An error occurred when trying to access the path.")
    return [files, directories]
    
func _add_dir_contents(dir: Directory, files: Array, directories: Array):
    var file_name = dir.get_next()
    while (file_name != ""):
        var path = dir.get_current_dir().plus_file(file_name)
        if dir.current_is_dir():
            var subDir = Directory.new()
            subDir.open(path)
            subDir.list_dir_begin(true, false)
            directories.append(path)
            _add_dir_contents(subDir, files, directories)
        else:
            files.append(path)
        file_name = dir.get_next()
    dir.list_dir_end()
