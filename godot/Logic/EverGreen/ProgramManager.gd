extends Node

export(bool) var run_ITM: bool = false
var loaded_ITM_once = false
export(bool) var activate_ITM_Player:bool = false
export(bool) var activate_ITM_Recorder:bool = false
export(bool) var clear_user_dir:bool = false
var ran_erase_user_data_dir = false
export(String, DIR) var ITM_test_path = ""
export(bool) var ITM_act_as_server = false
export(bool) var ITM_allow_launch_clients = false
var ITM_fake_server_unique_id = "930072000cf0009c8b200c608ef005da"
export(bool) var run_GUT_unit_tests = false
var loaded_GUT_once = false


var States: Array = []
var Settings: Resource
var Helpers: Resource
var SS: Dictionary = {}  # SuperSystems Name Space

var Networking: Node
var LoadingScreen: ReferenceRect
var IntegrationTestManager: Node
var itm_thread_num:int 
var current_state: int = 0
var previous_state: int = 0
var the_state_stack: Array = []
var loading_thread_num = null
var start_pixel = Color(0,0,0,1)
var pixel_goal = Color(1, 1, 1, 1)
var splash_screen_loaded = false
var wait_some_frames = 0
var submodules_ready = false
var FreecoiLInterface: Resource

const GODOT_MAX_INT = 9223372036854775807

func _ready():
    # Sanity check our variables
    var bad_configuration = false
    if run_GUT_unit_tests:
        if run_ITM:
            bad_configuration = true
            printerr("PM: Configuration Error: You can not both run GUT and " +
                "ITM at the same time."
            )
    elif run_ITM:
        if activate_ITM_Player and activate_ITM_Recorder:
            bad_configuration = true
            printerr("PM: Configuration Error: You can not activate ITM Player" +
                " and ITM Recorder at the same time."
            )
        elif not activate_ITM_Player and not activate_ITM_Recorder:
            bad_configuration = true
            printerr("PM: Configuration Error: You MUST activate either ITM " +
                "Player or ITM Recorder if run_ITM is true."
            )
    if bad_configuration:
        get_tree().quit(31)  # ERR_INVALID_PARAMETER = 31 --- Invalid parameter error.

func evaluate_state_change(next_state):
    if next_state == -1:
        the_state_stack.pop_back()
        next_state = the_state_stack[-1]
    elif next_state != current_state:
        the_state_stack.append(next_state)
    if next_state != current_state:
        States[current_state].on_exit()
        previous_state = current_state
        Settings.Session.set_data("ProgramManager_state_trigger", null)
        current_state = next_state
        States[current_state].on_enter(
                    current_state, previous_state)

func _process(delta) -> void:
    if not loaded_GUT_once:
        loaded_GUT_once = true
        if run_GUT_unit_tests:
            load_GUT()  
    if not loaded_ITM_once:
        loaded_ITM_once = true
        if run_ITM:
            load_ITM()
    if not ran_erase_user_data_dir:
        ran_erase_user_data_dir = true
        erase_user_data_dir()
    if not splash_screen_loaded:
        # Need to make sure that the splash screen gets drawn and let the engine settle out some.
        if wait_some_frames >= 6:  
#            var data = get_viewport().get_texture().get_data()
#            data.lock()
#            data.flip_y()
#            var new_zero = data.get_height() - 960
#
#            var start_search_at = new_zero + get_tree().root.get_node(
#                    "Container/Scene0/Splash/CenterContainer/VBoxContainer/ColorRect"
#                    ).rect_position.y
#            var pixel
#            pixel = data.get_pixel(262, start_search_at)
#            var searching_at = start_search_at
#            while pixel != pixel_goal:
#                searching_at += 1
#                pixel = data.get_pixel(262, searching_at)
#                if searching_at > start_search_at + 560:
#                    break
#            data.unlock()
#            if(pixel == null):
#                start_pixel = pixel
#            elif(pixel_goal == pixel):
            splash_screen_loaded = true
            call_deferred("load_and_setup_Helpers")
            call_deferred("load_and_setup_Settings")
            call_deferred("background_load_splash_state")
        else:
            wait_some_frames += 1           
    if len(States) > 0:
        if States[current_state].state_has_on_process:
            var next_state = States[current_state].on_process_event(delta)
            evaluate_state_change(next_state)
    
func _physics_process(delta) -> void:
    if len(States) > 0:
        if States[current_state].state_has_on_physics_process:
            var next_state = States[current_state].on_physics_process_event(delta)
            evaluate_state_change(next_state)
        
func _input(event) -> void:
    if len(States) > 0:
        if States[current_state].state_has_on_input:
            var next_state = States[current_state].on_input_event(event)
            evaluate_state_change(next_state)

func _unhandled_input(event) -> void:
    if len(States) > 0:
        if States[current_state].state_has_on_unhandled_input:
            var next_state = States[current_state].on_unhandled_input_event(event)
            evaluate_state_change(next_state)

func load_and_setup_Helpers():
    Helpers = load("res://Logic/Helpers.gd").new()

func load_and_setup_Settings():
    Settings = load("res://Logic/Settings.gd").new()
    submodules_ready = true

func background_load_splash_state():
    while not submodules_ready:
        yield(get_tree(), "idle_frame")
    Settings.Session.set_data("ProgramManager_state_trigger", null)
    loading_thread_num = PM.Helpers.threaded_background_loader("res://Logic/ProgramManager/MainScreens/Splash.gd")
    Settings.Session.connect(PM.Settings.Session.monitor_data("Helpers_background_load_result"), self,
                        "background_loaded_splash_state")

func background_loaded_splash_state(background_results):
    var results = background_results[loading_thread_num]
    if results == "finished":
        var starting_state_resource = Helpers.get_loaded_resource_from_background(loading_thread_num)
        Settings.Session.disconnect(PM.Settings.Session.monitor_data("Helpers_background_load_result"), self,
                        "background_loaded_splash_state")
        var starting_state_instance = starting_state_resource.new()
        States.append(starting_state_instance)
        if States[current_state].state_has_on_ready:
            States[current_state].on_enter(current_state, previous_state)
            var next_state = States[current_state].on_ready_event()
            evaluate_state_change(next_state)

func background_load_resource(resource_path, object, method, with_callback=true):
    if with_callback:
        Settings.Session.connect(PM.Settings.Session.monitor_data("Helpers_background_load_result"), object, method)
    loading_thread_num = Helpers.threaded_background_loader(resource_path)
    
func disconnect_background_load_resource(object, method):
    Settings.Session.disconnect(PM.Settings.Session.monitor_data("Helpers_background_load_result"), object, method)
    
func call_next_frame(obj, method):
    yield(get_tree(),"idle_frame")
    obj.call_deferred(method)

func load_ITM():
    splash_screen_loaded = true
    call_deferred("load_and_setup_Helpers")
    call_deferred("load_and_setup_Settings")
    while not submodules_ready:
        yield(get_tree(), "idle_frame")
    var ITM_path
    if activate_ITM_Player:
        ITM_path  = "res://tests/TestingHelpers/TestManager/ITM_Player/ITM_Player.tscn"
    else:
        ITM_path = "res://tests/TestingHelpers/TestManager/ITM_Recorder/ITM_Recorder.tscn"
    itm_thread_num = PM.Helpers.background_loader_n_callback(
        ITM_path, self, "setup_itm"
    )

func setup_itm(background_results):
    var results = background_results[itm_thread_num]
    if results == "finished":
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                itm_thread_num, self, "setup_itm").instance()
        get_tree().root.add_child(loaded_scene)
        call_deferred("background_load_splash_state")

func parse_command_line_args():
    var arguments = {}
    for argument in OS.get_cmdline_args():
        # Parse valid command-line arguments into a dictionary
        if argument.find("=") > -1:
            var key_value = argument.split("=")
            arguments[key_value[0].lstrip("--")] = key_value[1]
    return arguments
    
func erase_user_data_dir():
    while not submodules_ready:
        yield(get_tree(), "idle_frame")
    var returned  = PM.Helpers.get_dir_contents("user://")
    for file_path in returned[0]:
        var dir = Directory.new()
        dir.open(file_path)
        dir.remove(file_path)

func load_GUT():
    splash_screen_loaded = true
    # warning-ignore:RETURN_VALUE_DISCARDED
    get_tree().change_scene("res://tests/TestRunner.tscn")
    var gut_loaded = false
    while not gut_loaded:
        yield(get_tree(), "idle_frame")
        if get_tree().root.has_node("Gut"):
            if get_tree().root.get_node("Gut")._gut != null:
                gut_loaded = true
    get_tree().root.get_node("Gut").get_gut().get_gui().rect_scale.x = 0.73
