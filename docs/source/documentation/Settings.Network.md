# PM.Settings.Network

Network Settings include those variables that are NOT saved locally (NOT maintained between sessions) but are passed over the network (Network Sync).

## "peers_to_mups"

A dictionary that has keys of the Network Peers IDs which are ints but are stored as a string. The value is the mup which is a string equal to OS.get_unique_id() my_unique_persistant_id: AKA: MUPID, MUP_ID, and a part of MUPS.



