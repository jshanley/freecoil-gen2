extends "res://addons/gut/test.gd"

func before_all():
    while PM.Settings.Session.get_data("game_started") != 1:
        yield(get_tree(), "idle_frame")
    yield(get_tree().create_timer(1), "timeout")
    
func test_can_see_game_history():
    assert_eq(PM.Networking.EventSync.events_history.size(), 51)

func test_can_advance_to_game_state_lvl_3():
    pass
