extends "res://addons/gut/test.gd"

func before_all():
    while PM.Settings.Session.get_data("game_started") != 1:
        yield(get_tree(), "idle_frame")
    yield(get_tree().create_timer(1), "timeout")
    
func test_can_see_game_history():
    assert_eq(PM.Networking.EventSync.events_history.size(), 51)

func test_can_advance_to_game_state_lvl_3():
    assert_eq(PM.SS["GameCommon"].game_state_by_mup.size(), 2, "Size failed.")
    assert_eq(PM.SS["GameCommon"].game_state_by_mup[PM.ITM_fake_server_unique_id], 3, "Wrong game state.")
    gut.p(PM.SS["GameCommon"].game_state_by_mup)
    yield(get_tree().create_timer(1.5), "timeout")
    #gut.p(PM.Networking.EventSync.events_history)
