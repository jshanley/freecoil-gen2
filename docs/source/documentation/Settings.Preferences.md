# PM.Settings.Preferences

Session Preferences include those variables that are saved locally (maintained between sessions) though not passed over the network.

## "ThemeName"

App theme (currently only the default is available).

## "player_name" (string)

Username of the person using the device.

## "player_avatar" (string)

File path of the avatar that has been selected by the player. 

## "recoil_action"

Preference for enable/disable of recoil action of the player's gun. This setting is overridden by the InGame force_recoil_action variable.

### "recoil_action" values

* true == The recoil action of the current player's gun is preferred as enabled.
* false == The recoil action of the current player's gun is preferred as disabled.

## "narrator"

Setting to enable/disable the narrator audio in the app.

### "narrator" values

* true == The narrator audio is enabled.
* false == The narrator audio is disabled.

## "saved_weapons" (dictionary)

Defines the weapon name (key) and mac address (value).

### "saved_weapons" layout (name: mac address)

```
{"name1": mac address 1, 
"name2": mac address 2}
```

## "default_game_settings"

Contains all of the default game settings for each game mode.  This same dictionary format is used in the Settings.InGame area for the current game settings.  Thus, the individual key variable descriptions can be found in the Settings.InGame.md document.

### "default_game_settings" (example: "deathmatch_settings", "capturetheflag_settings")

An example ("deathmatch_settings") of default game settings is shown below.

```
{"game_mode": "Deathmatch", 
"team_amount": 0, 
"game_objective": "Kill Count",
"game_objective_goal": 10,
"game_length_time": 30,
"respawns_allowed": 5,
"respawn_type": "Location",
"respawn_delay": 30,
"initial_health": 10,
"initial_shield": 5,
"friendly_fire": false,
"force_recall_action":false,
"indoor_game": false,
"healing_radius: 10,
"heal_per_bandage": 3,
"healing_time": 10,
"initial_medpacks": 8}
```

## "saved_armories" (dictionary of dictionaries)

Contains the inventories for the 4 custom user armories (weapon characteristics).  See "preset_armory" in Settings.InGame.md for the format of a single inventory.

### "saved_armories" values

The following dictionary includes the preset armory names (name being subject to change by user).  Armory 1 through 4 Dictionaries are initially populated with the preset_armory (see Session variables, also subject to change by user).
```
{"Armory 1": {Armory 1 Dictionary}, "Armory 2": {Armory 2 Dictionary}, "Armory 3": {Armory 3 Dictionary}, "Armory 4": {Armory 4 Dictionary}}
```

## "default_armory_name"

The key (name) of the armory that is set as the default.
