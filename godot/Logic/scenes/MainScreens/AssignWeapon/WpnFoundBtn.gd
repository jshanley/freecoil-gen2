extends Button

var selected = false

func _ready():
    var __ = connect("button_down", self, "_on_wpn_found_btn_pressed")

func _on_wpn_found_btn_pressed():
    if not selected:
        selected = true
        modulate = Color(1, 0.35, 0, 1)
        PM.Settings.Session.set_data("fi_weapon_highlighted", editor_description)
        yield(get_tree(), "idle_frame")
        get_parent().deselect_all_others(name)

func deselect():
    selected = false
    pressed = false
    modulate = Color(1, 1, 1, 1)
