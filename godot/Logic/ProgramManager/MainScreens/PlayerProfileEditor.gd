extends Resource

#"state_has" variables are for the types of functions that would be used in this state
var state_has_on_process = false #Should be "false" if not used
var state_has_on_physics_process = false #Should be "false" if not used
#Should be "false" if not used, May be used for mouse or button tracking if PC use is expanded in the future
var state_has_on_input = false 
#Should be "false" if not used, Used for more global inputs (i.e. screen swipe that 
# aren't directly associated with a specific control/button)
var state_has_on_unhandled_input = false 
const STATE_ID:String = "PlayerProfileEditor" #Name of the scene that corresponds to this State Class
var state_num: int #No longer used, replaced with Match on StateID (sometimes used for internal tracking)
#Dictionary that locally maps the State numbers to StateID's for external related State Classes
var transitionable_states: Dictionary = {} 
var required_for_state_is_complete = false #Initial value, Guard variable
var loading_next_state_already = false #Initial value, Guard variable
var started_background_loading = false #Initial value, Guard variable
var my_screen_offset: String = "0,0" #Initial value
var my_screen_name: String = ""
var next_state: String
var scene_loaded = false

func on_enter(state_num_assigned, _previous_state) -> void:
    #state_num is used for on_process_event()->on_unhandeled_input_event(), etc returned values to PM
    state_num = state_num_assigned 
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "info") #Info only
    load_required_for_state()
    #Connect all the triggers that are needed for this State Class
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger") #General use (generic) trigger
    #Control to prevent the user from navigating to other screens without the State Class being aware
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    screen_change(null)

func on_exit() -> void: #Perform general cleanup of this State Class
    #Remove connections that were used for this State Class
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger")
    #Perform general cleanup
    call_deferred("cleanup")

func state_trigger(new_value): #Function for general-use trigger
    if new_value == true:
        if PM.Networking.get_tree().is_network_server():
            next_state = "GameSettingsReview"
        else:
            next_state = "Pregame"
        transition_to_next_state()
    elif new_value == false:
        PM.call_deferred("evaluate_state_change", -1)

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    # Guard against switching and screen switch fighting if we have not yet entered this state.
    if state_num == 0:
        pass
    elif PM.current_state != state_num:
        pass
    else:
        if current_screen == null:
            if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
                PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        else:
            #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
            if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
                PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        
func load_required_for_state():
    #Func Description:
        #Func Desc 1 - Compile list ("match state.STATE_ID") of other State Classes 
                    # that are involved in this State Class (i.e. those that are related 
                    # with the next potential scenes)?
        #Func Desc 2 - Background load the scene/screen that corresponds with this State Class
    #Used as a guard variable, preventing loading of resource twice, see background_load_state_ahead()
    if not started_background_loading: 
        started_background_loading = true
        var counter = 0
        for state in PM.States: #Populates the "transitionable_states" dictionary
            match state.STATE_ID:
                "GameSettingsReview": 
                    transitionable_states[state.STATE_ID] = counter
                "Pregame":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        #Loads the scene that corresponds to this State Class
        PM.background_load_resource("res://scenes/MainScreens/PlayerProfileEditor.tscn", self, "setup_required_for_state")
        scene_loaded = true

#When the resource is ready, set it up and draw it to screen  
func setup_required_for_state(background_results): 
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        PM.disconnect_background_load_resource(self, "setup_required_for_state")
        var loaded_scene = PM.Helpers.get_loaded_resource_from_background(
                PM.loading_thread_num).instance() #Initialization of the scene
        #Obtaining scene/screen location from Edittor Description
        my_screen_offset = loaded_scene.editor_description 
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene) #Adding scene to the view and running the screen/scene
        yield(PM.get_tree(), "idle_frame") #Wait 1 frame for on_ready and drawing
        required_for_state_is_complete = true
        screen_change(null)

func background_load_state_ahead():  # This is now optional for preloading the next scene/screen.
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():  # This is always called by the function above.
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame") #Yields to a frame until fully loaded
    transition_to_next_state()
        
# This is now the prefered method of transitioning to the next state.  Transitions the entire State Class.
func transition_to_next_state():  
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    #Deletes the screen/scene
    if scene_loaded:
        PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free() 
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
