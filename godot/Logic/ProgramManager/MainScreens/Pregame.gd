extends Resource

#"state_has" variables are for the types of functions that would be used in this state
var state_has_on_process = true #Should be "false" if not used
var state_has_on_physics_process = true #Should be "false" if not used
#Should be "false" if not used, May be used for mouse or button tracking if PC use is expanded in the future
var state_has_on_input = true 
#Should be "false" if not used, Used for more global inputs (i.e. screen swipe that 
# aren't directly associated with a specific control/button)
var state_has_on_unhandled_input = true 
const STATE_ID:String = "Pregame" #Name of the scene that corresponds to this State Class
var state_num: int #No longer used, replaced with Match on StateID (sometimes used for internal tracking)
#Dictionary that locally maps the State numbers to StateID's for external related State Classes
var transitionable_states: Dictionary = {} 
var required_for_state_is_complete = false #Initial value, Guard variable
var loading_next_state_already = false #Initial value, Guard variable
var started_background_loading = false #Initial value, Guard variable
var my_screen_offset: String = "0,0" #Initial value
var my_screen_name: String = ""
var next_state: String
var thread_num
var scene_loaded = false

var total_stages:float = 16
var completed_stages:float = 0

func on_enter(state_num_assigned, _previous_state) -> void:
    #state_num is used for on_process_event()->on_unhandeled_input_event(), etc returned values to PM
    state_num = state_num_assigned 
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "info") #Info only
    load_required_for_state()
    #Connect all the triggers that are needed for this State Class
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger") #General use (generic) trigger
    #Control to prevent the user from navigating to other screens without the State Class being aware
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")

func on_exit() -> void: #Perform general cleanup of this State Class
    #Remove connections that were used for this State Class
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data(
            "ProgramManager_state_trigger"), self, "state_trigger")
    #Perform general cleanup
    call_deferred("cleanup")

func state_trigger(new_value): #Function for general-use trigger
    next_state = new_value
    if next_state == "MapSelect" or next_state == "ArmorySettings":
        PM.Settings.Session.set_data("scene_routing_indicator", "OfflineSettings")
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    # Guard against switching and screen switch fighting if we have not yet entered this state.
    if state_num == 0:
        pass
    elif PM.current_state != state_num:
        pass
    else:
        if current_screen == null:
            if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
                PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        else:
            #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
            if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
                PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
        
func load_required_for_state():
    #Func Description:
        #Func Desc 1 - Compile list ("match state.STATE_ID") of other State Classes 
                    # that are involved in this State Class (i.e. those that are related 
                    # with the next potential scenes)?
        #Func Desc 2 - Background load the scene/screen that corresponds with this State Class
    #Used as a guard variable, preventing loading of resource twice, see background_load_state_ahead()
    if not started_background_loading: 
        started_background_loading = true
        var counter = 0
        for state in PM.States: #Populates the "transitionable_states" dictionary
            match state.STATE_ID:
                "GameSettingsReview": #REPLACE WITH LOBBY?
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        #Loads the scene that corresponds to this State Class
        PM.LoadingScreen.fade_in()
        PM.LoadingScreen.loading_progress(1.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/Playing.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/Playing.tscn", self, 
                "setup_required_for_state")

#When the resource is ready, set it up and draw it to screen  
func setup_required_for_state(background_results): 
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "setup_required_for_state").instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/LPanel.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/LPanel.tscn", self, 
                "stage_6_loading")

func stage_6_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_6_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/RPanel.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/RPanel.tscn", self, 
                "stage_7_loading")

func stage_7_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_7_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/Footer.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/Footer.tscn", self, 
                "stage_8_loading")
                
func stage_8_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_8_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/RadioComms.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/RadioComms.tscn", self, 
                "stage_9_loading")
                
func stage_9_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_9_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/RadioHistory.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/RadioHistory.tscn", self, 
                "stage_10_loading")
 
func stage_10_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_10_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/WaitingPanel.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/WaitingPanel.tscn", self, 
                "stage_11_loading")               

func stage_11_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_11_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/Scoreboard.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/Scoreboard.tscn", self, 
                "stage_12_loading")   

func stage_12_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_12_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/Respawn.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/Respawn.tscn", self, 
                "stage_13_loading") 

func stage_13_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_13_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/Eliminated.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/Eliminated.tscn", self, 
                "stage_14_loading") 

func stage_14_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_14_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/End of Game.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/End of Game.tscn", self, 
                "stage_15_loading") 

func stage_15_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_15_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/StartGameDelayPanel.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/StartGameDelayPanel.tscn", self, 
                "stage_16_loading")
                
#func stage_16_loading(background_results):
#    var results = background_results[thread_num]
#    if results == "finished":
#        completed_stages += 1
#        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
#        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
#                thread_num, self, "stage_16_loading").instance()
#        PM.get_tree().root.get_node("Container").current_scene.add_child(
#                loaded_scene)
#        yield(PM.get_tree(), "idle_frame")
#        PM.LoadingScreen.loading_item_name("res://scenes/InGame/StartGameDelayPanel.tscn")
#        thread_num = PM.Helpers.background_loader_n_callback(
#                "res://scenes/InGame/StartGameDelayPanel.tscn", self, 
#                "stage_17_loading")

func stage_16_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        completed_stages += 1
        PM.LoadingScreen.loading_progress(completed_stages / total_stages * 100.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_16_loading").instance()
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        call_deferred("finished_loading")

func finished_loading():
    PM.LoadingScreen.loading_progress(100.0)
    PM.LoadingScreen.fade_out()
    required_for_state_is_complete = true
    screen_change(null)
    PM.SS["GameCommon"].in_game_added_to_tree()

func background_load_state_ahead():  # This is now optional for preloading the next scene/screen.
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():  # This is always called by the function above.
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame") #Yields to a frame until fully loaded
    transition_to_next_state()
        
# This is now the prefered method of transitioning to the next state.  Transitions the entire State Class.
func transition_to_next_state():  
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    #Deletes the screen/scene
    if scene_loaded:
        PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free() 
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
