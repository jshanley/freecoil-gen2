extends NinePatchRect

var thread_name

func _ready():
    PM.Settings.Preferences.connect(PM.Settings.Preferences.monitor_data("player_avatar"), self, "avatar_selected")
    var player_avatar = PM.Settings.Preferences.get_data("player_avatar")
    if player_avatar != null and player_avatar != "":
        avatar_selected(player_avatar)

func avatar_selected(avatar_path):
    thread_name = PM.Helpers.background_loader_n_callback(avatar_path, self, "setup_avatar")
    
func setup_avatar(background_results):
    var results = background_results[thread_name]
    if results == "finished":
        texture = PM.Helpers.get_resource_from_background_n_disconnect(thread_name, self, "setup_avatar")
