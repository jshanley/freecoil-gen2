extends Resource

#"state_has" variables are for the types of functions that would be used in this state
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "PreGameSettings" #Main state for this script.
var state_num: int #No longer used, replaced with Match on StateID
var transitionable_states: Dictionary = {} #Enables local mapping of State number to StateID
var required_for_state_is_complete = false #Initial value
var loading_next_state_already = false #Initial value
var started_background_loading = false #Initial value
var my_screen_offset = "0,0" #Initial value
var my_screen_name = "" #Initial value
var next_state: String #Used for name of next state (transition)
var cleanup_after_some_frames = 5
var thread_num
var scene_loaded = false

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "info")
    load_required_for_state()
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"
            ), self, "state_trigger") #ProgramManager_state_trigger is a generic variable for general use
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Networking.Server_Module.setup_server_part1()

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"
            ), self, "state_trigger")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value == "Accept":   
        #This section is used to set variables that will be queried for every 
        # permissions state variable and the gun connection state variable
        if PM.Settings.Session.get_data("is_host") == 2:  # Dedicated Host
            PM.Settings.Session.set_data("need_gps", false, false, false)
            PM.Settings.Session.set_data("need_bluetooth", false, false, false)
        elif PM.Settings.Session.get_data("is_soldier") == true:
            if PM.Settings.InGame.get_data("gps_required") == true:
                PM.Settings.Session.set_data("need_gps", true, false, false)
            else:
                PM.Settings.Session.set_data("need_gps", false, false, false)
            PM.Settings.Session.set_data("need_bluetooth", true, false, false)
        elif (PM.Settings.Session.get_data("is_medic") == true):
            #GPS Proximity Medic
            if (PM.Settings.InGame.get_data("medic_type") > 0 
                    and PM.Settings.InGame.get_data("medic_type") < 5):
                PM.Settings.Session.set_data("need_gps", true, false, false)
                if PM.Settings.InGame.get_data("medic_type") > 2:
                    PM.Settings.Session.set_data("need_bluetooth", true, false, false)
                else:
                    PM.Settings.Session.set_data("need_bluetooth", false, false, false)
            #Audio-Sync Medic
            elif (PM.Settings.InGame.get_data("medic_type") > 4):
                if PM.Settings.InGame.get_data("medic_type") == 5:
                    PM.Settings.Session.set_data("need_gps", false, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", false, false, false)
                elif PM.Settings.InGame.get_data("medic_type") == 6:
                    PM.Settings.Session.set_data("need_gps", true, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", false, false, false)
                elif PM.Settings.InGame.get_data("medic_type") == 7:
                    PM.Settings.Session.set_data("need_gps", false, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", true, false, false)
                else:
                    PM.Settings.Session.set_data("need_gps", true, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", true, false, false)
        PM.Settings.InGame.set_data("pregame_settings_completed", true)
        #DEBUG ONLY - Check variables
        PM.Settings.Log("PreGameSettings RESULT:", "debug")
        PM.Settings.Log("gps_required = " + str(PM.Settings.InGame.get_data("gps_required")), "debug")
        PM.Settings.Log("need_gps = " + str(PM.Settings.Session.get_data("need_gps")), "debug")
        PM.Settings.Log("medic_type = " + str(PM.Settings.InGame.get_data("medic_type")), "debug")
        PM.Settings.Log("need_bluetooth = " + str(PM.Settings.Session.get_data("need_bluetooth")), "debug")
        
        #Route to next State Class
        if (PM.Settings.Session.get_data("need_gps") == true or PM.Settings.Session.get_data(
                "need_bluetooth") == true):
            next_state = "FineAccessLocationPermission"
        else:
            next_state = "PlayerNameEditor"
        call_deferred("background_load_state_ahead")
    elif new_value == "GoBack":
        PM.call_deferred("evaluate_state_change", PM.previous_state)

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID: #Search for state variables shown below and register their index
                "FineAccessLocationPermission":
                    transitionable_states[state.STATE_ID] = counter
                "PlayerNameEditor":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        PM.LoadingScreen.fade_in()
        PM.LoadingScreen.loading_progress(1.0)
        PM.LoadingScreen.loading_item_name("res://scenes/MainScreens/PreGameSettings.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/MainScreens/PreGameSettings.tscn", self, 
                "setup_required_for_state")
   
func setup_required_for_state(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(15.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "setup_required_for_state").instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(17.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/GameModes/PreGame.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/GameModes/PreGame.tscn", self, 
                "stage_2_loading")
            
func stage_2_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(49.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_2_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(51.0)
        PM.LoadingScreen.loading_item_name("res://Logic/EverGreen/ECS/Systems/SuperSystems/RandomGenerator/RandomGenerator.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://Logic/EverGreen/ECS/Systems/SuperSystems/RandomGenerator/RandomGenerator.tscn", self, 
                "stage_3_loading")
                
func stage_3_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(66.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_3_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(68.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/GameModes/GameCommon.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/GameModes/GameCommon.tscn", self, 
                "stage_4_loading")

func stage_4_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(83.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_4_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        PM.LoadingScreen.loading_progress(85.0)
        PM.LoadingScreen.loading_item_name("res://scenes/InGame/GameModes/BlinkHexTris.tscn")
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/InGame/GameModes/BlinkHexTris.tscn", self, 
                "stage_5_loading")

func stage_5_loading(background_results):
    var results = background_results[thread_num]
    if results == "finished":
        PM.LoadingScreen.loading_progress(98.0)
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "stage_5_loading").instance()
        PM.get_tree().root.get_node("Container").add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        call_deferred("finished_loading")

func finished_loading():
    PM.LoadingScreen.loading_progress(100.0)
    PM.LoadingScreen.fade_out()
    required_for_state_is_complete = true
    screen_change(null)

func background_load_state_ahead():
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

#Background loading for next state.  Last line calls ProgramManager and it handles the transition and cleanup
func setup_state_ahead(): 
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""
