extends Resource

var state_has_on_process = true
var state_has_on_physics_process = true
var state_has_on_input = true
var state_has_on_unhandled_input = true
const STATE_ID:String = "AwaitingHostPregame"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var loading_next_state_already = false
var started_background_loading = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String
var thread_num: int

func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    print("Program Manager State = " + STATE_ID + " " + str(state_num))
    load_required_for_state()
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.InGame.connect(PM.Settings.InGame.monitor_data("pregame_settings_completed"), self, "pregame_settings_ready")
    pregame_settings_ready(PM.Settings.InGame.get_data("pregame_settings_completed"))

func on_exit() -> void:
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.InGame.disconnect(PM.Settings.InGame.monitor_data("pregame_settings_completed"), self, "pregame_settings_ready")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value != null:
        pass

func on_process_event(_delta) -> int:
    return state_num
    
func on_physics_process_event(_delta) -> int:
    return state_num
        
func on_input_event(_event) -> int:
    return state_num

func on_unhandled_input_event(_event) -> int:
    return state_num

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID: #Search for state variables shown below and register their index
                "FineAccessLocationPermission":
                    transitionable_states[state.STATE_ID] = counter
                "PlayerNameEditor":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        thread_num = PM.Helpers.background_loader_n_callback(
                "res://scenes/MainScreens/AwaitingHostPregame.tscn", self, "setup_required_for_state")
    
func setup_required_for_state(background_results):
    var results = background_results[PM.loading_thread_num]
    if results == "finished":
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                thread_num, self, "setup_required_for_state").instance()
        my_screen_offset = loaded_scene.editor_description
        my_screen_name = loaded_scene.name
        PM.get_tree().root.get_node("Container").current_scene.add_child(
                loaded_scene)
        yield(PM.get_tree(), "idle_frame")
        required_for_state_is_complete = true
        screen_change(null)

func background_load_state_ahead():  # This is now optional.
    PM.States[transitionable_states[next_state]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():  # This is always called by the function above.
    while not PM.States[transitionable_states[next_state]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    transition_to_next_state()
        
func transition_to_next_state():  # This is now the prefereed method of transitioning to the next state.
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    if PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name) != null:
        PM.get_tree().root.get_node("Container").current_scene.get_node(
                my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""

func pregame_settings_ready(new_val):
    if new_val == true:
        #This section is used to set variables that will be queried for every 
        # permissions state variable and the gun connection state variable
        if PM.Settings.Session.get_data("is_soldier") == true:
            if PM.Settings.InGame.get_data("gps_required") == true:
                PM.Settings.Session.set_data("need_gps", true, false, false)
            else:
                PM.Settings.Session.set_data("need_gps", false, false, false)
            PM.Settings.Session.set_data("need_bluetooth", true, false, false)
        elif (PM.Settings.Session.get_data("is_medic") == true):
            #GPS Proximity Medic
            if (PM.Settings.InGame.get_data("medic_type") > 0 
                    and PM.Settings.InGame.get_data("medic_type") < 5):
                PM.Settings.Session.set_data("need_gps", true, false, false)
                if PM.Settings.InGame.get_data("medic_type") > 2:
                    PM.Settings.Session.set_data("need_bluetooth", true, false, false)
                else:
                    PM.Settings.Session.set_data("need_bluetooth", false, false, false)
            #Audio-Sync Medic
            elif (PM.Settings.InGame.get_data("medic_type") > 4):
                if PM.Settings.InGame.get_data("medic_type") == 5:
                    PM.Settings.Session.set_data("need_gps", false, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", false, false, false)
                elif PM.Settings.InGame.get_data("medic_type") == 6:
                    PM.Settings.Session.set_data("need_gps", true, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", false, false, false)
                elif PM.Settings.InGame.get_data("medic_type") == 7:
                    PM.Settings.Session.set_data("need_gps", false, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", true, false, false)
                else:
                    PM.Settings.Session.set_data("need_gps", true, false, false)
                    PM.Settings.Session.set_data("need_bluetooth", true, false, false)
        #Route to next State Variable
        if (PM.Settings.Session.get_data("need_gps") == true or PM.Settings.Session.get_data("need_bluetooth") == true):        
            next_state = "FineAccessLocationPermission"
        else:
            next_state = "PlayerNameEditor"
        call_deferred("background_load_state_ahead")
