extends Resource

func check_bluetooth_status():
    if PM.FreecoiLInterface.FreecoiL != null:
        PM.Settings.Session.set_data("fi_bluetooth_status", 
                PM.FreecoiLInterface.FreecoiL.bluetoothStatus())

func enable_bluetooth():
    if PM.FreecoiLInterface.FreecoiL!= null:
        PM.FreecoiLInterface.FreecoiL.enableBluetooth()

func disable_bluetooth():
    if PM.FreecoiLInterface.FreecoiL!= null:
        PM.FreecoiLInterface.FreecoiL.disableBluetooth()
