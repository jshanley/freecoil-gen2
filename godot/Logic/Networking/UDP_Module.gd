extends Resource

var pp_udp = null
var udp_test = false
var udp_test_from_peer = 0
var udp_test_broadcast_rx_count = 0
var udp_test_broadcast_tx_count = 0
var search_udp_broadcast = false
var __udp_broadcast_tx_time_since = null
var __udp_broadcast_hosting_tx_time_since = null
var host_udp_broadcast = false
var host_udp_broadcast_uid = null
var broadcast_has_been_missing_for = 4

#### UDP BroadCast For LAN
func broadcast_to_peers():
    pp_udp = PacketPeerUDP.new()
    pp_udp.set_broadcast_enabled(true)  # New with 3.2 must have this option enabled.
    pp_udp.set_dest_address("255.255.255.255", PM.Settings.NETWORK_BROADCAST_LAN_PORT)
    PM.Networking.network_loops.append("_hosting_send_udp_broadcast")
    PM.Networking.set_process(true)
    
func search_for_peers():
    PM.Settings.Log("Network: UDP: Search for Peers: Setting up new PacketPeerUDP.", "info")
    pp_udp = PacketPeerUDP.new()
    pp_udp.listen(PM.Settings.NETWORK_BROADCAST_LAN_PORT)
    PM.Settings.Session.register_data("udp_peer_dict", {})
    PM.Settings.Session.register_data("udp_server_dict", {})
    PM.Settings.Session.register_data("udp_server_details_dict", {})
    search_udp_broadcast = true
    PM.Networking.network_loops.append("_udp_broadcast_rxd")
    PM.Networking.set_process(true)
    PM.Settings.Log("Network: UDP: Search for Peers: Setup completed.", "info")
    
func _udp_broadcast_tx():
    if search_udp_broadcast:
        if __udp_broadcast_tx_time_since == null:
            __udp_broadcast_tx()
        elif OS.get_ticks_msec() - __udp_broadcast_tx_time_since > 500:
            __udp_broadcast_tx()
        else:
            pass
    else:
        PM.Networking.network_loops.remove("_udp_broadcast_tx")

func __udp_broadcast_tx():
    udp_test_broadcast_tx_count += 1
    pp_udp.put_var([PM.Settings.UDP_BROADCAST_GREETING, PM.Settings.Session.get_data("server_ip"), 
        PM.Settings.Session.get_data("server_port"), host_udp_broadcast_uid])
    __udp_broadcast_tx_time_since = OS.get_ticks_msec()
    
func _udp_broadcast_rxd():
    while pp_udp.get_available_packet_count() > 0:
        var udp_peer_dict = PM.Settings.Session.get_data("udp_peer_dict")
        var udp_server_dict = PM.Settings.Session.get_data("udp_server_dict")
        var udp_server_details_dict = PM.Settings.Session.get_data("udp_server_details_dict")
        var udp_var = pp_udp.get_var()
        var udp_peer_ip = pp_udp.get_packet_ip()
        var update_udp_data = false
        if PM.Networking.testing:
            udp_test = udp_var
            udp_test_broadcast_rx_count += 1
        if typeof(udp_var) == TYPE_ARRAY:
            var use_host_udp = true
            if PM.Settings.debug_toggles.has("self_host_ok"):
                pass
            else:
                if udp_var[3] == host_udp_broadcast_uid:
                    PM.Settings.Log("Network: UDP: Got broadcast from self.")
                    use_host_udp = false  # Throw it away.
            if use_host_udp:
                if udp_var[0] == PM.Settings.UDP_BROADCAST_GREETING:
                    PM.Settings.Log("PM.Networking: UDP: Got UDP Peer Greeting broadcast from: " +  
                            udp_peer_ip + " of " + str(udp_var), "debug")
                    udp_peer_dict[udp_peer_ip] = OS.get_system_time_secs()
                    update_udp_data = true
                    if PM.Networking.testing:
                        udp_test_from_peer = udp_var[3]
                elif udp_var[0] == PM.Settings.UDP_BROADCAST_HOST:
                    PM.Settings.Log("PM.Networking: UDP: Got UDP Server Greeting broadcast from: " + 
                            udp_peer_ip + " of " + str(udp_var), "debug")
                    udp_server_dict[udp_peer_ip] = OS.get_system_time_secs()
                    udp_server_details_dict[udp_peer_ip] = udp_var
                    update_udp_data = true
#                if not PM.Settings.Session.get_data("server_invite"):
#                    if udp_var[1] in PM.Settings.Session.get_data("server_ignore_list"):
#                        pass
#                    else:
#                        PM.Settings.Session.set_data("server_invite", true)
#                        PM.Settings.Log("PM.Networking: UDP: Client found server's address = " + udp_var[1] + 
#                            ":" + str(udp_var[2]), "info")
#                        PM.Settings.Session.set_data("server_ip", udp_var[1])
#                        PM.Settings.Session.set_data("server_port", udp_var[2])
#                        PM.Networking.stop_udp_peer_search()
#                        PM.Networking.Client.setup_as_client()
#                        #PM.Networking.get_tree().call_group("Container", "load_lobby") 
#                        if PM.Networking.testing:
#                            udp_test_from_peer = udp_var[3]             
        # Remove peers that have not broadcast lately. > 3 seconds.
        # Remove peers that have not broadcast lately. > 3 seconds.
        for udp_peer in udp_peer_dict:
            if OS.get_system_time_secs() - udp_peer_dict[udp_peer] > broadcast_has_been_missing_for:
                udp_peer_dict.erase(udp_peer)
                update_udp_data = true
        for udp_peer in udp_server_dict:
            if OS.get_system_time_secs() - udp_server_dict[udp_peer] > broadcast_has_been_missing_for:
                udp_server_dict.erase(udp_peer)
                udp_server_details_dict.erase(udp_peer)
                update_udp_data = true
        if update_udp_data:
            PM.Settings.Session.set_data("udp_server_dict", udp_server_dict)
            PM.Settings.Session.set_data("udp_server_details_dict", udp_server_details_dict)
            PM.Settings.Session.set_data("udp_peer_dict", udp_peer_dict)
            
func _hosting_so_toss_udp_broadcast():
    search_udp_broadcast = false
    if "_udp_broadcast_tx" in PM.Networking.network_loops:
        PM.Networking.network_loops.remove("_udp_broadcast_tx")
    if pp_udp != null:
        while pp_udp.get_available_packet_count() > 0:
            var udp_var = pp_udp.get_var()
            var udp_peer_ip = pp_udp.get_packet_ip()
            if udp_peer_ip in IP.get_local_addresses():
                pass  # Throw it away.
            else:
                if typeof(udp_var) == TYPE_ARRAY:
                    if udp_var[0] == PM.Settings.UDP_BROADCAST_HOST:
                        PM.Settings.Log("Network: UDP: Warning: Another server is hosting at " + udp_var[1] + 
                            ":" + str(udp_var[2]), "info")
                        PM.Settings.Log("        Verified IP Address is: " + udp_peer_ip, "warning")

func _hosting_send_udp_broadcast():
    if __udp_broadcast_hosting_tx_time_since == null:
        __hosting_send_udp_broadcast()
    elif OS.get_ticks_msec() - __udp_broadcast_hosting_tx_time_since > 500:
        __hosting_send_udp_broadcast()
    else:
        pass

func __hosting_send_udp_broadcast():
    pp_udp.put_var([PM.Settings.UDP_BROADCAST_HOST, PM.Settings.Session.get_data("server_ip"), 
            PM.Settings.Session.get_data("server_port"), host_udp_broadcast_uid, 
            PM.Settings.InGame.get_data("game_name"), PM.Settings.InGame.get_data("gps_required"),  
            PM.Settings.InGame.get_data("medic_type")])
    __udp_broadcast_hosting_tx_time_since = OS.get_ticks_msec()
    #PM.Settings.Log("Network: UDP: Sent UDP Host Broadcast.", "info")
 
func udp_peer_selected(peer_ip):
    PM.Settings.Log("Network: UDP: UDP Peer Selected: " + str(peer_ip), "info")
    stop_udp_peer_search()   
    PM.Settings.Session.set_data("players_ip", peer_ip)
    
func stop_udp_peer_search():
    PM.Settings.Log("Network: UDP: Stopping UPD Peer Search.", "info")
    search_udp_broadcast = false
    PM.Networking.network_loops.remove("_udp_broadcast_rxd")
    PM.Networking.network_loops.remove("_udp_broadcast_tx")
    
