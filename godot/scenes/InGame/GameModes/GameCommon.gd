extends Node

###############################################################################
# SuperSystems are just nodes that are excessable via the namespace:
# PM.SS["Example"]
var SuperSystemID = "GameCommon"

export(int) var editor_yield_duration: int = 60

var game_over = false
var events_not_sent = []
var events_not_acknowledged = []
var server_unackn_events_by_mup = {}
var game_history = []
var rxd_events = []
var event_counter = 0
var event_template = {"time": null, "event_id": null, "type": null, 
        "rec_by": null, "additional": {}}
var catch_up_active = false
var last_connection_status = null
var next_available_player_laser_id = 1
# 0 is not loaded and not_ready, 1 is loaded but not ready, 2 is loaded and ready/waiting,
# 3 is actively playing, 4 is game over
var game_state_by_mup = {}
var change_weapon_in_process = false
var min_frame_rate:float = 1.0 / 240.0
var frame_start_time: float
var all_in_game_ui_added_to_tree = false
var generic_timer_limit: int
var all_process_events_handled = true
var in_game_added_to_tree_complete = false

onready var ReloadSound = get_node("ReloadSound")
onready var EmptyShotSound = get_node("EmptyShotSound")
onready var GunShotSound = get_node("GunShotSound")
onready var TangoDownSound = get_node("TangoDownSound")
onready var NiceSound = get_node("NiceSound")
onready var HitIndicatorTimer = get_node("HitIndicatorTimer")
onready var RespawnTimer = get_node("RespawnDelayTimer")
onready var ReloadTimer = get_node("ReloadTimer")
onready var TickTocTimer = get_node("TickTocTimer")
onready var EventRecordTimer = get_node("EventRecordTimer")
onready var StartGameTimer = get_node("StartGameTimer")
onready var TimeRemainingTimer = get_node("TimeRemainingTimer")
var LPanel: ReferenceRect
var RPanel: ReferenceRect
var Footer: ReferenceRect
var StartPanel: ReferenceRect
var WaitingPanel: ReferenceRect
var RespawnPanel: ReferenceRect

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
    if Engine.editor_hint:
        # Code to execute in editor.
        on_editor_ready()
    if not Engine.editor_hint:
        # Code to execute in game.
        PM.SS[SuperSystemID] = self  # Set reference in PM namespace.
        on_real_ready()

func _process(_delta) -> void:
    # Process Alerts
    pass
                    
#
#func _physics_process(delta):
#    pass
#
#func _input(event):
#    pass
#
#func _unhandled_input(event):
#    pass

func process_event_all_other_events(event):
    if event["type"] == "fired":
        process_event_fired(event)
    elif event["type"] == "misfired":
        process_event_misfired(event)
    elif event["type"] == "reloading":
        process_event_reloading(event)
    elif event["type"] == "died":
        process_event_died(event)
    elif event["type"] == "hit":
        process_event_hit(event)
    elif event["type"] == "eliminated":
        process_event_eliminated(event)
    elif event["type"] == "end_game":
        process_event_end_game(event)
    elif event["type"] == "test_client":
        process_event_test_client(event)
    elif event["type"] == "test_server":
        process_event_test_server(event)


func check_if_frame_time_exceeded():
    if all_process_events_handled:  # No need to keep looping in process, if everything is done.
        return true
    var time_passed = OS.get_ticks_msec() - frame_start_time
    if time_passed > min_frame_rate * 1000:
        return true
    return false

func on_editor_ready() -> void:
    pass
    
func on_real_ready() -> void:
    PM.Networking.EventSync.event_processing_hooks_refs["GameCommon"] = self
    if PM.Settings.Session.get_data("is_host") >= 1:
        PM.Networking.set_process(true)
        PM.Networking.EventSync.is_a_client = false
    fix_looping_audio_to_not()
    if PM.Settings.Session.get_data("is_host") >= 1:
        var mups_to_peers = PM.Settings.Network.get_data("mups_to_peers")
        for mup in mups_to_peers:
            PM.SS["GameCommon"].game_state_by_mup[mup] = 0  # 0 is not loaded and ready
        PM.Settings.Network.connect(PM.Settings.Network.monitor_data("mups_status"), 
                self, "on_new_identified_connection")
        PM.Settings.Session.connect(PM.Settings.Session.monitor_data("mups_reconnected"), self, 
                "on_mup_reconnected")
        PM.Settings.Session.set_data("players_laser_id_by_mup", {})
        invert_mups_to_lasers(PM.Settings.Session.get_data("players_laser_id_by_mup"))
        if PM.run_ITM and PM.ITM_act_as_server:
            get_next_available_player_laser_id(PM.ITM_fake_server_unique_id)
        else:
            get_next_available_player_laser_id(OS.get_unique_id())
        server_setup_ingame_vars()
    else:
        while not PM.Networking.server_time_deviation_is_acceptable():
            yield(get_tree(), "idle_frame")
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "get_next_available_player_laser_id", "object": "GameCommon"
            }, PM.Networking.get_server_time_usec()
        )
        while not all_in_game_ui_added_to_tree:
            yield(get_tree(), "idle_frame")
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "notify_game_state_change", "object": "GameCommon", 
            "args": [1]}, PM.Networking.get_server_time_usec()
        )
    #FIXME: The below code is temporary, just for testing (Eric update - code made less temporary).
    if PM.Settings.Session.get_data("force_recoil_action") == null:
        PM.Settings.Session.set_data("force_recoil_action", false)
    var force_recoil_action = PM.Settings.Session.get_data("force_recoil_action")
    var recoil_action = PM.Settings.Preferences.get_data("recoil_action")
    if force_recoil_action == true or recoil_action == true:
        PM.FreecoiLInterface.enable_recoil(true)
    elif force_recoil_action == false and recoil_action == false:
        PM.FreecoiLInterface.enable_recoil(false)
    # Make Connections
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_trigger_btn_counter"
            ), self, "fi_trigger_btn_counter_event")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_reload_btn_counter"
            ), self, "fi_reload_btn_counter_event")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_power_btn_counter"
            ), self, "fi_power_btn_counter_event")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_thumb_btn_counter"
            ), self, "fi_thumb_btn_counter_event")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("connection_status"
            ), self, "connection_status_event")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_shooter1_shot_counter"
            ), self, "fi_shot_by_shooter1")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_shooter2_shot_counter"
            ), self, "fi_shot_by_shooter2")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("game_status"
            ), self, "game_status_update")
    if PM.Settings.Session.get_data("is_host") >= 1:
        set_player_start_game_vars()
    else:
        pass

func in_editor_yield():
    for i in range(editor_yield_duration):
        yield(get_tree(), "idle_frame")

func invert_mups_to_lasers(mups_to_lasers):
    if PM.Settings.Session.get_data("is_host") >= 1:
        var lasers_to_mups = {}
        for key in mups_to_lasers:
            lasers_to_mups[mups_to_lasers[key]] = key
        # NOTE: For JSON Objects must have keys that are strings not Integers.
        # Invert players and do not store in JSON.
        PM.Settings.Session.set_data("players_mup_by_laser_id", lasers_to_mups)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "players_mup_by_laser_id", "var_val": lasers_to_mups},
            PM.Networking.get_server_time_usec()
        )

func fi_trigger_btn_counter_event(_counter):
    if PM.Settings.Session.get_data("game_player_alive"):
        if PM.Settings.Session.get_data("game_weapon_magazine_ammo") == 0:
            EmptyShotSound.volume_db = 0
            EmptyShotSound.play()
            PM.Networking.EventSync.record_event("misfired", 
                {"gun": PM.Settings.Session.get_data("game_weapon_type")}, 
                PM.Networking.get_server_time_usec()
            )
        else:
            GunShotSound.volume_db = 0
            GunShotSound.play()
            PM.Networking.EventSync.record_event("fired", 
                {"gun": PM.Settings.Session.get_data("game_weapon_type")},
                PM.Networking.get_server_time_usec()
            )
    #else you are dead so pass.
            
    
func fi_reload_btn_counter_event(__):
    if PM.Settings.Session.get_data("game_player_alive"):
        reload_start()

func fi_power_btn_counter_event(__):
    var force_recoil_action = PM.Settings.Session.get_data("force_recoil_action")
    if force_recoil_action == null or force_recoil_action == false:
        if PM.FreecoiLInterface.recoil_enabled:
            PM.FreecoiLInterface.enable_recoil(false)
        else:
            PM.FreecoiLInterface.enable_recoil(true)

func fi_thumb_btn_counter_event(__):
    if PM.Settings.Session.get_data("game_player_alive"):
        change_weapon(null)
                
func fi_shot_by_shooter1(__):
    process_shot_by_shooter(1, PM.Settings.Session.get_data("fi_shooter1_laser_id"))
    
func fi_shot_by_shooter2(__):
    process_shot_by_shooter(2, PM.Settings.Session.get_data("fi_shooter2_laser_id"))

func process_shot_by_shooter(shooter1_or2, laser_id):
    var legit_hit = false
    # We already check that it is not laser ID 0 with a counter or 0 in FrecoiLInterface.gd.
    if PM.Settings.Session.get_data("game_player_alive"):
        if PM.Settings.Session.get_data("team_amount") > 0:
            if PM.Settings.Session.get_data("friendly_fire"):
                # Add a check to make sure the laser_id is a valid id in the current game.
                if laser_id in PM.Settings.Session.get_data("players_mup_by_laser_id"):
                    legit_hit = true
            else:
                if not (PM.Settings.Session.get_data("players_mup_by_laser_id")[laser_id] in 
                        PM.Settings.Session.get_data("game_player_teammates")):
                    # Add a check to make sure the laser_id is a valid id in the current game.
                    if laser_id in PM.Settings.Session.get_data("players_mup_by_laser_id"):
                        legit_hit = true
        else:
            # Add a check to make sure the laser_id is a valid id in the current game.
            if laser_id in PM.Settings.Session.get_data("players_mup_by_laser_id"):
                legit_hit = true
    if legit_hit:
        #FIXME: Need to adapt (future expansion) to weapons doing variable damage
        if PM.Settings.Session.get_data("game_player_shield") > 0:
            PM.Settings.Session.set_data("game_player_shield", PM.Settings.Session.get_data(
                    "game_player_shield") - 1)
        else:
            PM.Settings.Session.set_data("game_player_health", PM.Settings.Session.get_data(
                    "game_player_health") - 1)
        call_deferred("delayed_vibrate")  # Because it was slowing down the processing of shots.
        if PM.Settings.Session.get_data("game_player_health") <= 0:
            if PM.Settings.Session.get_data("game_player_deaths") + 1 > PM.Settings.Session.get_data(
                    "respawns_allowed"):
                eliminated(laser_id)
            else:
                respawn_start(laser_id)
        else:
            var hexagons_to_blink = 0
            if PM.Settings.Session.get_data("fi_shooter" + str(shooter1_or2) + "_sensor_clip") != 0:
                hexagons_to_blink += 1
            if PM.Settings.Session.get_data("fi_shooter" + str(shooter1_or2) + "_sensor_front") != 0:
                hexagons_to_blink += 2
            if PM.Settings.Session.get_data("fi_shooter" + str(shooter1_or2) + "_sensor_left") != 0:
                hexagons_to_blink += 4
            if PM.Settings.Session.get_data("fi_shooter" + str(shooter1_or2) + "_sensor_right") != 0:
                hexagons_to_blink += 8
            PM.SS["BlinkHexTris"].blink_hexagons(hexagons_to_blink)
        call_deferred("record_game_event", "hit", {"laser_id": laser_id})

func server_setup_ingame_vars():
    if PM.Settings.Session.get_data("is_host") >= 1:
        var game_mode = PM.Settings.Session.get_data("game_settings")["game_mode"]
        PM.Settings.Session.set_data("game_mode", game_mode)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_mode",
             "var_val": game_mode}, PM.Networking.get_server_time_usec()
        )
        var team_amount = PM.Settings.Session.get_data("game_settings")["team_amount"]
        PM.Settings.Session.set_data("team_amount", team_amount)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "team_amount", 
            "var_val": team_amount}, PM.Networking.get_server_time_usec()
        )
        #FIXME: game_objective and game_objective_goal to be implemented later.
        #If game_objective_goal = 0 then game time only is used as the game end criteria.
        #If game_objective_goal != 0 then game should end when game_objective_goal is reached by a
        #player or team (game is won).
        var game_objective = PM.Settings.Session.get_data("game_settings")["game_objective"]
        PM.Settings.Session.set_data("game_objective", game_objective)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_objective", "var_val": game_objective}, 
            PM.Networking.get_server_time_usec()
        )
        var game_objective_goal = PM.Settings.Session.get_data("game_settings")["game_objective_goal"]
        PM.Settings.Session.set_data("game_objective_goal", game_objective_goal)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_objective_goal", "var_val": game_objective_goal}, 
            PM.Networking.get_server_time_usec()
        )
        var game_length_time = PM.Settings.Session.get_data("game_settings")["game_length_time"]
        PM.Settings.Session.set_data("game_length_time", game_length_time)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_length_time", "var_val": game_length_time}, 
            PM.Networking.get_server_time_usec()
        )
        var respawns_allowed = PM.Settings.Session.get_data("game_settings")["respawns_allowed"]
        PM.Settings.Session.set_data("respawns_allowed", respawns_allowed)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "respawns_allowed", "var_val": respawns_allowed},
            PM.Networking.get_server_time_usec()
        )
        #FIXME: respawn_type to be implemented later.  Currently it is assumed as "Timed" in the
        #gamecommon code.
        var respawn_type = PM.Settings.Session.get_data("game_settings")["respawn_type"]
        PM.Settings.Session.set_data("respawn_type", respawn_type)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "respawn_type", 
            "var_val": respawn_type}, PM.Networking.get_server_time_usec()
        )
        var respawn_delay = PM.Settings.Session.get_data("game_settings")["respawn_delay"]
        PM.Settings.Session.set_data("respawn_delay", respawn_delay)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "respawn_delay",
             "var_val": respawn_delay}, PM.Networking.get_server_time_usec()
        )
        var initial_health = PM.Settings.Session.get_data("game_settings")["initial_health"]
        PM.Settings.Session.set_data("initial_health", initial_health)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "initial_health",
            "var_val": initial_health}, PM.Networking.get_server_time_usec()
        )
        var initial_shield = PM.Settings.Session.get_data("game_settings")["initial_shield"]
        PM.Settings.Session.set_data("initial_shield", initial_shield)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "initial_shield",
            "var_val": initial_shield}, PM.Networking.get_server_time_usec()
        )
        var friendly_fire = PM.Settings.Session.get_data("game_settings")["friendly_fire"]
        PM.Settings.Session.set_data("friendly_fire", friendly_fire)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "friendly_fire",
            "var_val": friendly_fire}, PM.Networking.get_server_time_usec()
        )
        var force_recoil_action = PM.Settings.Session.get_data("game_settings")["force_recoil_action"]
        PM.Settings.Session.set_data("force_recoil_action", force_recoil_action)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "force_recoil_action", "var_val": force_recoil_action},
            PM.Networking.get_server_time_usec()
        )
        var indoor_game = PM.Settings.Session.get_data("game_settings")["indoor_game"]
        PM.Settings.Session.set_data("indoor_game", indoor_game)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "indoor_game",
            "var_val": indoor_game}, PM.Networking.get_server_time_usec()
        )
        #FIXME: healing_radius to be implemented later.
        var healing_radius = PM.Settings.Session.get_data("game_settings")["healing_radius"]
        PM.Settings.Session.set_data("healing_radius", healing_radius)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "healing_radius", "var_val": healing_radius}, 
            PM.Networking.get_server_time_usec()
        )
        #FIXME: heal_per_bandage to be implemented later.
        var heal_per_bandage = PM.Settings.Session.get_data("game_settings")["heal_per_bandage"]
        PM.Settings.Session.set_data("heal_per_bandage", heal_per_bandage)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "heal_per_bandage", "var_val": heal_per_bandage},
            PM.Networking.get_server_time_usec()
        )
        #FIXME: healing_time to be implemented later.
        var healing_time = PM.Settings.Session.get_data("game_settings")["healing_time"]
        PM.Settings.Session.set_data("healing_time", healing_time)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "healing_time",
            "var_val": healing_time}, PM.Networking.get_server_time_usec()
        )
        #FIXME: initial_medpacks to be implemented later.
        var initial_medpacks = PM.Settings.Session.get_data("game_settings")["initial_medpacks"]
        PM.Settings.Session.set_data("initial_medpacks", initial_medpacks)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "initial_medpacks", "var_val": initial_medpacks},
            PM.Networking.get_server_time_usec()
        )

        # This is the teams by team number
        PM.Settings.Session.set_data("game_teams_by_team_num_by_mup",{})
        #FIXME: The below code was just kept around for reference for future team modes.
        if PM.run_ITM and PM.ITM_act_as_server:
            PM.Settings.Session.set_data("players_team_num_by_mup", {PM.ITM_fake_server_unique_id: 0})
        else:
            PM.Settings.Session.set_data("players_team_num_by_mup", {OS.get_unique_id(): 0})
        if team_amount > 0:
            var game_teams_by_team_num_by_mup = PM.Settings.Session.get_data("game_teams_by_team_num_by_mup")
            var game_team_status_by_num = {}
            for team_num in range(0, game_teams_by_team_num_by_mup.size()):
                if team_num == 0:
                    pass
                else:
                    game_team_status_by_num[team_num] = "playing"
            PM.Settings.Session.set_data("teams_status_by_team_num", game_team_status_by_num)
        else:
            PM.Settings.Session.set_data("teams_status_by_team_num", {})

        PM.Settings.Session.set_data("start_game_delay", 8)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "start_game_delay", "var_val": 8},
            PM.Networking.get_server_time_usec()
        )
        PM.Settings.Session.set_data("game_weapon_types", PM.Settings.Session.get_data("armory"))
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_weapon_types",
            "var_val": PM.Settings.Session.get_data("armory")},
            PM.Networking.get_server_time_usec()
        )
        var amount = PM.SS["RandomGenerator"].roll_int_die(1, 5) #Five Level 1 Weapons total
        var start_weapons = []
        var start_weapons_number = []
        for i in range(0, amount):
            var random_weapon_number = PM.SS["RandomGenerator"].roll_int_die(1, 5)
            while (random_weapon_number in start_weapons_number):
                random_weapon_number = PM.SS["RandomGenerator"].roll_int_die(1, 5)
            start_weapons_number.append(random_weapon_number)
            start_weapons.append("W" + str(random_weapon_number) + "A")
        PM.Settings.Session.set_data("game_start_weapon_types", start_weapons)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_start_weapon_types", "var_val": start_weapons},
            PM.Networking.get_server_time_usec()
        )
        var start_ammo = {}
        for weapon in start_weapons:
            start_ammo[weapon] = PM.SS["RandomGenerator"].roll_int_die(10, 300)
        PM.Settings.Session.set_data("game_start_ammo", start_ammo)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_start_ammo", "var_val": start_ammo},
            PM.Networking.get_server_time_usec()
        )
        PM.Settings.Session.set_data("game_weapon_prob_default", {})
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_weapon_prob_default", "var_val": {}}, 
            PM.Networking.get_server_time_usec()
        )
        PM.Settings.Session.set_data("game_weapon_probabilities", {})
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_weapon_probabilities", "var_val": {}},
            PM.Networking.get_server_time_usec()
        )
        PM.Settings.Session.set_data("game_weapons_ammo_drop", {})
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "game_weapons_ammo_drop", "var_val": {}}, 
            PM.Networking.get_server_time_usec()
        )
        var hits = {}
        for mup in PM.Settings.Network.get_data("mups_to_peers"):
            hits[mup] = 0
        PM.Settings.Session.set_data("player_hits_by_mup", hits)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_hits_by_mup", "var_val": hits},
            PM.Networking.get_server_time_usec()
        )
        PM.Settings.Session.set_data("players_kills_by_mup", hits.duplicate())
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "players_kills_by_mup", "var_val": hits.duplicate()},
            PM.Networking.get_server_time_usec()
        )
        PM.Settings.Session.set_data("players_deaths_by_mup", hits.duplicate())
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "players_deaths_by_mup", "var_val": hits.duplicate()},
            PM.Networking.get_server_time_usec()
        )
        var names = {}
        for mup in PM.Settings.Network.get_data("mups_to_peers"):  # reusing this variable for names
            names[mup] = ""
        PM.Settings.Session.set_data("player_name_by_mup", names)
        PM.Networking.EventSync.record_event("sync_var", 
            {"var_name": "player_name_by_mup", "var_val": names}, 
            PM.Networking.get_server_time_usec()
        )
        PM.SS["GameMode"].modify_server_setup_ingame_vars()

func set_player_start_game_vars():
    tell_server_my_name()
    set_player_respawn_vars()
    PM.Settings.Session.set_data("game_player_alive", false)
    PM.Settings.Session.set_data("game_tick_toc_time_remaining",
            PM.Settings.Session.get_data("game_length_time"))
    PM.Settings.Session.set_data("game_tick_toc_time_elapsed", 0)
    # Quad State: 0=Not Stated, 1=Started, 2=Paused, 3=Game Over
    PM.Settings.Session.set_data("game_started", 0)  
    #if PM.run_ITM and PM.ITM_act_as_sever?
#    PM.Settings.Session.set_data("game_player_team", 
#           PM.Settings.Session.get_data("players_team_num_by_mup")[OS.get_unique_id()])
#    PM.Settings.Session.set_data("game_player_teammates", PM.Settings.Session.get_data(
#        "game_teams_by_team_num_by_mup")[PM.Settings.Session.get_data("game_player_team")])
    PM.Settings.Session.set_data("game_player_last_killed_by", "")
    PM.Settings.Session.set_data("game_player_deaths", 0)
    PM.Settings.Session.set_data("game_player_kills", 0)
    ReloadTimer.wait_time = PM.Settings.Session.get_data("game_weapon_reload_speed")
    ReloadTimer.connect("timeout", self, "reload_finish")
    HitIndicatorTimer.wait_time = PM.Settings.Preferences.get_data("player_hit_indicator_duration")
    HitIndicatorTimer.connect("timeout", self, "hit_indicator_stop")
    if PM.Settings.Session.get_data("respawn_delay") > 0:
        RespawnTimer.connect("timeout", self, "respawn_finish")
        RespawnTimer.wait_time = PM.Settings.Session.get_data("respawn_delay")
    if PM.Settings.Session.get_data("is_host") >= 1:
        # Now that the server is all setup, we can tell the clients to start finishing their setup.
        PM.Networking.EventSync.record_event("server_request", {"mup": "all", 
            "method": "set_player_start_game_vars", "object": "GameCommon"}
            , PM.Networking.get_server_time_usec()
        )
        PM.Networking.EventSync.record_event("server_request", {"mup": "all", 
            "method": "now_entering_game_state_2", "object": "GameCommon"}, 
            PM.Networking.get_server_time_usec()
        )

func set_player_respawn_vars():
    var start_game_wpn_types = PM.Settings.Session.get_data("game_start_weapon_types")
    var weapon_type = start_game_wpn_types[0]
    PM.Settings.Session.set_data("game_player_weapons", start_game_wpn_types)
    PM.Settings.Session.set_data("game_weapon_type", weapon_type)
    PM.Settings.Session.set_data("game_weapon_name",
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["name"])
    PM.Settings.Session.set_data("game_weapon_damage", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["damage"])
    PM.Settings.Session.set_data("game_weapon_shot_modes", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["shot_modes"])
    PM.Settings.Session.set_data("game_weapon_shot_mode", PM.Settings.Session.get_data(
            "game_weapon_shot_modes")[0])
    PM.Settings.Session.set_data("game_weapon_magazine_size", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["magazine_size"])
    PM.Settings.Session.set_data("game_weapon_magazine_ammo", 0)
    PM.Settings.Session.set_data("game_player_health", PM.Settings.Session.get_data("initial_health"))
    PM.Settings.Session.set_data("game_player_shield", PM.Settings.Session.get_data("initial_shield"))
    PM.Settings.Session.set_data("game_player_ammo", PM.Settings.Session.get_data("game_start_ammo"))
    PM.Settings.Session.set_data("game_weapon_total_ammo", PM.Settings.Session.get_data(
            "game_player_ammo")[weapon_type])
    PM.Settings.Session.set_data("game_weapon_reload_speed", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["reload_speed"])
    PM.Settings.Session.set_data("game_weapon_rate_of_fire", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["rate_of_fire"])
    PM.Settings.Session.set_data("game_weapon_short_power", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["short_power"])
    PM.Settings.Session.set_data("game_weapon_long_power", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["long_power"])
    PM.Settings.Session.set_data("game_weapon_indoor_long_power", 
        PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["indoor_long_power"])
    var long_power
    var short_power
    if PM.Settings.Session.get_data("indoor_game") == true:
        long_power = PM.Settings.Session.get_data("game_weapon_indoor_long_power")
        short_power = 0
    else: #Outdoor game
        long_power = PM.Settings.Session.get_data("game_weapon_long_power")
        short_power = PM.Settings.Session.get_data("game_weapon_short_power")
    PM.FreecoiLInterface.new_set_shot_mode(PM.Settings.Session.get_data("game_weapon_shot_mode"),
        long_power, short_power)
            
func near_time(input):
    # is the currrent delta(min/max) variance allowed.
    return abs(EventRecordTimer.time_left - input) <= 1  

func on_mup_reconnected(mups_reconnected):
    if PM.Settings.Session.get_data("is_host") >= 1:
        print("**** mups_reconnected = " + str(mups_reconnected))
        var mup_id = mups_reconnected.pop_front()
        if PM.SS["GameCommon"].game_state_by_mup.has(mup_id):
            PM.Settings.Log("on_mup_reconnected( " + str(mups_reconnected) + " )")
        else:
            PM.SS["GameCommon"].game_state_by_mup[mup_id] = 0

func on_new_identified_connection(mups_status):
    for mup in mups_status:
        if not PM.SS["GameCommon"].game_state_by_mup.has(mup):
            PM.SS["GameCommon"].game_state_by_mup[mup] = 0
    
func process_event_fired(event_to_sort):
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
                GunShotSound.volume_db = -25
                GunShotSound.play()
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            GunShotSound.volume_db = -25
            GunShotSound.play()
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        pass

func process_event_misfired(event_to_sort):
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
            if near_time(event_to_sort["time"]):
                EmptyShotSound.volume_db = -20
                EmptyShotSound.play()
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            EmptyShotSound.volume_db = -20
            EmptyShotSound.play()
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        pass

func process_event_reloading(event_to_sort):
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
            if near_time(event_to_sort["time"]):
                ReloadSound.volume_db = -20
                ReloadSound.play()
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            ReloadSound.volume_db = -20
            ReloadSound.play()
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        pass

func process_event_died(event_to_sort):  # Some player died.
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
            if near_time(event_to_sort["time"]):
                if event_to_sort["additional"]["laser_id"] == PM.Settings.Session.get_data(
                        "player_laser_id"):
                    TangoDownSound.play()
            else:
                pass
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            if event_to_sort["additional"]["laser_id"] == PM.Settings.Session.get_data(
                    "player_laser_id"):
                TangoDownSound.play()
        else:
            pass
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        var laser_id = event_to_sort["additional"]["laser_id"]
        var shooter_mup = PM.Settings.Session.get_data("players_mup_by_laser_id")[laser_id]
        var victim_mup = event_to_sort["rec_by"]
        var players_kills_by_mup = PM.Settings.Session.get_data("players_kills_by_mup")
        var players_deaths_by_mup = PM.Settings.Session.get_data("players_deaths_by_mup")
        players_kills_by_mup[shooter_mup] = players_kills_by_mup[shooter_mup] + 1
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "players_kills_by_mup", 
                "var_val": players_kills_by_mup}, PM.Networking.get_server_time_usec())
        players_deaths_by_mup[victim_mup] = players_deaths_by_mup[victim_mup] + 1
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "players_deaths_by_mup", 
                "var_val": players_deaths_by_mup}, PM.Networking.get_server_time_usec())
        if PM.Settings.Session.get_data("team_amount") != 0:
            var teams_scores_by_team_num = PM.Settings.Session.get_data("teams_scores_by_team_num")
            var player_team_by_mup = PM.Settings.Session.get_data("players_team_num_by_mup")
            var shooter_player_team = player_team_by_mup[shooter_mup]
            var shooter_player_team_score = teams_scores_by_team_num[shooter_player_team]
            shooter_player_team_score += 100
            teams_scores_by_team_num[shooter_player_team] = shooter_player_team_score
            PM.Networking.EventSync.record_event("sync_var", {"var_name": "teams_scores_by_team_num", 
                    "var_val": teams_scores_by_team_num}, PM.Networking.get_server_time_usec())

func process_event_eliminated(event_to_sort):
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
            if near_time(event_to_sort["time"]):
                if event_to_sort["additional"]["laser_id"] == PM.Settings.Session.get_data(
                        "player_laser_id"):
                    if not true:  # FIXME: We should play the EliminatedPlayerSound
                        # We have 4 to choose from.
                        pass
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            if event_to_sort["additional"]["laser_id"] == PM.Settings.Session.get_data(
                    "player_laser_id"):
                if not true:  # FIXME: We should play the EliminatedPlayerSound
                    # We have 4 to choose from.
                    pass
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        var player_team_by_mup = PM.Settings.Session.get_data("players_team_num_by_mup")
        var elim_player_team = player_team_by_mup[event_to_sort["rec_by"]]
        var players_status_by_mup = PM.Settings.Session.get_data("players_status_by_mup")
        players_status_by_mup[event_to_sort["rec_by"]] = "eliminated"
        PM.Settings.Session.set_data("players_status_by_mup", players_status_by_mup)
        var laser_id = event_to_sort["additional"]["laser_id"]
        var shooter_mup = PM.Settings.Session.get_data("players_mup_by_laser_id")[laser_id]
        var victim_mup = event_to_sort["rec_by"]
        var player_kills_by_mup = PM.Settings.Session.get_data("players_kills_by_mup")
        var player_deaths_by_mup = PM.Settings.Session.get_data("players_deaths_by_mup")
        player_kills_by_mup[shooter_mup] = player_kills_by_mup[shooter_mup] + 1
        PM.Settings.Session.set_data("players_kills_by_mup", player_kills_by_mup)
        player_deaths_by_mup[victim_mup] = player_deaths_by_mup[victim_mup] + 1
        PM.Settings.Session.set_data("players_deaths_by_mup", player_deaths_by_mup)
        var team_is_eliminated = true
        for player in player_team_by_mup:
            if player_team_by_mup[player] == elim_player_team:
                if players_status_by_mup[player] != "eliminated":
                    team_is_eliminated = false
        if team_is_eliminated:
            var team_num_in_elimination_order = PM.Settings.Session.get_data(
                    "team_num_in_elimination_order")
            team_num_in_elimination_order.append(elim_player_team)
            PM.Settings.Session.set_data("team_num_in_elimination_order", 
                    team_num_in_elimination_order)
            var game_team_scores = PM.Settings.Session.get_data("teams_scores_by_team_num")
            var elim_player_team_score = game_team_scores[elim_player_team]
            elim_player_team_score -= 1000
            var shooter_player_team = player_team_by_mup[shooter_mup]
            var shooter_player_team_score = game_team_scores[shooter_player_team]
            shooter_player_team_score += 100
            game_team_scores[elim_player_team] = elim_player_team_score
            game_team_scores[shooter_player_team] = shooter_player_team_score
            PM.Settings.Session.set_data("teams_scores_by_team_num", game_team_scores)
            var team_status_by_num = PM.Settings.Session.get_data("teams_status_by_team_num")
            team_status_by_num[elim_player_team] = "eliminated"
            PM.Settings.Session.set_data("teams_status_by_team_num", team_status_by_num)
            var teams_remaining = 0
            for team in team_status_by_num:
                if team_status_by_num[team] == "playing":
                    teams_remaining += 1
            if teams_remaining <= 1:
                PM.Networking.EventSync.record_event("end_game", 
                    {"reason": "team_elimination"}, PM.Networking.get_server_time_usec()
                )
        if PM.Settings.Session.get_data("team_amount") == 0:
            var number_of_players_alive = 0
            #FIXME: Fix the code below, just not sure how to yet.
            var game_teams_by_team_num_by_mup = PM.Settings.Session.get_data(
                    "game_teams_by_team_num_by_mup")
            for player_mup in game_teams_by_team_num_by_mup[0]:
                if players_status_by_mup[player_mup] != "eliminated":
                    number_of_players_alive += 1
            if number_of_players_alive == 1:
                PM.Networking.EventSync.record_event("end_game", {"reason": "ffa_elimination"}, 
                    PM.Networking.get_server_time_usec()
                )

func process_event_hit(event_to_sort): # Some player got hit.
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
            if near_time(event_to_sort["time"]):
                if event_to_sort["additional"]["laser_id"] == PM.Settings.Session.get_data(
                        "player_laser_id"):
                    if not NiceSound.playing:  # FIXME: NiceSound should be renamed to hit sound. 
                        # Also we have 6 sounds to choose from that we can play now.
                        # But dont play them every hit right now.
                        if PM.SS["RandomGenerator"].roll_int_die(1, 6) == 6:
                            NiceSound.play()
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            if event_to_sort["additional"]["laser_id"] == PM.Settings.Session.get_data(
                    "player_laser_id"):
                if not NiceSound.playing:  # FIXME: NiceSound should be renamed to hit sound. 
                    # Also we have 6 sounds to choose from that we can play now.
                    # But dont play them every hit right now.
                    if PM.SS["RandomGenerator"].roll_int_die(1, 6) == 6:
                        NiceSound.play()
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        pass

func process_event_end_game(event_to_sort):
    if PM.Settings.Session.get_data("game_started") != 3:
        PM.SS["GameMode"].end_game(event_to_sort["additional"]["reason"])
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        pass

func process_event_start_game(event_to_sort):
    if PM.Settings.Session.get_data("is_host") == 0:
        var peer_id = PM.Settings.Network.get_data("mups_to_peers")[event_to_sort["rec_by"]]
        if peer_id == 1:  # This came from the server.
            if (PM.Settings.Session.get_data("game_status") == 2 or 
                    PM.Settings.Session.get_data("game_status") == 4):
                start_game_now()

func process_event_pause_game(event_to_sort):
    if PM.run_ITM and PM.ITM_act_as_server:
        if event_to_sort["rec_by"] != PM.ITM_fake_server_unique_id:
            pass
    elif event_to_sort["rec_by"] != OS.get_unique_id(): # We already alert off our own events in real time.
        if near_time(event_to_sort["time"]):
            pass
    if PM.Settings.Session.get_data("is_host") >= 1:  # Server
        pass

func process_event_client_request(event_to_sort):
    if PM.Settings.Session.get_data("is_host") >= 1:
        PM.Settings.Log("process_event_client_request() " + str(event_to_sort), "testing")
        var method = event_to_sort["additional"]["method"]
        var mup = event_to_sort["rec_by"]
        if event_to_sort["additional"].has("args"):
            var arguments = event_to_sort["additional"]["args"]
            call(method, mup, arguments)
        else:
            call(method, mup)

func process_event_server_request(event_to_sort):
    if PM.Settings.Session.get_data("is_host") == 0:  # Not a host.
        PM.Settings.Log("process_event_server_request() " + str(event_to_sort), "testing")
        var mup = event_to_sort["additional"]["mup"]
        var apply_the_request = false
        if mup == "all":
            apply_the_request = true
        elif mup == OS.get_unique_id():
            apply_the_request = true
        if apply_the_request:
            var method = event_to_sort["additional"]["method"]
            if event_to_sort["additional"].has("args"):
                var arguments = event_to_sort["additional"]["args"]
                call(method, arguments)
            else:
                call(method)

func connection_status_event(new_status):
    if last_connection_status != new_status:
        last_connection_status = new_status
        PM.Networking.EventSync.record_event("connection", {"status": new_status}, 
            PM.Networking.get_server_time_usec()
        )

func reload_start():
    PM.FreecoiLInterface.reload_start()
    ReloadTimer.wait_time = PM.Settings.Session.get_data("game_weapon_reload_speed")
    ReloadTimer.start()
    get_tree().root.get_node("Container").current_scene.get_node("Footer/FooterBackImage/AmmoBox/AmmoTotNumber/Reloading").visible = true
    var collect_magazine_ammo = (PM.Settings.Session.get_data("game_weapon_magazine_ammo") + 
        PM.Settings.Session.get_data("game_weapon_total_ammo"))
    PM.Settings.Session.set_data("game_weapon_total_ammo", collect_magazine_ammo)
    PM.Settings.Session.set_data("game_weapon_magazine_ammo", 0)
    ReloadSound.volume_db = 0
    #ReloadSound.pitch_scale = 0.45 / PM.Settings.Session.get_data("game_weapon_reload_speed")
    ReloadSound.play()
    PM.Networking.EventSync.record_event("reloading", 
        {"gun": PM.Settings.Session.get_data("game_weapon_type"), 
        "reload_speed": PM.Settings.Session.get_data("game_weapon_reload_speed")}, 
        PM.Networking.get_server_time_usec()
    )
    
func reload_finish():
    var remove_magazine_ammo = 0
    if PM.Settings.Session.get_data("game_weapon_total_ammo") > PM.Settings.Session.get_data(
                "game_weapon_magazine_size"):
        PM.Settings.Session.set_data("game_weapon_magazine_ammo", PM.Settings.Session.get_data(
                "game_weapon_magazine_size"))
        remove_magazine_ammo = (PM.Settings.Session.get_data("game_weapon_total_ammo") - 
            PM.Settings.Session.get_data("game_weapon_magazine_size"))
    elif PM.Settings.Session.get_data("game_weapon_total_ammo") == 0:
        PM.Settings.Session.set_data("game_weapon_magazine_ammo", 0)
    else:
        PM.Settings.Session.set_data("game_weapon_magazine_ammo", PM.Settings.Session.get_data(
                "game_weapon_total_ammo"))
    PM.Settings.Session.set_data("game_weapon_total_ammo", remove_magazine_ammo)
    PM.FreecoiLInterface.reload_finish(PM.Settings.Session.get_data("game_weapon_magazine_ammo"),
            PM.Settings.Session.get_data("player_laser_id"))
    var weapon_type = PM.Settings.Session.get_data("game_weapon_type")
    get_tree().root.get_node("Container").current_scene.get_node("Footer/FooterBackImage/AmmoBox/AmmoTotNumber/Reloading").visible = false
    PM.Settings.Log("reload_finish() completed!")

func eliminated(laser_id):
    if PM.Settings.Session.get_data("game_started") == 1:
        PM.FreecoiLInterface.reload_start()
        PM.Settings.Session.set_data("game_player_alive", false)
        PM.Networking.EventSync.record_event("eliminated", {"laser_id": laser_id},
            PM.Networking.get_server_time_usec()
        )
        if laser_id != 0:
            var shooter_mup = PM.Settings.Session.get_data("players_mup_by_laser_id")[laser_id]
            var shooter_name = PM.Settings.Session.get_data("player_name_by_mup")[shooter_mup]
            PM.Settings.Session.set_data("game_player_last_killed_by", shooter_name)
        else:
            PM.Settings.Session.set_data("game_player_last_killed_by", "ID 0")
        PM.Settings.Session.set_data("game_player_deaths", PM.Settings.Session.get_data(
                "game_player_deaths") + 1)
        get_tree().call_group("Container", "next_menu", "2,0")

func respawn_start(laser_id):
    if PM.Settings.Session.get_data("game_started") == 1:
        PM.FreecoiLInterface.reload_start()
        PM.Settings.Session.set_data("game_player_alive", false)
        if PM.Settings.Session.get_data("respawn_delay") > 0:
            TickTocTimer.connect("timeout", self, "update_respawn_delay_panel")
            RespawnTimer.start()
            update_respawn_delay_panel()
        if laser_id != 0:
            var shooter_mup = PM.Settings.Session.get_data("players_mup_by_laser_id")[laser_id]
            var shooter_name = PM.Settings.Session.get_data("player_name_by_mup")[shooter_mup]
            PM.Settings.Session.set_data("game_player_last_killed_by", shooter_name)
        else:
            PM.Settings.Session.set_data("game_player_last_killed_by", "ID 0")
        PM.Settings.Session.set_data("game_player_deaths", PM.Settings.Session.get_data(
                "game_player_deaths") + 1)
        call_deferred("instant_move_Panel_in", RespawnPanel)
        call_deferred("record_game_event", "died", {"laser_id": laser_id})
    
func respawn_finish():
    if PM.Settings.Session.get_data("game_started") == 1:
        set_player_respawn_vars()
        PM.Settings.Session.set_data("game_player_alive", true)
        PM.Networking.EventSync.record_event("alive", {}, 
            PM.Networking.get_server_time_usec()
        )
        reload_finish()
        instant_move_panel_out(RespawnPanel)

func change_weapon(wpn_name=null):
    PM.FreecoiLInterface.reload_start()
    var player_wpns = PM.Settings.Session.get_data("game_player_weapons")
    var weapon_type = PM.Settings.Session.get_data("game_weapon_type")
    if wpn_name == null:  # We just swap to the next weapon
        var counter = 0
        for wpn in player_wpns:  # find the index of the weapon.
            if wpn == weapon_type:
                break
            else:
                counter += 1
        counter += 1
        if counter == player_wpns.size():
            counter = 0
        weapon_type = player_wpns[counter]
        PM.Settings.Session.set_data("game_weapon_type", weapon_type)
        PM.Settings.Session.set_data("game_weapon_name",
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["name"])
        PM.Settings.Session.set_data("game_weapon_damage", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["damage"])
        PM.Settings.Session.set_data("game_weapon_shot_modes", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["shot_modes"])
        PM.Settings.Session.set_data("game_weapon_shot_mode", PM.Settings.Session.get_data(
                "game_weapon_shot_modes")[0])
        PM.Settings.Session.set_data("game_weapon_magazine_size", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["magazine_size"])
        PM.Settings.Session.set_data("game_weapon_magazine_ammo", 0)
        PM.Settings.Session.set_data("game_weapon_total_ammo", PM.Settings.Session.get_data(
                "game_player_ammo")[weapon_type])
        PM.Settings.Session.set_data("game_weapon_reload_speed", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["reload_speed"])
        PM.Settings.Session.set_data("game_weapon_rate_of_fire", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["rate_of_fire"])
        PM.Settings.Session.set_data("game_weapon_short_power", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["short_power"])
        PM.Settings.Session.set_data("game_weapon_long_power", 
            PM.Settings.Session.get_data("game_weapon_types")[weapon_type]["long_power"])
        var long_power
        var short_power
        if PM.Settings.Session.get_data("indoor_game") == true:
            long_power = PM.Settings.Session.get_data("game_weapon_indoor_long_power")
            short_power = 0
        else: #Outdoor game
            long_power = PM.Settings.Session.get_data("game_weapon_long_power")
            short_power = PM.Settings.Session.get_data("game_weapon_short_power")
        PM.FreecoiLInterface.new_set_shot_mode(PM.Settings.Session.get_data("game_weapon_shot_mode"),
            long_power, short_power)
        reload_start()

func notify_game_state_change(mup, args):
    if PM.Settings.Session.get_data("is_host") >= 1:
        var new_value = args[0]
        PM.SS["GameCommon"].game_state_by_mup[mup] = new_value
        PM.Settings.Log("notify_game_state_change( " + str(new_value) + " ) for mup " 
                + str(mup) + "  " + str(PM.SS["GameCommon"].game_state_by_mup), "testing")
        if new_value >= 2:
            var mups_reconnected = PM.Settings.Session.get_data("mups_reconnected")
            mups_reconnected.erase(mup)
            PM.Settings.Session.set_data("mups_reconnected", mups_reconnected, false, false)
        update_ingame_with_new_player(mup)
        PM.SS["GameMode"].call_deferred("check_if_enough_ready_to_start")

func update_ingame_with_new_player(mup):
    var player_kills_by_mup = PM.Settings.Session.get_data("players_kills_by_mup")
    var player_deaths_by_mup = PM.Settings.Session.get_data("players_deaths_by_mup")
    player_kills_by_mup[mup] = 0
    player_deaths_by_mup[mup] = 0
    PM.Settings.Session.set_data("players_kills_by_mup", player_kills_by_mup)
    PM.Settings.Session.set_data("players_deaths_by_mup", player_deaths_by_mup)

func tell_server_my_picture_path() :
    pass
#    rpc_id(1, "remote_tell_server_my_picture_path", PM.Settings.Preferences.get_data("player_avatar"))
   
func remote_tell_server_my_picture_path(picture_path):
    if PM.Settings.Session.get_data("is_host") >= 1:
        var sender_rpc_id = get_tree().get_rpc_sender_id()
        var sender_mup = PM.Settings.Network.get_data("peers_to_mups")[sender_rpc_id]
         
func tell_server_my_name():
    if PM.Settings.Session.get_data("is_host") >= 1:
        if PM.run_ITM and PM.ITM_act_as_server:
            remote_tell_server_my_name(PM.ITM_fake_server_unique_id, [PM.Settings.Preferences.get_data("player_name")])
        else:
            remote_tell_server_my_name(OS.get_unique_id(), [PM.Settings.Preferences.get_data("player_name")])
    else:
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "remote_tell_server_my_name", "object": "GameCommon", 
            "args": [PM.Settings.Preferences.get_data("player_name")]}, 
            PM.Networking.get_server_time_usec()
        )

func remote_tell_server_my_name(mup, args):
    if PM.Settings.Session.get_data("is_host") >= 1:
        var new_name = args[0]
        var player_names_by_mup = PM.Settings.Session.get_data("player_name_by_mup")
        player_names_by_mup[mup] = new_name
        PM.Settings.Session.set_data("player_name_by_mup", player_names_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "player_name_by_mup", 
                "var_val": player_names_by_mup}, PM.Networking.get_server_time_usec())

func get_next_available_player_laser_id(mup):
    if PM.Settings.Session.get_data("is_host") >= 1:
        next_available_player_laser_id += 1
        if next_available_player_laser_id > 65:  # Error too many players.
            pass  # FIXME: Maybe one day, not likley to ever happen though.
        var players_laser_id_by_mup = PM.Settings.Session.get_data("players_laser_id_by_mup")
        players_laser_id_by_mup[mup] = next_available_player_laser_id - 1
        PM.Settings.Session.set_data("players_laser_id_by_mup", players_laser_id_by_mup)
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "players_laser_id_by_mup", 
                "var_val": players_laser_id_by_mup}, PM.Networking.get_server_time_usec())
        invert_mups_to_lasers(PM.Settings.Session.get_data("players_laser_id_by_mup"))
        if PM.run_ITM and PM.ITM_act_as_server:
            if mup == PM.ITM_fake_server_unique_id:
                remote_set_player_laser_id([next_available_player_laser_id - 1])
            else:
                PM.Networking.EventSync.record_event("server_request", {"mup": mup, 
                    "method": "remote_set_player_laser_id", "args": 
                    [next_available_player_laser_id - 1], "object": "GameCommon"}, 
                    PM.Networking.get_server_time_usec()
                )
        elif mup == OS.get_unique_id():
            remote_set_player_laser_id([next_available_player_laser_id - 1])
        else:
            PM.Networking.EventSync.record_event("server_request", {"mup": mup, 
                "method": "remote_set_player_laser_id", "args": 
                [next_available_player_laser_id - 1], "object": "GameCommon"}, 
                PM.Networking.get_server_time_usec()
            )

func start_game_start_delay():
    if PM.Settings.Session.get_data("is_host") >= 1:
        yield(get_tree().create_timer(0.1), "timeout")  # Just to let the network settle out.
        var start_at_time = (OS.get_ticks_usec() + (
                PM.Settings.Session.get_data("start_game_delay")) * 1000000)
        PM.Networking.EventSync.record_event("server_request", {"mup":"all", "method": 
            "remote_start_game_start_delay", "args": [start_at_time], 
            "object": "GameCommon"}, PM.Networking.get_server_time_usec()
        )
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_status", 
            "var_val": 1}, PM.Networking.get_server_time_usec()
        )
        if PM.Settings.Session.get_data("is_host_pregame_ready"):
            remote_start_game_start_delay([start_at_time])
    
func remote_start_game_start_delay(args):
    var start_at_time = args[0]
    EventRecordTimer.start()
    TickTocTimer.connect("timeout", self, "tick_toc_continue")
    TickTocTimer.start()
    var wait_time_secs = (float(start_at_time) - 
            float(PM.Networking.get_server_time_usec())) / 1000000.0
    if wait_time_secs <= 0.0:
        wait_time_secs = 0.001
    StartGameTimer.wait_time = wait_time_secs
    StartGameTimer.connect("timeout", self, "start_the_game")
    StartGameTimer.start()
    if PM.Settings.Session.get_data("is_host") == 0 or PM.Settings.Session.get_data("is_host_pregame_ready"):
        TickTocTimer.connect("timeout", self, "update_start_delay_panel")
        update_start_delay_panel()
        instant_move_panel_out(WaitingPanel)
        instant_move_Panel_in(StartPanel)
        PM.SS["GameMode"].remote_start_game_start_delay_hook()
    
func start_the_game():
    PM.Settings.Session.set_data("game_started", 1)
    if PM.Settings.Session.get_data("is_host") >= 1:
        if PM.run_ITM and PM.ITM_act_as_server:
            notify_game_state_change(PM.ITM_fake_server_unique_id, [3])
        else:
            notify_game_state_change(OS.get_unique_id(), [3])
    else:
        PM.Networking.EventSync.record_event("client_request", 
            {"method": "notify_game_state_change", "object": "GameCommon", 
            "args": [3]}, PM.Networking.get_server_time_usec()
        )
    TimeRemainingTimer.start()
    if PM.Settings.Session.get_data("is_host") == 0 or PM.Settings.Session.get_data("is_host_pregame_ready"):
        instant_move_panel_out(StartPanel)
        respawn_finish()
    if PM.Settings.Session.get_data("is_host") >= 1:  # is a host
        PM.Networking.EventSync.record_event("sync_var", {"var_name": "game_status",
             "var_val": 2}, PM.Networking.get_server_time_usec()
        )
    PM.Networking.EventSync.record_event("start_game", {}, PM.Networking.get_server_time_usec())  

func start_game_now():
    EventRecordTimer.start()
    TickTocTimer.connect("timeout", self, "tick_toc_continue")
    TickTocTimer.start()
    PM.Settings.Session.set_data("game_status", 2)
    TimeRemainingTimer.start()
    if PM.Settings.Session.get_data("is_host") == 0 or PM.Settings.Session.get_data("is_host_pregame_ready"):
        TickTocTimer.connect("timeout", self, "update_start_delay_panel")
        get_tree().call_group("Container", "next_menu", "0,0")
        instant_move_panel_out(WaitingPanel)
        PM.SS["GameMode"].remote_start_game_start_delay_hook()
        instant_move_panel_out(StartPanel)
        get_tree().call_group("Container", "next_menu", "0,0")
        respawn_finish()
    PM.Networking.EventSync.record_event("start_game", {}, PM.Networking.get_server_time_usec())

func in_game_added_to_tree():
    var execute_in_game_added_to_tree = false
    if PM.Settings.Session.get_data("is_host") == 0:
        execute_in_game_added_to_tree = true
    else:
        if PM.Settings.Session.get_data("is_host_pregame_ready"):
            execute_in_game_added_to_tree = true
    if execute_in_game_added_to_tree:
        LPanel = get_tree().root.get_node("Container").current_scene.get_node("LPanel")
        RPanel = get_tree().root.get_node("Container").current_scene.get_node("RPanel")
        Footer = get_tree().root.get_node("Container").current_scene.get_node("Footer")
        StartPanel = get_tree().root.get_node("Container"
                ).current_scene.get_node("StartGameDelayPanel")
        WaitingPanel = get_tree().root.get_node("Container"
                ).current_scene.get_node("WaitingPanel")
        RespawnPanel = get_tree().root.get_node("Container"
                ).current_scene.get_node("RespawnPanel")
        call_deferred("move_Panel_in", LPanel)
        call_deferred("move_Panel_in", RPanel)
        call_deferred("move_Panel_in", Footer)
        call_deferred("instant_move_Panel_in", WaitingPanel)
        all_in_game_ui_added_to_tree = true
        if PM.Settings.Session.get_data("is_host_pregame_ready"):
            if PM.run_ITM and PM.ITM_act_as_server:
                notify_game_state_change(PM.ITM_fake_server_unique_id, [2])
            else:
                notify_game_state_change(OS.get_unique_id(), [2])
        yield(get_tree().create_timer(0.1), "timeout")
        in_game_added_to_tree_complete = true

func instant_move_Panel_in(Panel_ref):
    Panel_ref.rect_position.x = 0
    Panel_ref.rect_position.y = 0
       
func move_Panel_in(Panel_ref):
    var continue_motion = true
    var add_to_move_x = true
    var add_to_move_y = false
    if Panel_ref.rect_position.x > 1:
        add_to_move_x = false
    if Panel_ref.rect_position.y > 1:
        add_to_move_y = false
    while continue_motion:
        continue_motion = false
        if add_to_move_x:
            if Panel_ref.rect_position.x < 0:
                Panel_ref.rect_position.x += 10
                continue_motion = true
            else:
                Panel_ref.rect_position.x = 0
        else:
            if Panel_ref.rect_position.x > 0:
                Panel_ref.rect_position.x -= 10
                continue_motion = true
            else:
                Panel_ref.rect_position.x = 0
        if add_to_move_y:
            if Panel_ref.rect_position.y < 0:
                Panel_ref.rect_position.y += 10
                continue_motion = true
            else:
                Panel_ref.rect_position.y = 0
        else:
            if Panel_ref.rect_position.y > 0:
                Panel_ref.rect_position.y -= 10
                continue_motion = true
            else:
                Panel_ref.rect_position.y = 0
        yield(get_tree(), "idle_frame")

func instant_move_panel_out(Panel_ref):
    Panel_ref.rect_position.x = 561
    Panel_ref.rect_position.y = 961
   
func move_panel_out(Panel_ref):
    var continue_motion = true
    while continue_motion:
        continue_motion = false
        if Panel_ref.rect_position.x < 561:
            Panel_ref.rect_position.x += 10
            continue_motion = true
        else:
            Panel_ref.rect_position.x = 561
        if Panel_ref.rect_position.y < 961:
            Panel_ref.rect_position.y += 10
            continue_motion = true
        else:
            Panel_ref.rect_position.y = 961
        yield(get_tree(), "idle_frame")

func tick_toc_continue():
    TickTocTimer.start()
    
func update_start_delay_panel():
    if StartGameTimer.time_left != 0.0:
        var time_minutes = int(StartGameTimer.time_left) / 60
        var time_secs = "%02d" % (int(StartGameTimer.time_left) - time_minutes * 60)
        time_minutes = "%02d" % time_minutes
        var time_text = str(time_minutes) + ":" + str(time_secs)
        StartPanel.get_node("CenterContainer/VBoxContainer/Label2").text = time_text
    else:
        TickTocTimer.disconnect("timeout", self, "update_start_delay_panel")
        
func update_respawn_delay_panel():
    if RespawnTimer.time_left != 0.0:
        TickTocTimer.start()
        var time_minutes = int(RespawnTimer.time_left) / 60
        var time_secs = "%02d" % (int(RespawnTimer.time_left) - time_minutes * 60)
        time_minutes = "%02d" % time_minutes
        var time_text = str(time_minutes) + ":" + str(time_secs)
        RespawnPanel.get_node("CenterContainer/VBoxContainer/RespawnTime").text = time_text
    else:
        RespawnPanel.get_node("CenterContainer/VBoxContainer/RespawnTime").text = "0.00"
        TickTocTimer.disconnect("timeout", self, "update_respawn_delay_panel")

remote func remote_set_player_laser_id(args):
    var new_id = args[0]
    PM.Settings.Log("Got Laser ID of " + str(new_id))
    PM.Settings.Session.set_data("player_laser_id", new_id)
    PM.FreecoiLInterface.set_laser_id(PM.Settings.Session.get_data("player_laser_id"))

func delayed_vibrate():
    PM.FreecoiLInterface.vibrate(200)

func now_entering_game_state_2():
    while PM.Settings.Session.get_data("player_laser_id") == null:
        yield(get_tree(), "idle_frame")
    while not PM.Settings.Session.get_data("player_name_by_mup").has(OS.get_unique_id()):
        yield(get_tree(), "idle_frame")
    while not in_game_added_to_tree_complete:
        yield(get_tree(), "idle_frame")
    while not PM.Networking.server_time_deviation_is_acceptable():
        yield(get_tree(), "idle_frame")
    var high_fps_count = 0
    while high_fps_count < 120:  # This forces the fps to smooth out, before we call a start to the game.
        if Engine.get_frames_per_second() > 40:
            high_fps_count += 1
        else:
            high_fps_count -= 1
        yield(get_tree(), "idle_frame")
    PM.Networking.EventSync.record_event("client_request", 
        {"method": "notify_game_state_change", "object": "GameCommon",
        "args": [2]}, PM.Networking.get_server_time_usec()
    )

func game_status_update():
    if PM.Settings.Session.get_data("game_status") == 6 or PM.Settings.Session.get_data("game_status") == 13:
        PM.Networking.EventSync.record_event("end_game", 
            {"reason": "host_terminated"}, PM.Networking.get_server_time_usec()
        )

func fix_looping_audio_to_not():
    ReloadSound.stream.loop = false
    EmptyShotSound.stream.loop = false
    #NiceSound.stream.loop = false
    GunShotSound.stream.loop = false
    #TangoDownSound.stream.loop = false

func test_send_to_client():
    PM.Networking.EventSync.record_event("test_client", 
        {"test": "client_got_this_string"}, PM.Networking.get_server_time_usec()
    )
    
func test_send_to_server():
    PM.Networking.EventSync.record_event("test_server", 
        {"test": "server_got_this_string"}, PM.Networking.get_server_time_usec()
    )

func process_event_test_client(event):
    if PM.Settings.Session.get_data("is_host") == 0:
        if event["additional"]["test"] == "client_got_this_string":
            print("Test Client Recieve worked. " + str(event["time"]))

func process_event_test_server(event):
    if PM.Settings.Session.get_data("is_host") >= 1:
        if event["additional"]["test"] == "server_got_this_string":
            print("Test server Recieve worked. " + str(event["time"]))
