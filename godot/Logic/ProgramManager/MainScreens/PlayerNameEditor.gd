extends Resource
# Technically this extends Resource, but that is the default if empty.

var state_has_on_ready = false
var state_has_on_process = false
var state_has_on_physics_process = false
var state_has_on_input = false
var state_has_on_unhandled_input = false
const STATE_ID:String = "PlayerNameEditor"
var state_num: int
var transitionable_states: Dictionary = {}
var required_for_state_is_complete = false
var started_background_loading = false
var loaded_profile_name_editor = false
var loaded_profile_avatar_editor = false
var loaded_profile_editor = false
var loaded_en_mv_welcome = false
var player_name_is_set = false
var player_avatar_is_set = false
var my_screen_offset: String = "0,0"
var my_screen_name: String = ""
var next_state: String
var loading_next_state_already = false
var scene_loaded = false

 
func on_enter(state_num_assigned, _previous_state) -> void:
    state_num = state_num_assigned
    PM.Settings.Log("Program Manager State = " + STATE_ID + " " + str(state_num), "info")
    load_required_for_state()
#    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("fi_location_status"), self, 
#        "check_if_enabled")
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    PM.get_tree().root.get_node("Container/Audio/Stream1").play()
    PM.Settings.Session.connect(PM.Settings.Session.monitor_data("ProgramManager_state_trigger"), self, "state_trigger")
    PM.Settings.Preferences.connect(PM.Settings.Preferences.monitor_data("player_name"), self, "name_is_set")
    screen_change(null)
    

func on_exit() -> void:
    PM.Settings.Preferences.disconnect(PM.Settings.Preferences.monitor_data("player_name"), self, "name_is_set")
    PM.Settings.Session.disconnect(PM.Settings.Session.monitor_data("current_screen"), self, "screen_change")
    call_deferred("cleanup")

func state_trigger(new_value):
    if new_value != null:
        pass

func screen_change(current_screen): #Function used to handle user attempting to navigate to other screens
    if current_screen == null:
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)
    else:
        #When ready, need to change this to allow MainMenu, PlayerSettings transitions (replace or adapt below)
        if PM.Settings.Session.get_data("current_screen") != my_screen_offset:
            PM.get_tree().call_group("Container", "next_screen", my_screen_offset)

func check_if_enabled(granted):
    if granted:
        PM.call_deferred("evaluate_state_change", transitionable_states["WeaponConnected"])
    else:
        screen_change(null)

func load_required_for_state():
    if not started_background_loading:
        started_background_loading = true
        var counter = 0
        for state in PM.States:
            match state.STATE_ID:
                "PlayerAvatarSelector":
                    transitionable_states[state.STATE_ID] = counter
            counter += 1
        var player_name = PM.Settings.Preferences.get_data("player_name")
        if player_name != null and player_name != "":
            next_state = "PlayerAvatarSelector"
            transition_to_next_state()
        else:
            PM.background_load_resource("res://scenes/MainScreens/PlayerNameEditor.tscn", 
                    self, "setup_profile_name_editor")
            scene_loaded = true  

func setup_profile_name_editor(background_results):
    if not required_for_state_is_complete:
        var results = background_results[PM.loading_thread_num]
        if results == "finished":
            PM.disconnect_background_load_resource(self, "setup_profile_name_editor")
            var loaded_scene = PM.Helpers.get_loaded_resource_from_background(
                    PM.loading_thread_num).instance()
            my_screen_offset = loaded_scene.editor_description
            my_screen_name = loaded_scene.name
            PM.get_tree().root.get_node("Container").current_scene.add_child(
                    loaded_scene)
            loaded_profile_name_editor = true
            PM.background_load_resource("res://assets/sounds/menu/EN_MV_welcome.ogg", 
                    self, "setup_en_mv_welcome")

func setup_en_mv_welcome(background_results):
    if not required_for_state_is_complete:
        var results = background_results[PM.loading_thread_num]
        if results == "finished":
            PM.disconnect_background_load_resource(self, "setup_en_mv_welcome")
            var temp = PM.get_tree().root.get_node("Container/Audio/Stream1")
            temp.stream = PM.Helpers.get_loaded_resource_from_background(PM.loading_thread_num)
            temp.stream.loop = false
            loaded_en_mv_welcome = true
            call_deferred("setup_required_for_state")

func setup_required_for_state():
    if not required_for_state_is_complete:
        if (loaded_profile_name_editor and loaded_en_mv_welcome):
            PM.get_tree().root.get_node("Container").current_scene.get_node(
                    "PlayerNameEdit/CenterContainer/VBoxContainer/Name").grab_focus()
            required_for_state_is_complete = true

func name_is_set(__):
    next_state = "PlayerAvatarSelector"
    transition_to_next_state()

func background_load_state_ahead():
    PM.States[transitionable_states["PlayerAvatarSelector"]].load_required_for_state()
    call_deferred("setup_state_ahead")

func setup_state_ahead():
    while not PM.States[transitionable_states["PlayerAvatarSelector"]].required_for_state_is_complete:
        yield(PM.get_tree(), "idle_frame")
    PM.call_deferred("evaluate_state_change", transitionable_states["PlayerAvatarSelector"])

func cleanup():
    yield(PM.get_tree(), "idle_frame")
    if scene_loaded:
        PM.get_tree().root.get_node("Container").current_scene.get_node(my_screen_name).queue_free()
    required_for_state_is_complete = false
    loading_next_state_already = false
    started_background_loading = false
    my_screen_offset = "0,0"
    my_screen_name = ""

# This is now the prefered method of transitioning to the next state.  Transitions the entire State Class.
func transition_to_next_state():  
    PM.call_deferred("evaluate_state_change", transitionable_states[next_state])
