extends Resource

var Client


func _connected_ok():
    PM.Settings.Log("Network: Client: Connection Established to the server, my peer id is: " + 
            str(PM.Networking.get_tree().get_network_unique_id()), "info")
    PM.Networking.rpc_id(1, "identify", OS.get_unique_id(), 
            PM.Networking.get_tree().get_network_unique_id())
    PM.Networking.number_of_pings_unanswered = 0
    PM.Settings.Session.set_data("connection_status", "connected")
    
func _connection_failed():
    PM.Settings.Log("Network: Client: Connection failed.", "warning")
    PM.Networking.get_tree().set_network_peer(null)
    PM.Settings.Session.set_data("connection_status", "disconnected")
    # Attempt to reconnect.
    
func _server_disconnected():
    PM.Settings.Log("Network: Client: Connection terminated by the server.", "warning")
    PM.Networking.get_tree().set_network_peer(null)
    PM.Settings.Session.set_data("connection_status", "disconnected")
    
func setup_as_client():
    if PM.Settings.Session.get_data("connection_status") == "disconnected":
        PM.Settings.Session.set_data("connection_status", "reconnecting")
    else:
        PM.Settings.Session.set_data("connection_status", "connecting")
    PM.Settings.Log("Network: Client: Setting up as a client with server address of " + 
            PM.Settings.Session.get_data("server_ip") + ":" + 
            str(PM.Settings.Session.get_data("server_port")), "info")
    Client = NetworkedMultiplayerENet.new()
    Client.create_client(PM.Settings.Session.get_data("server_ip"), int(PM.Settings.Session.get_data("server_port")))
    PM.Networking.get_tree().set_network_peer(Client)
