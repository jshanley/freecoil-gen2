# PM.Settings.Session

Session Settings are critical because they are not passed over the network and they are not saved between runs of the application/game. This is like the games scratch pad that it keeps from all of the other players. But it still comes with all of the other benefits of being a Settings Data class. The Session data is reset when the app restarts.

## "is_host"

Used to determine if a user will be a host or dedicated host. Dedicated host, is NOT a player, sets up the game, but does NOT play, can also manage the game as it is progressing.

### "is_host" Values

* 0 == The user is not a host.
* 1 == The user is a host (not a dedicated host).
* 2 == The user is a dedicated host.

## "is_observer"

Used to determine if a user will be an observer. NOT a player just watching scores & game events.

### "is_observer" Values

* true == The user is an observer.
* false == The user is NOT an observer.

## "is_soldier"

Used to determine if a player is a soldier that will use a Recoil weapon.

### "is_soldier" Values

* true == The user is a Soldier player.
* false == The user is NOT a Soldier player.

## "game_name"

Used to define the game name (string) for broadcasting from the host to the players.

## "need_gps"

Used to determine if the current user requires GPS.

### "need_gps" Values

* true == The current user requires GPS.
* false == The current user does not require GPS.

## "need_bluetooth"

Used to determine if the current user requires Bluetooth(Gun).

### "need_bluetooth" Values

* true == The current user requires Bluetooth(Gun).
* false == The current user does not require Bluetooth(Gun).

## "game_player_health" TBD

## "game_player_alive" TBD

## "game_weapon_magazine_ammo" TBD

## "fi_bluetooth_status"

Current status of the bluetooth functionality.

### "fi_bluetooth_status" Values

* 0 == Bluetooth is Off/Disabled.
* 1 == Bluetooth is On/Enabled.
* 2 == Error setting up Bluetooth.

## "fi_fine_access_location_permission"

Fine access location permission status (necessary for Bluetooth connectivity).

### "fi_fine_access_location_permission" Values

* true == Permission was granted.
* false == Permission was not granted.

## "fi_cell_location_status"

Indicates whether the device has something that can provide cellular location.

### "fi_cell_location_status" Values

* true
* false

## "fi_gps_location_status"

Indicates whether the device has something that can provide gps location.

### "fi_gps_location_status" Values

* true
* false

## "fi_weapons_found"

Weapons found on Bluetooth query.

### "fi_weapons_found" layout

{"name"(broadcast): "mac address"}

## "fi_weapon_highlighted"

Weapon name (Bluetooth broadcast) of user selection

## "fi_laser_is_connected"

Status of weapon connection

### "fi_laser_is_connected" Values

* 0 == Never Connected
* 1 == Trying
* 2 == Connected
* 3 == Disconnected
* 4 == Retrying

## "fi_laser_recoil" (related to Preferences variable recoil_action and InGame variable force_recoil_action)

Recoil action status of currently connected weapon.

### "fi_laser_recoil" Values

* true == Recoil action is activated
* false == Recoil action is not activated

## "fi_laser_mac_address"

Mac address of the currently connected weapon.

## "fi_laser_battery_lvl"

Battery level of the currently connected weapon.

### "fi_laser_battery_lvl" Values

* Range of Values = 0-100
* Full Power = 84-100
* Mid Power = 68-83
* Low Power = 60-67
* No Power = 0-59

## "fi_trigger_btn_counter"

Counter for the number of times that the trigger button has been pulled.

## "fi_reload_btn_counter"

Counter for the number of times that the reload button has been pushed.

## "fi_thumb_btn_counter"

Counter for the number of times that the thumb button has been pushed.

## "fi_power_btn_counter"

Counter for the number of times that the power button has been pushed.

## "fi_shooter1_laser_id" or "fi_shooter2_laser_id" (this also corresponds to player_id)

The unique player's laser id of the received IR shot (first or second shot received).

### "fi_shooter1_laser_id" or "fi_shooter2_laser_id" Values

* Range of Player ID's = 01-63

## "fi_shooter1_shot_counter" or "fi_shooter2_shot_counter"

The total number of cummulative times shot by the fi_shooter1_laser_id or fi_shooter2_laser_id player.

## "fi_shooter1_weapon_profile" or "fi_shooter2_weapon_profile"

The weapon profile of fi_shooter1_laser_id or fi_shooter2_laser_id player.

## "fi_shooter1_charge_level" or "fi_shooter2_charge_level"

The charge level of the shot from fi_shooter1_laser_id or fi_shooter2_laser_id player.

## "fi_shooter1_sensor_clip" or "fi_shooter2_sensor_clip"

Signifies when the clip sensor was hit by fi_shooter1_laser_id or fi_shooter2_laser_id player.

### "fi_shooter1_sensor_clip" or "fi_shooter2_sensor_clip" Values

* Question (Jordan):  Are the values either 0 (miss) or 1 (hit)?

## "fi_shooter1_sensor_front" or "fi_shooter2_sensor_front"

Signifies when the front sensor was hit by fi_shooter1_laser_id or fi_shooter2_laser_id player.

### "fi_shooter1_sensor_front" or "fi_shooter2_sensor_front" Values

* Question (Jordan):  Are the values either 0 (miss) or 1 (hit)?

## "fi_shooter1_sensor_left" or "fi_shooter2_sensor_left"

Signifies when the left sensor was hit by fi_shooter1_laser_id or fi_shooter2_laser_id player.

### "fi_shooter1_sensor_left" or "fi_shooter2_sensor_left" Values

* Question (Jordan):  Are the values either 0 (miss) or 1 (hit)?

## "fi_shooter1_sensor_right" or "fi_shooter2_sensor_right"

Signifies when the right sensor was hit by fi_shooter1_laser_id or fi_shooter2_laser_id player.

### "fi_shooter1_sensor_right" or "fi_shooter2_sensor_right" Values

* Question (Jordan):  Are the values either 0 (miss) or 1 (hit)?

## "fi_power_btn_pressed"

Signifies when the power button was pressed.

### "fi_power_btn_pressed" Values

Signifies when the power button was pressed.

* 0 == Not Pressed
* 1 == Pressed

## "fi_trigger_btn_pressed"

Signifies when the trigger was pulled.

### "fi_trigger_btn_pressed" Values

* 0 == Not Pressed
* 1 == Pressed

## "fi_thumb_btn_pressed"

Signifies when the thumb button was pressed.

### "fi_thumb_btn_pressed" Values

* 0 == Not Pressed
* 1 == Pressed

## "fi_reload_btn_pressed"

Signifies when the reload button was pressed.

### "fi_reload_btn_pressed" Values

* 0 == Not Pressed
* 1 == Pressed

## "fi_laser_status" TBD

## "fi_wpn_prfl" TBD

## "fi_location_quality" TBD

## "physical_laser_type" TBD

## "mup_id" TBD

## "server_ip" TBD

## "server_port" TBD

## "server_ip_issue" TBD

## "players_ip" TBD

## "player_team" TBD

## "connection_status" TBD

## "preset_armory"

Defines the weapons that are allowed in the game and their respective preset settings.

### "preset_armory" Values

It is important to note that all of the weapons are virtual and are not tied to the physical model of the weapon you are utilizing in game.

```
{"W1A":{"name":"Pistol","mag size":7,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":3,"rate of fire":0.3,"short power":150,"long power":15}, "W1B":{"name":"Pistol","mag size":15,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":3,"rate of fire":0.3,"short power":150,"long power":15},
"W1C":{"name":"Pistol","mag size":15,"damage":1,"semi-auto":false,"burst":true,"auto":false,"reload speed":3,"rate of fire":0.3,"short power":150,"long power":15},
"W1D":{"name":"Pistol","mag size":30,"damage":1,"semi-auto":false,"burst":false,"auto":true,"reload speed":3,"rate of fire":0.3,"short power":150,"long power":15},
"W2A":{"name":"Assault Rifle","mag size":10,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":4,"rate of fire":0.3,"short power":60,"long power":135},
"W2B":{"name":"Assault Rifle","mag size":20,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":4,"rate of fire":0.3,"short power":60,"long power":135},
"W2C":{"name":"Assault Rifle","mag size":20,"damage":1,"semi-auto":false,"burst":true,"auto":false,"reload speed":4,"rate of fire":0.3,"short power":60,"long power":135},
"W2D":{"name":"Assault Rifle","mag size":30,"damage":1,"semi-auto":false,"burst":false,"auto":true,"reload speed":4,"rate of fire":0.3,"short power":60,"long power":135},
"W3A":{"name":"Machine Gun","mag size":50,"damage":1,"semi-auto":false,"burst":false,"auto":true,"reload speed":10,"rate of fire":0.3,"short power":60,"long power":135},
"W3B":{"name":"Machine Gun","mag size":80,"damage":1,"semi-auto":false,"burst":false,"auto":true,"reload speed":10,"rate of fire":0.3,"short power":60,"long power":135},
"W3C":{"name":"Machine Gun","mag size":150,"damage":1,"semi-auto":false,"burst":false,"auto":true,"reload speed":8,"rate of fire":0.3,"short power":60,"long power":135},
"W3D":{"name":"Machine Gun","mag size":150,"damage":1,"semi-auto":false,"burst":false,"auto":true,"reload speed":6,"rate of fire":0.3,"short power":60,"long power":135},
"W4A":{"name":"Sniper Rifle","mag size":8,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":6,"rate of fire":0.5,"short power":0,"long power":255},
"W4B":{"name":"Sniper Rifle","mag size":12,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":5,"rate of fire":0.5,"short power":0,"long power":255},
"W4C":{"name":"Sniper Rifle","mag size":15,"damage":1,"semi-auto":true,"burst":false,"auto":false,"reload speed":5,"rate of fire":0.5,"short power":0,"long power":255},
"W4D":{"name":"Sniper Rifle","mag size":15,"damage":2,"semi-auto":true,"burst":false,"auto":false,"reload speed":5,"rate of fire":0.5,"short power":0,"long power":255},
"W5A":{"name":"Shotgun","mag size":2,"damage":2,"semi-auto":true,"burst":false,"auto":false,"reload speed":5,"rate of fire":0.5,"short power":255,"long power":0},
"W5B":{"name":"Shotgun","mag size":7,"damage":2,"semi-auto":true,"burst":false,"auto":false,"reload speed":8,"rate of fire":0.5,"short power":255,"long power":0},
"W5C":{"name":"Shotgun","mag size":10,"damage":2,"semi-auto":true,"burst":false,"auto":false,"reload speed":10,"rate of fire":0.5,"short power":255,"long power":0},
"W5D":{"name":"Shotgun","mag size":12,"damage":2,"semi-auto":true,"burst":false,"auto":false,"reload speed":10,"rate of fire":0.5,"short power":255,"long power":0}}
```

The "W" stands for Weapon. The # is the Weapon number.  The A, B, C, and D are the level of the weapon (1-4).  As the level increases the weapon gains capability.

```eval_rst
.. note:: rate_of_fire is not yet fully implemented.
```

## "next_armory_name"

The name (key) of the armory that is to be used next (copied to corresponding InGame variable).  This is a key of the "saved_armories" variable in the Preferences area of the host.

## "next_armory"

The armory that is to be used next (copied to corresponding InGame variable).  See "preset_armory" variable for dictionary format.

## "next_game_settings"

Contains all of the game settings that have been set by the host for the current game (copied over to "current_game_settings" in InGame variables).  See InGame("current_game_settings") documentation for further details.

## "scene_routing_indicator"

General use (generic) variable to enable adaptable routing of the user amongst scenes.

### "scene_routing_indicator"

* "All Battlemenus" == Route user through all BattleMenu scenes
* "Single Battlemenu" == Route user only to the indicated BattleMenu scene
* "OfflineSettings" == Route user back to OfflineSettings when finished
* "GameSetupOverview" == Route user back to GameSetupOverview when finished
* "Download Map" == Indicator that user was routed from the UserTypeChoice to MapSelect

## "current_armory_name"

The name of the current armory (from amongst the keys of the "saved_armories" variable in Settings.Preferences).  This variable is used only to display to the user the currently used armory amongst those that are available.
